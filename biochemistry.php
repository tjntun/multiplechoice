<?php $numbersOfQuestion = 241; 
    include 'inc/random.php'; ?>
<?php $i = 0; ?>
<div class="tab-item" id="tab-3">
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This organ is included in the endocrine system and  has a role in digestion.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Adrenals and Parathyroid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Pancreas and Thyroid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Spleen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />liver
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            For the hormones that are regulated by the pituitary gland, a signal is sent from the:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Hypothalamus to the pituitary gland in the form of releasing hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Hypothalamus to the pituitary gland in the form of  stimulating  hormone
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is the organ responsible for the direct control of the endocrine system through the pituitary gland.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Parathryroid  gland
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Thyroid gland
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Exopthalmus
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />Goiter
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The hypothalamus contains special cells called neurosecretory cells- neurons that secrete hormones like
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Thyrotropin-releasing hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Erythropoietin and thrombopoietibn
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What stimulate/s the release of follicle stimulating hormone and luteinizing hormone.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />GNRH
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />GnRH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is/these are the hormone/s that causes the pituitary gland to stop the release of growth hormone.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Somastostatin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Oxytoxin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is/these are the hormone/s that control the sexual function and production of sex steroids.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Leuteinizing hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Follicle stimulating hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Exopthalmus
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />Goiter
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            ACTH is released under the influence of:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Corticotropin-releasing hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Prolactin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            These are the non-cancerous tumors that occur in the pituitary glans.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="1" />Pituitary Adenosarcomas
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Pituitary  Adenomas
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The primary function of the thyroid gland is the production of the hormones:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />T3 or thyroxine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />T4  or thriiodothyronine
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The pineal gland is composed of:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Glial cells
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />pinealocytes
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />GNRH
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />GnRH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Growth hormones are:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Releasing
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />inhibiting
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The hypothalamus is a part of the brain located _______ to the brain stem.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Anterior
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Superior
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is These are the term/s  applied for protruding eyes seen in Grave’s Disease.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Exopthalmus
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Goiter
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The thymus is an organ responsible for the production of:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />thymocytes
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />lymphocytes
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />GNRH
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />GnRH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            These belong to the morphine group except one:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Codeine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Narcotine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />pseudomorphine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />papaverine
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is used as anti malarial drug.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Cinchona
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Benzodiazepine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Caffeine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Curare
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Used in Obstetrics
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Cocaine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Strychnine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Ergot
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Physostigmine
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Used as cleansing solution:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Clorox
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Hcl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Both
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Neither
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is added to water to make it potable:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Chlorine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Flourine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Lysol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Iodine
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Used in embalming:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Chloroform
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Formalin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Lysol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Cresol
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is also known as quick lime:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />CO
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />CaO
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />CO2
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />H2S
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            This is contained in paints:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Lead
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Zinc
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Ag
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Flouride
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Whitening agent in a toothpaste:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Flouride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Au
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Silver
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />CaO
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Used for Heart diseases
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Picric Acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Acetanilid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Digitalin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Acetophenetidin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            After mixing the arterial blood sample and heparin by rotating the syringe, it should be placed in ice water or coolant        at 1 to 5&deg;C temperature while being transported to the laboratory, what is the purpose of this practice?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />To stabilize the blood gas
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />To minimize leukocyte consumption of oxygen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />To prevent entry of oxygen from the environment
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />To avoid bubbles while transporting the blood
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            When urea in serum will react with the diacetyl monoxime reagent, the end product is known  as:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />red-orange tautomer
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />yellow-diazine derivative
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />indophenol blue
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />allantoin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In statistics, a measure of dispersion of values is known as:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />standard deviation
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />mean
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />cooficient of variation
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />average
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which  of these is the most commonly used patient-based quality control technique where current test result is compared with the previous result.?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Proficiency testing
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Internal QC program
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Outcome studies
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Delta check
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What is the percentage of serum calcium that is ionized?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />30%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />45%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />60%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />80%
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What assistance does an external quality assurance program provide for the laboratory?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Means to correlate test performed by different departments within the same laboratory.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Evaluation of its performance by comparison with other laboratories using the same method
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Data checks with previous tests on the same patient
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Limits for reference intervals
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What may be the cause  of neonatal physiological jaundice of the hepatic type?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Hemolytic episode caused by an ABO incompatibility departments within the same laboratory.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Structure of the common bile duct laboratories using the same method
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Hemolytic episode caused by a Rh incompatibility
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Deficiency  in the bilirubin conjugation enzyme system.
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What lipid component provides the starting material for the synthesis of various steroid hormone?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Fatty acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Triglyceride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Cholesterol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Phospholipid
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Lipoprotein that transports the exogeneous dietary fat
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Chylomicron
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />LDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The System International of expressing enzyme  activity defined as the amount of enzyme that catalyzes the conversion of 1 mole of substrate to produce  per second under proper condition is termed ___________________
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Units/ Liter
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Units/ml
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Umol/ Liter
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Katal Unit
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What   Lactate dehydrogenase isoenzyme that has the greatest clinical  significance  in the  detection of hepatic disorders.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />LD1
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />LD2
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />LD3
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />LD5
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            If the total bilirubin is 3.1 mg/dl  and the conjugated bilirubin is 2.0 mg/dl, the unconjugated bilirubin
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />0.5 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />1.1 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />2.2 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />5.1 mg/dl
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following drugs formed purple color when combined with ferric ions ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Saicylate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Acetaminophen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Alcohol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Barbiturates
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            May detect the presence of the substance through EMIT even up to 2 days after oral ingestion.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />methadone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Methaqualone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Prophoxyphene
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />None of the above
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following laboratory parameter may increase with  Amphetamine intoxication.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Creatinine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />CPK
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Blood Glucose
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />A & B
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which amino acid is directly involved in thyroid hormone synthesis?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />alanine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Glutamine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />threonine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Tyrosine
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The hormone that functions to promote sodium ion reabsorption by the kidney and thus  water retention is :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />aldosterone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />cortisol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />estrogen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />progesterone
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is a diagnostics  test for cystic fibrosis ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Sweat chloride Analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Sodium analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Bicarbonate analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Phosphate analysis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The expected blood gas results for a patient in chronic renal failure would match the pattern of :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Metabolic acidosis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Respiratory  acidosis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Metabolic alkalosis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Respiratory alkalosis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Reinsch’s test is used for screening toxic concentration of  which of the following substances.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Bismuth & Mercury
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Mercury & Cyanide
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Cyanide & Bismuth
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Arsenic & Cyanide
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The toxic substance can be analyzed using  hair & nails because of its affinity with the protein.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />ARSENIC
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Bismuth
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Lead
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Mercury
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of these methods measure the Iodine trapping function of the thyroid gland ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Thyroid Scan
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Thyroxine Index
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Resin T3 Uptake
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Radio active Iodine Uptake
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The liver plays an important role in endocrine hemostasis  because:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />It secretes most of the hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />It is the site of inactivation of most hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />it is where activation of many inactive hormones occurs
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />It produces bilirubin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In the case of burns and starvation, protein catabolism exceeds anabolism which is referred to as:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Positive N2  Balance
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Zero Net Charge
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Neutral Nitrogen Balance
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Negative Nitrogen Balance
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which combination describes a normal gaussian curve?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Median is greater then the mean
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Median is less than the Mean
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Mean is equal to the  median and the mode
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Mode is equal to the median
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the  following  is  an effect  of increase parathyroid hormone secretion ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Decreased blood calcium level
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Increased intestinal absorption of calcium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Increased renal absorption of phosphate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Decreased bone resorption
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following chromatographic methods allows quantitative detection & sharper separation of drugs?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />TLC
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />GC-MS
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />HPLC
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />LC-MS
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Ingestion of ethylene glycol, a toxic agent, is relativley common among children due to  its:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Aromatic smell
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Pink color
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Red color
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Sweet taste
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A patient is admitted with biliary cirrhosis. If a serum protein electrophoresis is performed, which of the following globulin fractions will be most elevated?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />alpha-1
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />alpha-2
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />beta
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />gamma
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Before unconjugated bilirubin can react with Ehrlich’s diazo reagent, which of the following must be added?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />acetone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />ether
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />distilled water
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />methyl alcohol
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A patient is presented with Addison’s disease  serum Na & K analysed are done. The results would revealed:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Normal Na, Low K
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Low Na, Low K
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Low Na High K
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />High Na Low K
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            If a blood gas specimen is lefy exposed to air, which of the following changes will occur ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />PO2 and pH increase, PCO2  decreases
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />PO2 and pH  decrease, pCO2 increases
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />PO2  increases, pH and pCO2  decreases.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />pO2 decreases, pCO2 and pH increase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the compensatory mechanism a respiratory acidosis?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Hypoventillation
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />decreased reabsorption of bicarbonate by the kidneys
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />increased Na and H exchange by the Kidneys.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Increased ammonia formation by the kidneys
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Why are the total thyroxine (T4) levels increased a pregnant women  and those who take oral contraceptives?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />inappropriate iodine metabolism
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />changes in tissue use
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />changes in concentration of thyroxine –binding globulin (TBG)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />changes in thyroglobulin synthesis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In  bilirubin determination, the purpose of adding a concentrated caffeine solution or methyl alcohol is to:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Allow indirect bilirubin to react with color reagent
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Dissolve conjugated  bilirubin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Precipitate protein)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Prevent any change  in pH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Given the following results: <br>
            Alkaline phosphatase -----------marked increase<br>
            Aminotransferase---------------slightly increase<br>
            Aspartete aminotransferase----- slight increase<br>
            Alanine gamma-glutamyl transferase ---marked increase<br>
            This is most consistent with :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Acute hepatitis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Osteitis fibrosa
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Chronic hepatitis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Obstructive Jaundice
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In which of the following disease states is conjugated bilirubin a major serum component?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Biliary obstruction
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Hemolysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Neonatal jaundice
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Erythroblastosis fetalis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What is the GFR and its interpretation of a 28 yr.old male patient given the following laboratory  data.<br>
            Urine creatinine    250 mg/dl<br>
            Serum creatinine    3.0  mg/dl<br>
            Urine volume   500  ml after 24 hours of collection<br>
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />40 ml/min., decreased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />50  ml/min., decreased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />75 ml/min., increased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />110 ml/min., increased
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            An elevated creatinine value is most likely to be accompanied by which of the following results?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Elevated BUN, 10x  creatinine value
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Elevated UA, 10x creatinine value
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Normal BUN
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Normal Uric acid
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In a patient with suspected primary hyperthyroidism associated  with Grave’s Disease, one would expect the following laboratory serum results : free thyrozine  (FT4)____________,Thyroid Hormone Binding Ratio (THRB)____________, and Thyroid-stimulating hormone (TSH)______________
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Increased, decreased, increased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Increased,decreased, decreased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Increased, increased,decreased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Decreased, decreased, increased
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Because of infertility problems a physician would like to determine when a woman  ovulates. The physician orders serial assays of plasma progesterone, From these assays how can the physician recognize when ovulation occurs?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />After ovulation progesterone rapidly increases
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />After progesterone rapidly decreases
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Right before ovulation, progesterone rapidly increases
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />There is a gradual  steady increases in progesterone throughout the menstrual cycle.
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A  sudden shift in daily values in a Q.C chart is likely to be the result  of:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Changing  an operating technologist
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Replacing the instruments sample aspiration probe
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Changing the spectrophotometer lamp In the middle of the sample run.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Running new standards concurrently  with the current standard  lot.
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A 21 year –old man  with nausea, vomiting and jaundice has the laboratory findings:<br><br></b>
            <p style="float:left; margin-right:19px" >
                Total serum bilirubin level        <br>             
                Conjugated  serum bilirubin level  <br>          
                Urine urobilinogen                 <br>                      
                Fecal Urobilinogen                 <br>                 
                Urine bilirubin                    <br>                     
                AST                                <br>                        
                Alkaline Phosphatase                         
            </p>
            <p>
                8.5 mg/dl (normal 0-1.0)<br>
                6.1 mg/dl(normal 0-0.5)<br>
                Increased<br>
                Decreased<br>
                Positive<br>
                300 U/L (normal, 0-50U/L)<br>
                170 U/L (normal, 0- 150 U/L)<br>
            </p>
            <p>These can best be  explained  as representing”</p>
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Unconjugated hyperbilirubinemia, probably due to hemolysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Unconjugated hyperbilirubinemia,probably due to toxic liver damage
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Conjugated hyperbilirubinemia, probably due to biliary tract disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Conjugated hyperbilirubinemia, probably due to hepatocellular obstruction.
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A 57 –year old female patient with edema, hypertension and electrolyte imbalance has the following laboratory results:<br><br></b>
            <p style="float:left; margin-right:19px" >
                BUN                  <br>             
                Creatinine  <br>          
            </p>
            <p>
                -  120 mg/dl<br>
                - 10 mg/dl<br>
            </p>
            <p>This clinical  condition implies:</p>
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Renal Calculi Obstruction
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Pre-renal Azotemia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Post renal Azotemia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />True renal Azotemia
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Thyroid releasing hormone (TRH) is given to a patient. Serum thyroid- stimulating hormone (TSH)  levels are taken before and after the injection and the values are the same – low. This patient probably has which of the following disorders ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Primary Hypothyroidism’
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Secondary hypothyroidism
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Tertiary hypothyroidism
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Iodine deficiency
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The presence of  a very high titer for antithyroglobulin antibodies and the detection  of antimicrosomal antibodies  is highly suggestive of what disorder?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Hashimoto’s Thyroiditis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Multinodular goiter
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Pernicious anemia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Thyroid Adenoma
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
                Which of these are classified as random errors?
                <ul >
                    <li>1.  Mislabeling of sample</li>
                    <li>2.  Improper mixture of sample and reagent</li>
                    <li>3.  Sample instability</li>
                    <li>4.  Deterioration of reagent</li>
                </ul>
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />1 AND 2
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />3 and 4
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />1,2 and 3
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />all  of the above
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A patient is presented with addison’s disease. Serum sodium and potassium analyses are done. The results would reveal :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Normal sodium, low potassium levels
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Low sodium , low potassium levels
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Low sodium , high potassium levels
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />High sodium, low potassium levels
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the ff ions is considered as an important enzymes activator  :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />CALCIUM
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Sodium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Chloride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Phosphorous
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following statements is true of enzyme?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Not all enzymes are protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Enzymes alter the equilibrium constant of the reaction
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Enzymes do not alter the rate of reaction
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Enzyme activity is independent of temperature and pH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following conditions is the cause of hyperbilirubinemia in  impaired conjugation of bilirubin?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Hemolytic anemia’
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Gilbert’s syndrome
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Crigler- Najar syndrome
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Dubin –Johnsons syndrome
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following test has the best predictive value on the third day following onset or myocardial infarction?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />LD
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />CK
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />AST
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />ALT
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Elevation  of serum activity in patients with myocardial infarction, skeletal muscle diseases, pulmonary diseases, CNS disease, hypothyroidism as wella sinall patients following vigorous exercise and intramuscular injections is characteristics of which of the following enzymes ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />LD
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />CK
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />AST
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />ALT
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of these is a good practice use of laboratory coat ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Never  leave laboratory coat in the workplace
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Use secondary laboratory coat when performing phlebotomy outside the laboratory
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Use laboratory coat only when inside the laboratory
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Laboratory coat must be cleaned at home before coming to the workplace
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What is the Normality  of a 3.6 M Sulfuric acid solution given the molecular weights?( H=1, S=32, O-16)
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />1.8 N
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />3.6N
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />4.9 N
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />7.2 N
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            How many grams of HCL is used to prepare 250 ml of a 4.8 solution of HCL? (H-1, Cl-35.5 )
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />36  G
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />36.5 g
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />40 g
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />43.8 g
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What is the molarity of a 2 .5 N  solution of NaOH?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />1.50 mol/l
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />2.5 mol/l
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />3.35 mol/l
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />4.5 mol/l
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What factors  are considered  as risk  factors  for atherosclerotic  disease.<br>
            1.High LDL chole<br>   2 .High  HDL chole<br> 3. Low LDL chole<br> 4. Low HDL chole
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />1 & 3
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />2 & 4
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />3 & 2
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />1 & 4
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The KjeldahL technique  is the reference method for the determination of serum total protein. This method is based on:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Quantitation of peptide bonds
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Determination of refracrtive index of protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Quantitation of the Nitrogen content of protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Turbidity measurement of precipitated protein
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The SI unit  of bilirubin  is:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />mgs/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" /> mEq/l
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />umol/l
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />mmol/l
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Product of   the catabolism  of hemoglobin:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Acid phosphate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Bilirubin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Total protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Ammonia
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In   Crigler- Najjar  syndrome, the defect in bilirubin  metabolism lies in:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Uptake  of bilirubin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Absence of UDP-  glucuronyl  transferase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Obsruction in billiary passage
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Increase  red cell destruction
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Jaffe’s reaction  in creatinine  utilizes
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Alkaline   hypochlorite solution
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Phenol  -nitroprusside solution
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Sulfuric  acid & acetic anhydride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Picric acid &   NaOH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Relatively   accurate  & useful  measure  of glomerular filtration:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Creatinine  Clearance
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Urea Clearance
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Inulin Clearance
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />PSP  Clearance
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A lipid profile  would include:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Lipoprotein, Total protein, Cholesterol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Triglycerides, Cholesterol, Albumin/ Globulin ratio
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Cholesterol, Phosphorous,Triglycerides
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Lipoprotein, Triglycerides, Cholesterol
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following  plasma constituents  is NOT normally found in the glomerular filtrate.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Potassium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Sodium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Creatinine
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Reagent used in Evelyn- Malloy   method  of determining  Bilirubin:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Diazo reagent
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Biuret reagent
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Diluted sulfuric acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />10%  NaOH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type of hypolipoprotenemia charactrized by a near absence of LDL or betalipoprotenemia in serum due a defective synthesis of Apo B.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Tangier’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Bassen Kornzweig syndrome
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Lipoprotein Xd
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />A&B
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The ratio  of  BUN to  CREATININE:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />5:10
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />10:1
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />15: 1
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />20 :1
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The lipoprotein which is believed to have a protective function against cholesterol deposition in a blood vessel wall:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Chylomicron
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />LDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />HDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Major  protein component  of HDL   present in small  amount of  intestinal chylomicrons:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Apo  D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Apo  C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Apo  B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Apo  A
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Enzyme  that  catalyzes  the esterification   of cholesterol:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="1" />Lipoprotein- lipase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="2" />Lecithin : Cholesterol  Acyltransferase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="3" />Cholesterol Oxidase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"   value="4" />Lipolytic  lipase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            It  is known as CLEARING  FACTOR  ,which hydrolyzes  protein-bound  triglycerides, specially in post heparin  specimen
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Lipoprotein lipase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Peptidase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Aminopeptidase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Lipase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The  GOOD  CHOLESTEROL
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Chylomicron
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />LDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following substances is markedly elevated in gout as a result of a disturbance in purine bodies:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Albumin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Creatinine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Urea
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Uric acid
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            An abnormal protein which is present in conditions such as in Liver impairment, obstruction  of hepatic cell ducts:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Lipoprotein X
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Albumin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />delta- globulin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A primary type of hyperlipoprotenemia, moderate elevation of plsma triglyceride without elevation of    cholesterol in obese patients with hyperglycemia & hyperuricemia.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Type  II A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Type IV-  Familial hypertriglyceridemia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Typer IV  Remnant hyperlipedemia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Type IV  Combined Hypercholesterolemia
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Creatinine   clearance  is useful  in assessing:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Renal  glomerular filtration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hepatic function
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Renal  blood flow
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Function of   the pancreas
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In serum protein electrophoresis, when a barbital buffer of  pH 8.6   is employed,  the protein  fraction that   migrates  the fastest toward  the anode is:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Albumin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Alpha-1 globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Beta- globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Gamma- globulin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Milky  plasma  maybe due to the presence of  :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Chylomicron
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hemolysis  due to  the presence  of RBC
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Excess amount of carbohydrates
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />excess  amount of urea
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Kernicterus is an abnormal  accumulation of  bilirubin  in:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Brain tissue
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Heart tissue
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Muscle tissue
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Kidney tissue
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type of support media   available for serum  electrophoresis :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Agar gel
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Starch gel
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Cellulose acetate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />all  of the above
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Increased  levels  of  direct  & indirect bilirubin are seen in:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Bile duct blockage
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hemolytic disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Hematoma resorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />A  & B
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A number  of compounds in human serum contain nitrogen,others are  non protein –nitrogen compounds.   What is the compound that comprises  the majority of the non-protein nitrogen  fractions in serum?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Creatinine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Uric acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Creatine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Urea
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The amount  of cholesterol in  LDL   Is  about:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />15%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />30%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />70%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />45%
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In Evelyn- Malloy  method  diazo reagent  is used for bilirubin diglucoronide:
            Diazo  reagents are:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Sodium nitrite
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Sulfanilic acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />hydrochloric acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" /> A,B &C
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            According to the expert Committee on the Diagnosis and Classification of Diabetes Mellitus, as of 1999 the new Fasting blood glucose value that will confirm that the patient is Diabetic is:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />120 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />126 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />140mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" /> 200 mg/dl
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following glucose load was recommended by the WHO to be utilized for an oral glucose tolerance test?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />50 grams
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />75 grams
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />100 grams
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />150 grams
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What should be the  range Hgb A1C level of people without Diabetes?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />1&3
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />2 &4
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />1,2,&3
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />4 only
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is reason  why Hexokinase is the generally accepted reference method for glucose determination ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />It uses G6 PD enzyme that helps inhibit interfering effect of oxidizing substances
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />It uses peroxidase enzyme that is inhibited by ascorbic acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />It uses electrode to conduct electricity during the reaction
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />It uses chromogen that can accepts ozygeny
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Childhood onset diabetes characateristically
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Is precipitated by obesity
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Is progressive
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Is associated  with insufficiency of insulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Is never  associated with even a brief remission
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is diagnostically increased in acute pancreatitis?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />AMYLASE
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Creatinine Kinase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Alkaline phosphatase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Lactate dehydrogenase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In muscular dystrophy, which of the following enzymes will be most useful for evaluation?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Lactate dehydrogenase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Creatinine Kinase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Aldolase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Alkaline Phosphatase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following can employ a polagraphic oxygen electrode
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Hexokinase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Glucose hexokinse
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />O-toluidine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Nelson somogyi
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            When measuring enzyme activity, if the instrument is operating 50 C lower than the temperature prescribed for the method, how will the results be affected?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LOWER THAN EXPECTED
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Higher than expected
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Varied, showing no particular pattern
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />All will be clinically abnormal
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following enzymes, catalyzes the transfer of amino group causing  the interconversion of aminoacids and  alpha- oxoacids?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />amylase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />aspartate transaminase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />alkaline phosphatase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Lactate dehydrogenase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            When separated by electrophoresis, the most anodic (fastest –moving) fractions of the lactate dehydrogenase isoenzymes is elevated in the presence of which of the ff conditions:?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />ACUTE VIRAL HEPATITIS
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Pernicious anemia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Intramuscular injectionse
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Skeletal muscle injury
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A 4 year old woman complain of fatigue, heat intolearnce,hair loss,. Serum thyroxine and I -T3  resin uptake are abnormally low. What test would confirm that this is a pituitary deficiency?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />FREE T3
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />FREE T4
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Thyroglobulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />TSH
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Ninety percent of the copper present in the blood is bound to :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Transferrin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Ceruloplasmin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Albumin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Cryoglobulin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Blood samples  were collected  at the beginning of  an exercise  class and after 30 minutes of aerobic activity , which of the following would be most consistent wth the post exercise samples?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Normal Lactic acid, low pyruvate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Low lactic acid, elevated pyruvaten
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Elevated lactic  acid, low pyruvate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Elevate lactic acid, elevate d pyruvate
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Thin Layer Chromatography is of particular use in the identification of :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Lipids
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Drugs
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Inorganic ions
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Enzyme inhibitors
                    </label>
                </li>
            </ul>
        </li>
        <table style="width:250px; " >
            <tr>
                <td></td>
                <td>TAG</td>
                <td>CHO<br>L</td>
                <td>LDL</td>
                <td>VL<br>DL</td>
                <td>CHYLO</td>
            </tr>
            <tr>
                <td>a</td>
                <td>Inc</td>
                <td>N/inc</td>
                <td>Dec.</td>
                <td>inc</td>
                <td>Inc</td>
            </tr>
            <tr>
                <td>b</td>
                <td>N</td>
                <td>inc</td>
                <td>Inc</td>
                <td>N</td>
                <td>N</td>
            </tr>
            <tr>
                <td>c</td>
                <td>inc</td>
                <td>inc</td>
                <td>Inc</td>
                <td>N</td>
                <td>N</td>
            </tr>
            <tr>
                <td>d</td>
                <td>inc</td>
                <td>Inc</td>
                <td></td>
                <td>Inc</td>
                <td>N/inc</td>
            </tr>
            <tr>
                <td>e</td>
                <td>inc</td>
                <td>N/inc</td>
                <td>N/inc</td>
                <td>Inc</td>
                <td>N</td>
            </tr>
            <tr>
                <td>f</td>
                <td>inc</td>
                <td>inc</td>
                <td>N/dec</td>
                <td>inc</td>
                <td>inc</td>
            </tr>
        </table>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 1 (familial LPP lipase Def.)
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />E
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 2a (familial Hypercholesterolemia)
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />E
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 2B - mixed defect (combined hyperlipidemia )
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />E
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 3 Familial Dysbetalipoproteinemia
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />E
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 4 Hypertriglycedemia
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />E
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 5
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />A
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />B
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />C
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />D
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />E
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />F
                    </label>
                </li>
            </ul>
        </li>
        <br>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Also known as Beta lipoprotein or bad cholesterol
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />IDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Transfer triglycerides from the liver to peripheral tissue
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />IDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Also known as pre-beta lipoprotein
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />IDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Transfer dietary cholesterol to peripheral tissues
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />VLDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />IDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Largest and least dense ,produced in the intestine, Delivery of dietary lipids to hepatic and peripheral cells
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Chylomicrons
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />HDL
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />IDL
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Refractometry
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Digestion of protein ,measurement of nitrogen content.
                    <br>Reference method .Assume average nitrogen content of 16%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Measurement  of refractive index due to solutes in serum (velocity in air and water)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Formation of violet –colored chelate between Cu2+  ions and peptide bonds
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />Protein binds to dye and causes a special shift in the absorbance of maximum dye
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Kjeldahl
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Digestion of protein ,measurement of nitrogen content.
                    <br>Reference method .Assume average nitrogen content of 16%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Measurement  of refractive index due to solutes in serum (velocity in air and water)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Formation of violet –colored chelate between Cu2+  ions and peptide bonds
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />Protein binds to dye and causes a special shift in the absorbance of maximum dye
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Dye binding
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Digestion of protein ,measurement of nitrogen content.
                    <br>Reference method .Assume average nitrogen content of 16%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Measurement  of refractive index due to solutes in serum (velocity in air and water)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Formation of violet –colored chelate between Cu2+  ions and peptide bonds
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />Protein binds to dye and causes a special shift in the absorbance of maximum dye
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Biuret
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Digestion of protein ,measurement of nitrogen content.
                    <br>Reference method .Assume average nitrogen content of 16%
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="2" />Measurement  of refractive index due to solutes in serum (velocity in air and water)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="3" />Formation of violet –colored chelate between Cu2+  ions and peptide bonds
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"  value="4" />Protein binds to dye and causes a special shift in the absorbance of maximum dye
                    </label>
                </li>
            </ul>
        </li>
        <br>
        <!-- <table style="width:350px; " > -->
        <!-- <tr>
            <td></td>
            <td>Principle</td>
                
            </tr>
            <tr>
            <td width="9%">a</td>
            <td>Digestion of protein ,measurement of nitrogen content<br>
            
                Reference method .Assume average nitrogen content of 16%
            </td>       
            
            </tr>
            <tr>                    
            <td>b</td>
            <td>Measurement  of refractive index due to solutes in serum (velocity in air and water)
            </td>       
            
            </tr>
            
            <tr>                    
            <td>c</td>
            <td>Formation of violet –colored chelate between Cu2+  ions and peptide bonds</td>      
            </tr>
            
            <tr>                    
            <td>d</td>
            <td>Protein binds to dye and causes a special shift in the absorbance of maximum dye</td>       
            </tr>
            
            </table> -->
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following isolates light within a narrow region of the spectrum?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Photomultiplier
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Monochromator
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Photovoltaic cell
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Detector
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            When performing spectrophotometer quality control checks, what is the holium oxide gas filter used to asses?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Linearity
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Stray light
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Absorbance accuracy
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Wavelength accuracy
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In an electrolytic cell, which of the following is the half cell where reduction takes place?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Anode
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Cathode
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Combination electrode
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Electrode response
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which chromatography system is commonly used in conjuction with mass spectrophotometry?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />High performing liquid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Ion exchange
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Partition
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Gas liquid
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is material known composition available in highly purified form?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Standard
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Control
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Technical reagent
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Test analyte
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Type 1 diabetes mellitus may be described by all of the following expect:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Insulin deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Association of destruction of b cells of pancrease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Ketoacidosis prone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Occurs more frequently in adults
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Bile acids that are synthesized in the liver are derived from what substance?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Bilirubin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Fatty acids
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Cholesterol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Triglycerides
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The VLDL fraction primarily transports what substance?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Cholesterol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Chylomicrons
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Triglycerides
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Phospolipidis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In what condition would an increased level of serum albumin is expected?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Malnutririon
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Acute inflammation
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Dehydration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Renal disease
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            All of the following plasma proteins are manufactured by the liver except?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Albuminn
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Haptoglubin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Fibrinogen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />IgG
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The presence of the only slight visible hemolytic will significantly increase the serum level of which of the following electrolytes?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Sodium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Potassium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Chloride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Bicarbonate
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Enzymes frequently assayed that aid in the assessment of liver function includes all of the following except?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />ALT
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />CK
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />ALP
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Ggt
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is used in the treatment of depression?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Potassium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Lithium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Calcium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Chloride
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The weight of the fluid compared with the weight of the identical volume of water is?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Specific gravity
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Molecular weight
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Absolute density
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Equivalent density
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />Absolute weight
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In determining serum chloride, the serum and cells of clotted blood should be separated as soon as possible to avoid the adverse effects of:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Urea
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Bicarbonate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Phosphorus
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Calcium
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The physical law describing the relationship of gram molecular weight and the number of atom is?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Charles’ law
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Beer’s law
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Avogadro’s law
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Dalton’s law
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Chornic renal disease can result in generalized kidney failure that is evidence of 
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Glumerular disfunction
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Carbonic anghydrase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Catalase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Carbamyl synthethase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Carbon dioxide is converted to carbonic acid in the body by action of?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Carbon oxidae
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Carbonic anhydrase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Catalase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Carbon monoxide
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Calcium and phosphate metabolism is regulated by?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Thyroid gland
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Parathyroid glands
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Islet of langerhans
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Adrenal glands
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Fibrinogen, prothrombin, albuminand some of the globulin are formed in
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Bone marrow
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Kidney
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Liver
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Stomach
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The somoygi method for amylase assay based on the measurement of
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Glucose
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Reducing sugar
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Starch remaining
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Amylopectin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Triglycerides are composed of fatty acids hooked in what type of linkage to glycerol?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Alchohol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Ester
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Ketone
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Most circulating T3 and T4 are bound to
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Albumin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />TBG
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Ceruloplasmin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Thyroglobuline
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Parathyroid hormone secretion is regulated by?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Dietary calcium intake
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Ionized calcium levels in serum
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Total serum calcium level
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Cyclic AMP level
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A very high uric acid is found in a patient without joint pain. Which other lab test is indicated?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Calcium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />BUN
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Lipase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Bone Marrow
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Glycated hemoglobin
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Insulin
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Albumin
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Glucose oxidase
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            CK-MB
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Cholinesterase
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Hypoglycemia
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Hexokinase
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Troponin
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            ALT
            </b>
            <ul>
                <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Reference method for qualifying serum glucose</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Synthesized by B-cells of pancrease</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Protein elevated in myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Enzymatic glucose method requiring perozidase</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Serum glucose less than 55 mg/dl</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Enzyme elevated in hepatitis</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Isoenzyme quantified in acute myocardial infacrtion</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Measured in urine to monitor diabetic complications</label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Enzyme enhibited by organic insecticides </label></li>
            <li><label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Assessment of plasma glucose during previous 6 to 8 we</label></li>
            
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            It  is known as CLEARING  FACTOR  ,which hydrolyzes  protein-bound  triglycerides, specially in post heparin  specimen
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Lipoprotein lipase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Peptidase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Aminopeptidase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Lipase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
                What is the GFR and its interpretation of a 28 yr.old male patient given the following laboratory  data.         
                <br><br>
                <p style="float:left; margin-right:19px" >
                    Urine creatinine        <br>             
                    Serum creatinine  <br>          
                    Urine volume                 <br>                      
                </p>
                <p>
                    250 mg/dl<br>
                    3.0  mg/dl<br>
                    500  ml after 24 hours of collection<br>
                </p>
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />40 ml/min., decreased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />50  ml/min., decreased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />75 ml/min., increased
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />110 ml/min., increased
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The hormone that functions to promote sodium ion reabsorption by the kidney and thus  water retention is :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />aldosterone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />cortisol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />estrogen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />progesterone
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            For the hormones that are regulated by the pituitary gland, a signal is sent from the:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Hypothalamus to the pituitary gland in the form of releasing hormone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hypothalamus to the pituitary gland in the form of  stimulating  hormone
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            May detect the presence of the substanc through EMIT even up to 2 days after oral ingestion.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />methadone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Methaqualone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Prophoxyphene
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />None of the above
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Why are the total thyroxine (T4) levels increased a pregnant women  and those who take oral contraceptives?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />inappropriate iodine metabolism
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />changes in tissue use
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />changes in concentration of thyroxine –binding globulin (TBG)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />changes in thyroglobulin synthesis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Thyroid releasing hormone (TRH) is given to a patient. Serum thyroid- stimulating hormone (TSH)  levels are taken before and after the injection and the values are the same – low. This patient probably has which of the following disorders ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Primary Hypothyroidism’
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Secondary hypothyroidism
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Tertiary hypothyroidism globulin (TBG)
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Iodine deficiency
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following test has the best predictive value on the third day following onset or myocardial infarction?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />LD
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />CK
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />AST
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />ALT
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In Crigler- Najjar  syndrome, the defect in bilirubin  metabolism lies in:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Uptake  of bilirubin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Absence of UDP- glucuronyl transferase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Obsruction  in billiary  passage
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Increase red cell destruction
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Creatinine   clearance  is useful  in assessing:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Renal  glomerular filtration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hepatic function
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Renal  blood flow
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Function of   the pancreas
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In serum protein electrophoresis, when a barbital buffer of  pH 8.6   is employed,  the protein  fraction that   migrates  the fastest toward  the anode is:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Albumin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Alpha-1 globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Beta- globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Gamma- globulin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Aldosterone
            </b>
            <ul>
                ;
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Etriol
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            CAH (Congenital adrenal Hyperplasia)
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Testosterone
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Epinephire
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Thyroxine
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Hyponatremia
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            calcium
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            phosphate
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            ;
            <b>
            Scurvy
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="1" />a.  Most potent androgen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="2" />b.  Parathyroid hormone mobilize this analyte from bone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="3" />c.  Decreased in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="4" />d.  Quantified serially to asses fetoplacental well being
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="5" />e.  Parathyroid hormone acts on renal tubules to decrease this analyte’s reabsorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="6" />f.  Caused by vitamin C deficiency
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="7" />g.  Increased in pheochromocytoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="8" />h.  Thyroid hormone in highest serum concentration
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="9" />i.  Change in an electrolyte occurring in Addison’s disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="10" />j. Inherited enzyme defect of 21- hydroxylase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            ;
            <b>
            If the total bilirubin is 3.1 mg/dl  and the conjugated bilirubin is 2.0 mg/dl, the unconjugated bilirubin
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="1" />0.5 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="2" />1.1 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="3" />2.2 mg/dl
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i ?>"    value="4" />5.1 mg/dl
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following drugs formed purple color when combined with ferric ions ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Saicylate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Acetaminophen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Alcohol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Barbiturates
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What lipid component provides the starting material for the synthesis of various steroid hormone?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Fatty acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Triglyceride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Cholesterol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Phospholipid
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            What assistance does an external quality assurance program provide for the laboratory?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Means to correlate test performed by different departments within the same laboratory.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Evaluation of its performance by comparison with other laboratories using the same method
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Data checks with previous tests on the same patient
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Limits for reference intervals
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The KjeldahL  technique  is the reference method for the determination  of serum total protein. This method is based on :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Quantitation of peptide bonds
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Determination of refracrtive index of protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Quantitation of the Nitrogen content of protein
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Turbidity measurement of precipitated protein
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Enzyme  that  catalyzes  the esterification   of cholesterol:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Lipoprotein- lipase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Lecithin : Cholesterol  Acyltransferase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Cholesterol Oxidase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Lipolytic  lipase
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Increased  levels  of  direct  & indirect bilirubin are seen in:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Bile duct blockage
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hemolytic disease
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Hematoma resorption
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />A  & B
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following can employ a polagraphic oxygen electrode
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Hexokinase
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Glucose hexokinse
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />O-toluidine
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Nelson somogyi
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Blood samples  were collected  at the beginning of  an exercise  class and after 30 minutes of aerobic activity , which of the following would be most consistent wth the post exercise samples?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Normal Lactic acid, low pyruvate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Low lactic acid, elevated pyruvate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Elevated lactic  acid, low pyruvate
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Elevate lactic acid, elevate d pyruvate
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is reason  why Hexokinase is the generally accepted reference method for glucose determination ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />It uses G6 PD enzyme that helps inhibit interfering effect of oxidizing substances
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />It uses peroxidase enzyme that is inhibited by ascorbic acid
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />It uses electrode to conduct electricity during the reaction.
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />It uses chromogen that can accepts ozygen
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            In serum protein electrophoresis, when a barbital buffer of  pH 8.6   is employed,  the protein  fraction that   migrates  the fastest toward  the anode is:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Albumin 
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Alpha-1 globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Beta- globulin
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Gamma- globulin
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The presence of the only slight visible hemolytic will significantly increase the serum level of which of the following electrolytes?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Sodium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Potassium
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Chloride
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Bicarbonate
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of these methods measure the Iodine trapping function of the thyroid gland ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Thyroid Scan
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Thyroxine Index
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Resin T3 Uptake
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Radio active Iodine Uptake
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The thyroid gland from a patient with grave’s disease shows diffuse enlargement.  Serum T3, T4 levels are marked;y elevated.  The thyroid gland will most likely show:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Dysplasia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Hypoplasia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Hyperplasia
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Metaplasia
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A 12 year old boy present with continuous high grade fever and sore throat.  Examination of the tonsils show marked enlargement, redness and purulent exudates on the surface.  This is an example of which type of inflammation?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Acute
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Chronic
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Fibrinous
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Hemorrhagic
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The principal source of the hormone testosterone in the male is from the:
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Anterior pituitary gland
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Insterstitial cells of Leydig
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Prostate gland
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Seminal vesicle
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            A 2.5 cm solid tumour is excised from the left breast from a 22 year old medical technologist, and sent for frozen section.  The pathologist reports diagnosis of a “Benign neoplasm”.  Which of the following histopathology is compatible with such findings?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Angiosarcoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Fibroadenoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Infiltrating lobular carcinoma
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Invasive ductal carcinoma
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            VLDL
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        </li>    
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Bile Acids
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Chylomicrons
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Lipase
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Ketone
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Cholesterol
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            HDL cholesterol
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Alkaline phosphatase
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Protoporphyrin IX
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Porphobilinogen
            </b>
            <ul>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.   Risk factor for coronary artery disease</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.   Cause of postprandial lipemia</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.   Primary carrier of triglycerides in the blood</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.   Immediate precursor of heme formation</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.   Excess amount excreted in urine in acute intermittent porphyria</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.   Need for hydrolyasis of triglycerides</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.   Beta- hydrobutyric acid</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.   Increase amount found in serum in obstructive jaundice</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">i.   Synthesized in the liver from cholesterol</label>
                </li>
                <li>
                    <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="10">j.  Primary component of beta lipoprotein</label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>


        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Sodium
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Potassium
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Ferritin
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Transferrin
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Bicarbonate
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Base excess
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Respiratory acidocis
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Tacrolimus
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Alkaline phosphatase
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            NAPA
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Ceruloplasmin
            </b>
            <ul>
                <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="1">a.  Active metabolite of antiarrhythmic drug procainamide<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="2">b.  Measure of non respiratory buffers of the blood<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="3">c.  Primary storage form of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="4">d.  Renal threshold of 110-130 mmol/L<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="5">e.  Associated with increase in PCO2<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="6">f.  Total CO2 is the sum of carbonic acid and ____<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="7">g.  Hemolysis cause by false increase in the serum electrolyte<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="8">h.  Transport protein of iron<br></label>
            </li>                
            <li>
                <label><input type="radio" class="answer-radio" name="question-<?php echo $i; ?>" value="9">i.  Used as an immunosuppressant in organ transplantation. </label>
            </li>  
            </ul>
        </li>
                          
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The hormone that functions to promote sodium ion reabsorption by the kidney and thus  water retention is :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />aldosterone
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />cortisol
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />estrogen
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />progesterone
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of the following is a diagnostics  test for cystic fibrosis ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Sweat chloride Analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Sodium analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Bicarbonate analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Phosphate analysis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The expected blood gas results for a patient in chronic renal failure would match the pattern of :
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Metabolic acidosis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Respiratory  acidosis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Bicarbonate analysis
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Respiratory alkalosis
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Reinsch’s test is used for screening toxic concentration of  which of the following substances.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Bismuth & Mercury
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Mercury & Cyanide
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Cyanide & Bismuth
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Arsenic & Cyanide
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            The toxic substance can be analyzed using  hair & nails because of its affinity with the protein.
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />ARSENIC
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Bismuth
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Lead
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Mercury
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
        <?php if(in_array(++$i, $randomQuestion)): ?>
        <li class="question-item" id="biochemistry-<?php echo $i; ?>">
            <b>
            Which of these methods measure the Iodine trapping function of the thyroid gland ?
            </b>
            <ul>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="1" />Thyroid Scan
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="2" />Thyroxine Index
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="3" />Resin T3 Uptake
                    </label>
                </li>
                <li>
                    <label>
                    <input type="radio" class="answer-radio" name="question-<?php echo $i; ?>"    value="4" />Radio active Iodine Uptake
                    </label>
                </li>
            </ul>
        </li>
        <?php endif; ?>
</div>
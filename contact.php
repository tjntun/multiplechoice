<?php include 'inc/header.php'; ?>
<h3>Please use the following email address for all the communications:</h3>

<a href="mailto:freemedtechreview@gmail.com">freemedtechreview@gmail.com</a>

<h3>Show Your Support</h3>
<ul style="text-align:center; width:60%; margin:0 auto;">
	<li>
		<p>• Give thanks via donation by Credit or Debit card through PayPal</p>
		<br>
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
			<input type="hidden" name="cmd" value="_donations">
			<input type="hidden" name="business" value="freemedtechreview@gmail.com">
			<input type="hidden" name="lc" value="US">
			<input type="hidden" name="item_name" value="Freemedtechreview">
			<input type="hidden" name="no_note" value="0">
			<input type="hidden" name="currency_code" value="USD">
			<input type="hidden" name="bn" value="PP-DonationsBF:btn_donateCC_LG.gif:NonHostedGuest">
			<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
			<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
		</form>
		<br>
		<br>
	</li>
	<li>• <a href="#">Advertise on this site</a></li>
</ul>
<?php include 'inc/footer.php'; ?>
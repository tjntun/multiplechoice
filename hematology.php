<?php include 'inc/random.php'; ?>
<div class='tab-item' id='tab-1'>
    <?php $i = 0; ?>

    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This/These are helpful laboratory investigations in the initial evaluation of anemia.  These are the basis of a methodical approach to anemia.<br>
        &nbsp;&nbsp;&nbsp;1.    Reticulocyte Count<br>
        &nbsp;&nbsp;&nbsp;2.    Peripheral Blood<br>
        &nbsp;&nbsp;&nbsp;3.    RBC Count<br>
        &nbsp;&nbsp;&nbsp;4.    Hemoglobin<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-1" value="1" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-1" value="2" />1 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-1" value="3" />2 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-1" value="4" />3 and 4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The reticulocyte count is found high in the following conditions<br>
        &nbsp;&nbsp;&nbsp;1.    Blood loss<br> 
        &nbsp;&nbsp;&nbsp;2.    Response to treatment to iron, folate B12 deficiency<br>
        &nbsp;&nbsp;&nbsp;3.  Hemolysis.<br>
        &nbsp;&nbsp;&nbsp;4.  4 only<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-2" value="1" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-2" value="2" />1,2 and 3 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-2" value="3" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-2" value="4" />4 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In hypoproliferative anemia, the following are observed.<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-3" value="1" />Primary marrow
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-3" value="2" />Low reticulocyte count
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-3" value="3" />Both
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-3" value="4" />Neither
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is an index of heterogeneity of cells size.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-4" value="1" />MCV
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-4" value="2" />RDW
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-4" value="3" />CBC
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-4" value="4" />MCHC
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        If the MCV is high and RDW is also increased, what conditions from below would fit 
        the given findings?<br>
        1. Folate /B12 deficiency<br>       
        2. Immune Hemolytic Anemia<br>
        3. Cold Agglutinins<br>
        4. Iron deficiency Anemia<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-5" value="1" />1  and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-5" value="2" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-5" value="3" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-5" value="4" />4 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the most frequently encountered anemias in hospitalized and ambulatory    
        patients?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-6" value="1" />Hypochromic anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-6" value="2" />Normochromic normocytic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-6" value="3" />Megaloblastic Anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-6" value="4" />None
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        These are common causes of iron deficiency anemia.<br>
        1.  Dietary deficiency<br>          
        2.  Chronic Blood Loss<br>
        3.  Malabsorption (sprue)<br>
        4.  Extravascular Hemolysis
        <br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-7" value="1" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-7" value="2" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-7" value="3" />1,2,3  and 4 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-7" value="4" />1 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are characteristics of ACD ( Anemia of Chronic Diseases)
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-8" value="1" />Low Serum Iron
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-8" value="2" />Low TIBC
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-8" value="3" />Decreased iron stores
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-8" value="4" />Low Transferrin Saturation
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which are more specific for Vitamin B12 deficiency?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-9" value="1" />Macrocytosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-9" value="2" />Macroovalocytic RBCs
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-9" value="3" />Normoovalocytic RBCs
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-9" value="4" />Microcytosis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are characteristics of folate and Vitamin B12 deficiency except one.<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-10" value="1" />Hyperplastic marrow with markedly ineffective     erythropoiesis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-10" value="2" />Non-segmented neutrophils, neutrophilia and thrombocytosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-10" value="3" />Macro-ovalocytosis with occasional H-J bodies and basophilic stippling.
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-10" value="4" />Megaloblasytic changes in proliferative cells of the mouth, gut, small intestine and cervix.
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are the processes/conditions that interrupt B12 absorption?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-11" value="1" />Gastric Atrophy
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-11" value="2" />After gastrectomy
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-11" value="3" />Chronic intravascular Anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-11" value="4" />latum infection
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Diagnostic criteria for aplastic anemia except:<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-12" value="1" />Peripheral pancytopenia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-12" value="2" />Hypocellular  bone marrow
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-12" value="3" />Platelet Count is 130  X 109 /L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-12" value="4" />CRC is less than 1%
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What blood products are needed when there is a life-threatening bleeding in a patient with aplastic anemia?<br>
        1.  Platelets   <br>            
        2.  RBCs    <br>
        3.  Leukocytes      <br>    
        4.  Plasma<br>
        <br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-13" value="1" />1 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-13" value="2" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-13" value="3" />3 and 4 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-13" value="4" />2 and 4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are characteristics of hemolytic anemia.<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-14" value="1" />Absence  of hemoglobinuria
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-14" value="2" />Shortened RBC survival
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-14" value="3" />Increased production of RBCs 7-8 fold
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-14" value="4" />Increased Retic count
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-14" value="5" />Indirect hyperbilirubinemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are basic types of RBC defects that lead to hemolysis in hereditary hemolytic anemia.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-15" value="1" />H T R
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-15" value="2" />Hemoglobin  abnormalities
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-15" value="3" />Membrane defects
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-15" value="4" />Enzymatic defects
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are membrane disorders causing  hereditary hemolytic anemia except:<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-16" value="1" />Elliptocytosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-16" value="2" />Spherocytosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-16" value="3" />Pyruvickinase
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-16" value="4" />Stomacytosis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are acquired hemolytic disorders resulting from  
        extravascular defects.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-17" value="1" />Physical agents
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-17" value="2" />Autoimmune Hemolytic Anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-17" value="3" />Malaria
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-17" value="4" />Hypersplenism
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-17" value="5" />G6PD
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In hereditary spherocytosis, lysis occurs in a  media with relatively
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-18" value="1" />high osmotic strength
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-18" value="2" />low  osmotic strength
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-18" value="3" /> Normal osmotic strength
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is the principal membrane protein found  decreased in hereditary spherocytosis.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-19" value="1" />Actin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-19" value="2" />Spectrin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-19" value="3" />Ankyrin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-19" value="4" />Glycophorin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the most common enzymatic defect in RBCs  that lead to hemolysis?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-20" value="1" />PK
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-20" value="2" />G6PD
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-20" value="3" />5&rsquo;  Nucleotidase
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        RBCs are vulnerable to oxidative attack when GSH  is depleted; oxidation results to precipitation of hemoglobin.  What      inclusion bodies can be seen that will  lead to the diagnosis of G6PD deficiency.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-21" value="1" />Heinz  bodies
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-21" value="2" />Howell Jolly Bodies
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-21" value="3" />Cabots Ring
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-21" value="4" />Maragliano body
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the most common enzymatic defect in the  erythrocyte Embden Meyerhof pathway.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-22" value="1" />PK
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-22" value="2" />G6PD
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-22" value="3" />5&rsquo; nucleotidase
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-22" value="4" />Maragliano body
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        There are two causes of symptomatic cyanosis,  the congenital methemoglobinemia and methemoglobin reductase deficiency.  Which is tramsimitted as autosomal dominat disorder.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-23" value="1" />Congenital   methemoglobinemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-23" value="2" />Methemoglobin  reductase deficiency
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Fragmentation hemolysis is characterized by the  appearance of the following except&gt;
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-24" value="1" />Helmet cells
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-24" value="2" />Burr cells
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-24" value="3" />Spehrocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-24" value="4" />Schistocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-24" value="5" />Schizocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-24" value="6" />Discocytes
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The followng are drugs that commonly cause  neutropenia.<br>
        1.  Chloramphenical<br>         
        2.  Phenotiazines<br>   3.  Antithyroid drugs<br>       4.  Antihistmaines<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-25" value="1" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-25" value="2" />1,2,3  and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-25" value="3" />1 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-25" value="4" />4 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following cells are seen in a condition known as leukoerythroblastosis.<br>
        1.  Myelocytes<br>          
        2.  Metamylocytes<br>   3.  Myeloblasts<br>     4.  Nucleated RBCs<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-26" value="1" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-26" value="2" />1,2  and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-26" value="3" />1,2  3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-26" value="4" />1 and 2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Atypical lymphocytes are seen in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-27" value="1" />EBV
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-27" value="2" />CMV
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-27" value="3" />Both
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-27" value="4" />Neither
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the typical cytogenetic marker of  CML.  ( Chronic myelogenous leukemia)  resulting in Philadelphia chromosome.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-28" value="1" />16:22  translocation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-28" value="2" />9:22 translocation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-28" value="3" />18:22 translocation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-28" value="4" />1:9 translocation   
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are seen in chronic myelogenous  leukemia except:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-29" value="1" />Juvenile neutrophils
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-29" value="2" />Ph Chromosome
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-29" value="3" />Increased  LAP
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-29" value="4" />Low LAP
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Patients presenting with large  spleen, fibrotic marrows and the presence of teardrop shaped erythrocytes on  the           peripheral blood film have what myeloproliferative disorder?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-30" value="1" />Myelofibrosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-30" value="2" />Polycythemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-30" value="3" />Immune Hemolytic Anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-30" value="4" />Siderablastic Anemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Platelet millionaires with a count of over  1,000,000/ul is seen in a myrloproliferative disorder known as:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-31" value="1" />Essential  thrombocytopenia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-31" value="2" />Glanzmann&rsquo;s disease 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-31" value="3" />Wiskott-Aldrich syndrome 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-31" value="4" />Thrombocytopathy
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        If the following are observed.  Cellular bone marrow aspirate wwith blasts is  &gt; 30% of nucleated RBCs (+)        peroxidase and (+) Sudan Black.  What leukemia is this based on FAB  classification?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-32" value="1" />Acute  myeloblastic leukemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-32" value="2" />Chronic myelogenous leukemia 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-32" value="3" />Reticuloendotheliosis leukemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-32" value="4" />Acute lymphoblastic leukemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Abnormal promyelocytes with reniform or (kidney  shaped) nuclei, bundles of Auer rods and with packed bright pink or purple  granules are seen in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-33" value="1" />Myeloblastic leukemia without maturation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-33" value="2" />Myeloblastic  leukemia with maturation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-33" value="3" />Myelomonocytic leukemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-33" value="4" />Hypergranular promyelocytic leukemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Give the type of leukemia based on given Wright'’'s stain morphology and staining characteristics.
        Cytoplasm, - abundant<br>
        Granules     - sometimes present<br>
        Auer rods   -  maybe present<br>
        Peroxidase and Sudan Black (+)<br>
        PAS           - (+)/ (-)</b><b><br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-34" value="1" />AML
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-34" value="2" />ALL
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-34" value="3" />Monocytic Leukemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-34" value="4" />Erythroleukemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What cytochemical feature is observed in hairy cell?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-35" value="1" />Alkaline Phosphatase
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-35" value="2" />Tartrate  resistant acid phosphatase
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-35" value="3" />Non-specific esterase
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-35" value="4" />PAS (+)
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the classic cell seen in a lymph node of patient'’'s with Hodgkin”s disease?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-36" value="1" />Hairy Cell
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-36" value="2" />Sezary Cell
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-36" value="3" />Reed-Sternberg Cell
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-36" value="4" />Gaucher Cell
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Characteristics of Reed-Sternberg Cells<br>
        1.   Large cell with 2 nuclei<br>
        2.   With distinct nucleolus in each nucleus<br>
        3.  Fibrosis does not exist<br>
        4.  Rare if overwhelmed by reactive lymph, polys and eosi.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-37" value="1" />1,2, 3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-37" value="2" />1 2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-37" value="3" />1, 2 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-37" value="4" />1  and 2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In nodular schlerosis, there is retraction of the cells surrounding the Reed-Sternberg cells during fixation.  What     kind of cell is produced?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-38" value="1" />Lacunar  cell
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-38" value="2" />Sezary cell
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-38" value="3" />Faggot cell
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are characteristics of Burkitt'’'s ;ymphoma cells
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-39" value="1" />They are B lymphocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-39" value="2" />Round  or Oval cell
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-39" value="3" />Basophilic Cytoplasm with vacuoles
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-39" value="4" />Stain (+) for fat
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the appearnce of a tissue affected by Burkitt'’'s lymphoma if it is replaced with a monotonous infiltrate of cells with interspaced macrophages.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-40" value="1" />crinkled “ Tissue Paper” appearance
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-40" value="2" />“ Starry sky” appearance
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-40" value="3" />“Owl'’'s eye appearance
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-40" value="4" />“ bundles of wood” appearance
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        These are observed in Secondary Hemostasis:<br>1.  Vessel wall interaction<br>          
        2.  Clotting factors activate each other<br>    3.  exemplified by Hemophilia A<br> 4.  deposition of fibrin around the
        platelet plug
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-41" value="1" />1, 2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-41" value="2" />2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-41" value="3" />1,2  3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-41" value="4" />2 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is impaired with a decrease presence of VWF at the site of endothelial damage?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-42" value="1" />Platelet aggregation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-42" value="2" />Platelet release reaction
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-42" value="3" />Secondary hemostasis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-42" value="4" />Platelet  Adhesion
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The main defect of Bernard-Soulier syndrome is
        </b>
        <ul>
            <li>
                <label>
                <input type="radio" name="43" value="1" />Reduced abnormal factor VIII:VWF
                </label>
            </li>
            <li>
                <label>
                <input type="radio" name="43" value="2" />Absence of platelet glycoprotein Ib, a receptor for VWF
                </label>
            </li>
            <li>
                <label>
                <input type="radio" name="43" value="3" />Absence of platelet glycoprotein Iib, IIIa, a receptor for VWF and fibrinogen.
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What drug will irreversibly inhibit platelet cyclooxygenase thar impair the function of platelets?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-44" value="1" />Penicillin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-44" value="2" />Epinephrine
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-44" value="3" />Persantin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-44" value="4" />Aspirin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In monitoring warfarin therapy, this test is used.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-45" value="1" />P.T.
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-45" value="2" />B.T.
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-45" value="3" />PTT
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-45" value="4" />CRT
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In monitoring heparin therapy, this test is used.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-46" value="1" />P.T.
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-46" value="2" />B.T. 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-46" value="3" />PTT
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-46" value="4" />CRT
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What hereditary disorder result in a prolonged PTT without bleeding?
        </b><br>
        <p style="float:left; margin-right:20px;">
            1.  hereditary factor XII deficiency<br>        
            2.  Fletcher factor deficiency      <br>    
        </p>
        <p>
            3.   Fitzgerald factor deficiency<br>
            4.   Factor X deficiency          <br>
        </p>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-47" value="1" />1,2 and 3                
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-47" value="2" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-47" value="3" />3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-47" value="4" />1 and 4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        When a prolonged PTT is identified, a mixing study is usually done to determine if normal plasma will correct it.  If  it not corrected the deficiency is:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-48" value="1" />Deficiency in intrinsic pathway
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-48" value="2" />Presence of inhibitor
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-48" value="3" />Deficiency in both intrinsic and extrinsic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-48" value="4" />Presence of activators
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        These are characteristics of lupus anticoagulant
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-49" value="1" />It is an acquired inhibotor
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-49" value="2" />Usually IgG against phospholipid moieties
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-49" value="3" />Causes a prolongation of the PTT and PT
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-49" value="4" />Associated with stroke and other thrombotic disease
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-49" value="4" />None
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        With bleeding, normal PTT but a prolonged PT is/are caused by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-50" value="1" />Factor VII deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-50" value="2" />Initiation   of coumarin anticoagulation 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-50" value="3" />Both
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-50" value="4" />Neither
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        With mild bleeding, prolonged PTT and normal PT is caused by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-51" value="1" />Factor VII deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-51" value="2" />Factor IX deficiency 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-51" value="3" />Factor VIII-C deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-51" value="4" />Factor V deficiency
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        With severe bleeding, prolonged PTT and normal PT is caused by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-52" value="1" />Factor VII deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-52" value="2" />Factor IX deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-52" value="3" />Factor VIII-C deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-52" value="4" />Factor V deficiency
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are the laboratory findings:<br>
        Peripheral blood smear . . . . decreased platelets with red cell fragmentation<br>
        PT and PTT. . . . . . . . . . . .  .prolonged<br>
        Fibrinogen and Platelet Ct.  decreased<br>
        Fibrin degradation products Increased<br>
        :<br>
        The above finding would tell that the patient is suffering of. 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-53" value="1" />DIC
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-53" value="2" />Hemophilia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-53" value="3" />Parahemophilia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-53" value="4" />Hemophilia B
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are prolonged with Dysfibrinogenemia except:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-54" value="1" />PT
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-54" value="2" />Euglobulin Clot Lysis Time
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-54" value="3" />PTT
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-54" value="4" />Thrombin time
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Patients receiving certain antibiotics develop prolongation of the PT and PTT.  What is the most likely cause?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-55" value="1" />Penicillin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-55" value="2" />Cloxacillin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-55" value="3" />Oflocillin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-55" value="4" />Amoxicillin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-55" value="5" />All of these
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-55" value="6" />none of these
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        These are cells that are poorly phagocytic and do not release lysozyme, but release peroxidase pyrogen and oxidase that neutralize histamine
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-56" value="1" />Basophils
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-56" value="2" />Eosinophils
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-56" value="3" />Neutrophils
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-56" value="4" />Lymphocytes
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Granules in red blood cells stained by Prussian Blue:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-57" value="1" />Peroxidase granules
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-57" value="2" />Alpha granules
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-57" value="3" />Hemosiderin granules
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-57" value="4" />H2SO4  monoesters granules
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A falsely elevated hematocrit is obtained using a defective centrifuge.  Which of the following will be affected?
        1.  MCV<br>         
        2.  MCHC<br> 3.  RDW<br>        4.   MCH<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-58" value="1" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-58" value="2" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-58" value="3" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-58" value="4" />4 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Cell indices obtained on a patient are as follows:<br>
        MCV = 72fl. <br>        
        MCH= 23pg<br>MCHC= 29g/dl<br>   
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-59" value="1" />Hypochromic normocytic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-59" value="2" />Hypochromic microcytic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-59" value="3" />Normochromic normocytic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-59" value="4" />Normochromic microcytic
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        ESR is not affected by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-60" value="1" />Rouleaux factor
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-60" value="2" />EDTA and Sodium Citrate
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-60" value="3" />Fibrinogen
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-60" value="4" />number of RBCs
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The effect of a buffer solution with a pH of 7.8 is:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-61" value="1" />Red cells are lyzed
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-61" value="2" />Red cells are blue
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-61" value="3" />Red cells are pink
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-61" value="4" />Red Cells agglutinate
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What substances within the reticuloycte are precipitated by supravital stain?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-62" value="1" />RNA
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-62" value="2" />DNA
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-62" value="3" />granules
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-62" value="4" />Nucleoli
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In electronic method of counting using the principle of electrical impedance, the cells produce:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-63" value="1" />light reflection
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-63" value="2" />laser beam of light
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-63" value="3" />voltage pulse
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-63" value="4" />fluorescence light
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In hemocytometer counting, the NRBCs could be confused for:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-64" value="1" />giant platelets
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-64" value="2" />megakaryocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-64" value="3" />monoblasts
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-64" value="4" />leukocytes
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are characteristics   of pernicious anemia<br>
        1.  Hypersegmented PMN  s   &nbsp; C.  Pancytopenia<br>
        2.  Macrocytic RBCs&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;D.  Increased Retic Count<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-65" value="1" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-65" value="2" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-65" value="3" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-65" value="4" />2 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The organ of the body acting as filter and perform pitting imperfections from the erythrocyte without destroying the integrity of the membrane.<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-66" value="1" />Liver
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-66" value="2" />Spleen
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-66" value="3" />Kidney
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-66" value="4" />Pancreas
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        These are the characteristics of iron deficiency anemia:<br>
        1.  hypochromic microcytic<br>       
        2.  increased platelet count<br>
        3.  increased TIBC<br>
        4.  Decreased Serum Iron
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-67" value="1" />4 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-67" value="2" />2 and 4  
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-67" value="3" />1,2 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-67" value="4" />1 3 and 4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Iron deficiency anemia and anemia due to chronic infections have the same characteristics as follows except one.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-68" value="1" />Total iron binding capacity is decreased
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-68" value="2" />Decreased serum iron level   
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-68" value="3" />Same RBC morphology
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-68" value="4" />Red Cell indices- all increased
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What blood picture does Thalassemia present?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-69" value="1" />Macrocytic hypochromic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-69" value="2" />Macrocytic, normochromic 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-69" value="3" />Hypochromic microcytic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-69" value="4" />Hypochromic normocytic
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Characteristic/s of congenital hereditary spherocytosis.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-70" value="1" />Intravascular hemolysis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-70" value="2" />Extravascular hemolysis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-70" value="3" />MCHC increased
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-70" value="4" />Increased Osmotic fragility
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are characteristics of spherocytes.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-71" value="1" />Decreased resistance to hypotonic solution 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-71" value="2" />Prone to splenic sequestration 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-71" value="3" />Thinner than normal RBCs
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-71" value="4" />Lose their deformability due to the defect in spectrin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Hgb H disease is a devere clinical expression of alpha thalassemia.  How many alpha gene is functioning out of four.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-72" value="1" />1 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-72" value="2" />2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-72" value="3" />3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-72" value="4" />4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which is the major hemoglobin found in Sickle Cell anemia?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-73" value="1" />Hgb S
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-73" value="2" />Hgb A
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-73" value="3" />Hgb A2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-73" value="4" />Hgb F
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following configurations is the characteristic of Hgb F?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-74" value="1" />gamma 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-74" value="2" />alpha 2 gamma 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-74" value="3" />Beta 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-74" value="4" />Alpha 2 beta 2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Autosplenectomy occurs in sickle cell anemia due to:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-75" value="1" />repeated infarcts to the spleen as a result of the overwhelming sickling phenomenon
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-75" value="2" />Spelnic pitting
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-75" value="3" />Amino acid substitution
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-75" value="4" />Hypersplenism
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are usually found in Hgb C disease<br>
        1.  Target cells<br>
        2.  Hgb crystals<br>
        3.  OFT- decreased of fragilitt, Increased resistance<br>
        4.  Lysine is substituted for glutamic acid at the beta chain
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-76" value="1" />1 and 2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-76" value="2" />3 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-76" value="3" />1, 2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-76" value="4" />1,2 ,3 and 4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The autohemolysis test is normal, sucrose hemolysis test is positive.  Which condition fits to the given laboratory findings?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-77" value="1" />PK deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-77" value="2" />PNH
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-77" value="3" />G6PD deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-77" value="4" />Hereditary spherocytososis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The autohemolysis test is positive and OFT gives also a postive result.  Which of the following conditions would fit the given lab.results?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-78" value="1" />G6PD
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-78" value="2" />PK deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-78" value="3" />PNH
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-78" value="4" />Hereditary spherocytosis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The autohemosisi test is positive and Fluorescence test is also positive.  Which of the following conditions would fit the given lab. results?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-79" value="1" />G6PD deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-79" value="2" />PK deficiency
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-79" value="3" />PNH
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-79" value="4" />Hereditary spherocytosis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The presence of Donath-Landsteiner antibody will cause hemolysis of red blood cells under this condition.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-80" value="1" />sample placed at 4oC
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-80" value="2" />sample placed at 4 deg. C and then warmed at 37 deg. C
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-80" value="3" />Sample placed at 37 deg. C
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-80" value="4" />Sample placed at 37 deg.C and then at 4 deg.C
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The increased amount of free hemoglobin in hemolytic anemia will:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-81" value="1" />Increase the amount of haptoglobin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-81" value="2" />Decrease the amount of haptoglobin 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-81" value="3" />Not alter the amount of haptoglobin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-81" value="4" />Stabilize the amount of haptoglobin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is  a test recommended for autoimmune hemolytic anemia.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-82" value="1" />D A T
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-82" value="2" />I A T 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-82" value="3" />Elution Test
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-82" value="4" />Rosette test
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following conditions will shift the oxyhemoglobin dissociation curve to the right except.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-83" value="1" />Hgb Kansas
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-83" value="2" />Acidosis 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-83" value="3" />Increased CO2
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-83" value="4" />Increased CO2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In aplastic anemia, the following are observed except:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-84" value="1" />Granulocytopenia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-84" value="2" />Lymphocytes constitute the majority of the cells seen 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-84" value="3" />Retic count is decreased
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-84" value="4" />Bone Marrow is hyperplastic
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        “ Bite” cells are RBCs that contain which inclusion bodies
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-85" value="1" />Howell Jolly bodies
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-85" value="2" />Heinz Bodies  
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-85" value="3" />Cabot'’'s Ring Bodies
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-85" value="4" />Basophilic stippling
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        “ Bite” cells are RBCs that contain which inclusion bodies
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-86" value="1" />Ineffective erythropoiesis due to destruction of erythroid precursor cells
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-86" value="2" />Defective DNA synthesis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-86" value="3" />Red Cells are more liable to intrameduallry destruction
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-86" value="4" />Peripheral blood smear shows less-segmented neutrophils
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The morphologic classification of anemia (RBC indices) was introcued by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-87" value="1" />Westergren
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-87" value="2" />Wintrobe
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-87" value="3" />Donath-Landsteiner
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-87" value="4" />Zimmer and Hargraves
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which chains of amino acid are synthesized in Thalassemia major that lead to the elevation of Hgb C?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-88" value="1" />Beta
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-88" value="2" />Alpha 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-88" value="3" />gamma
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-88" value="4" />epsilon
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following except one are causes of aplastic anemia except:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-89" value="1" />Shift to the left of O2 dissociation curve
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-89" value="2" />Fanconi Syndrome 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-89" value="3" />Chemicals
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-89" value="4" />Drugs
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        All are true of microangiopathic hemolytic anemia:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-90" value="1" />due to shear stress to RBCs
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-90" value="2" />fibrin strands are laid down within microcirculation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-90" value="3" />Red Cells are fragmented
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-90" value="4" />Red Cells form rouleaux
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What cells are seen in microangiopathic hemolytic anemia?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-91" value="1" />Spehrocytes and NRBCs
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-91" value="2" />Target cells and Cabots Ring bodies
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-91" value="3" />Pappenheimer bodies and basophilic stippling
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-91" value="4" />Toxic granulations and Dohle Bodies
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Most patients with HIV infection demonstrate:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-92" value="1" />Reactive lymphocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-92" value="2" />Atypical lymphocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-92" value="3" />Downey cells
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-92" value="4" />all of them
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Therapeutic bleeding in polycythemia vera may lead to the development of:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-93" value="1" />Iron deficiency anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-93" value="2" />Pernicious anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-93" value="3" />Hemolytic Anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-93" value="4" />Sideroblastic Anemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which antibiotics are less implicated in aplastic anemia:<br>
        1.  Chloramphenicol <br>    
        2.  Penicillin<br>3.  Sulfonamide   <br>                4.  Tetracycline
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-94" value="1" />1 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-94" value="2" />1 & 4    
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-94" value="3" />2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-94" value="4" />4 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Auer rods are seen in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-95" value="1" />monoblasts
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-95" value="2" />myeloblasts  
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-95" value="3" />Both
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-95" value="4" />neither
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is term applied to increase of myeloid cells.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-96" value="1" />Pancytopenia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-96" value="2" />panmyelosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-96" value="3" />Leukoerythroblastosis reaction
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-96" value="4" />Dysplasia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In marked amount of firbosis like in myelofibrosis red cells that pass the fibrotic tissue become:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-97" value="1" />Helmet cells
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-97" value="2" />Schistocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-97" value="3" />Tear Drop cells
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-97" value="4" />Burr cells
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which term is most appropriate to describe PV with production of all cell lines and are all increased?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-98" value="1" />Pancytosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-98" value="2" />Panmyelosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-98" value="3" />Pancytopenia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-98" value="4" />Panhyperplasia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which cells contain azurophilic granules that release thromboplastic substance causing DIC?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-99" value="1" />Myeloblasts
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-99" value="2" />Promyelocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-99" value="3" />Metamyelocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-99" value="4" />Stabs
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The following are characteristics of secondary polycythemia except:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-100" value="1" />Splenomegaly
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-100" value="2" />Increased erythropoietin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-100" value="3" />Decreased O2 saturation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-100" value="4" />Red Cell mass is increased
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The WBC count is corrected in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-101" value="1" />Erythroblastosis faetalis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-101" value="2" />the presence of NRBC
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In anemia, The RBC counting technique is modified by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-102" value="1" />counting cells in more than 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-102" value="2" />decreasing the dilution
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In chornic granulocytic leukemia, the WBC counting technique is modified by:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-103" value="1" />diluting the blood using the WBC pipette 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-103" value="2" />increasing the dilution using the RBC pipette
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In Basophil count, the cells are stained using:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-104" value="1" />Toluidine Blue
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-104" value="2" />Cethylperedinium chloride
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In platelet count, if there is thrombocytopenia, the cells are counted in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-105" value="1" />25 RBC squares
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-105" value="2" />10 RBC squares
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Eosinophil count is indicated
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-106" value="1" />in allergic conditions
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-106" value="2" />in parasitism
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        WBC count is used to:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-107" value="1" />diagnose infections
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-107" value="2" />assess therapy
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Basophil Count is indicated in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-108" value="1" />allergic conditions
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-108" value="2" />in mast cell leukemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        ACTH test is used to:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-109" value="1" />distinguish Cushing”s disease from Addison'’'s disease
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-109" value="2" />evaluate the function of the adrenals
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Basophils produce and release:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-110" value="1" />Histamine and heparin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-110" value="2" />Serotonin and S R S-A
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Eosinophilic granules
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-111" value="1" />Coarse and  basophilic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-111" value="2" />acidophilic and contain Spermine like substance
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Platelets :
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-112" value="1" />adhere to foreign surface
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-112" value="2" />release thrombosthenin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Platelets are difficult to count because they:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-113" value="1" />easily distintegrate
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-113" value="2" />they prefer  non wettable surface
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Platelets originate from:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-114" value="1" />thrombocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-114" value="2" />megakaryocytes
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Thorn'’'s method utilizes:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-115" value="1" />Naubauer Counting Chamber
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-115" value="2" />Randolf'’'s dilutimg fluid
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Pilot'’' s Method utilizes:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-116" value="1" />Naubauer counting chamber
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-116" value="2" />Phloxine dye
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Indirect method of platelet count is done
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-117" value="1" />in  stained blood smear
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-117" value="2" />by counting 1000 RBCs
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The RBC count is done using:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-118" value="1" />25 squares in anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-118" value="2" />10 squares out of 25
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        WBC count is done :
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-119" value="1" />with a dilution of 1:200 in chronic myelocytic leukemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-119" value="2" />with a dilution of 1:10 in aleukemic leukemia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        WBC diluting fluid is:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-120" value="1" />isotonic
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-120" value="2" />hypotonic
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Plasma cells are characterized by:<br>
        1.  perinuclear halo<br>
        2.  eccentric nucleus<br>
        3.  Cartwheel chromatin<br>
        4.  bilobed nucleus<br>
        5.  coarse skein
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-121" value="1" />1,2, 3, 4 and 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-121" value="2" />1,2, and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-121" value="3" />2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-121" value="4" />2,3 and 5
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Pelger Huet Anomaly<br>
        1.  eyeglass nucleus<br>
        2.  failure of segmentation<br>
        3.  dumbbell polymorph<br>
        4.  maybe pseudo
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-122" value="1" />1,2 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-122" value="2" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-122" value="3" />4 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-122" value="4" />2 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Jordan”s anomaly<br>
        1.  Erb'’'s muscular dystrophy<br>
        2.  tetraphyletic anomaly<br>
        3.  with fat containing vacuole<br>
        4.  Sudan Black positive
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-123" value="1" />1,2 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-123" value="2" />3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-123" value="3" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-123" value="4" />1 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Atypical  Lymphocytes
        </b>
        <p style="float:right; margin-left:20px;">
            1.  plasmacytoid cells  <br>    
            2.  monocytoid cells<br>
        </p>
        <p>  
            3.  IM cells<br>
            4.  found in Epstein Barr Virus infection
        </p>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-124" value="1" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-124" value="2" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-124" value="3" />3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-124" value="4" />1 and 2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Basket cells<br>
        </b>
        <p>
            1.  Degenerated cells<br>       
            2.  Smeared cells<br>
            3.  damaged lymphocytes<br> 
            4.  damaged  neutrophils<br>    
        </p>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-125" value="1" />4 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-125" value="2" />3 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-125" value="3" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-125" value="4" />1,2 and 4
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Spherocytes<br>
        1.  found in banked blood<br>
        2.  found in ABO incompatibility<br>
        3.  fragility in increased, decreased resistance<br>
        4.  fragility is decreased, increased resistance<br>
        5.  diameter is smaller than normal but thicker
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-126" value="1" />1,2, and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-126" value="2" />1,2,3 and 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-126" value="3" />2 and 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-126" value="4" />5 only
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Chediak Higashi Anomaly<br>
        1.  characterized by pancytosis<br>
        2.  characterized by albinism<br>
        3   affects eosi, neutron and lympho<br>
        4.  is a syndrome<br>
        5.  inclusions are seen in the cytoplasm
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-127" value="1" />1,2, and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-127" value="2" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-127" value="3" />1,2,3,4 and 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-127" value="4" />1,2,3 and 5
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Reticulocytes<br>
        1.  immediate precursors of WBCs<br>
        2.  young  RBCs<br>
        3.  with remnants of RNA synthesis<br>
        4.  characterized by a network of filaments<br>
        5.  stained by supravital dyes
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-128" value="1" />1 and 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-128" value="2" />1,2,3,4 and 5 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-128" value="3" />2,3,4 and 5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-128" value="4" />3 4 and 5
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Hemmeler anomaly<br>
        1.  granulomere defect of platelets<br>
        2.  absence of granules in megakaryocytes<br>
        3.  an anomaly of platelets and megakaryocytes<br>
        4.  related to Vasterbotten anomaly
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-129" value="1" />4 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-129" value="2" />3 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-129" value="3" />1,2 and 3
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-129" value="4" />2 and 3
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Malarial Stippling<br>
        1.  harbors the parasites of tertian and quartan malaria<br>
        2.  seen as Schuffner'’'s granules in P. vivax<br>
        3.  seen as Maurer'’'s granules in P. ovale   <br>
        4.  seen as Ziemann'’'s granules in P. malariae
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-130" value="1" />1,2,3 and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-130" value="2" />1,2, and 4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-130" value="3" />2 only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-130" value="4" />1 and 2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Cascade theory of coagulation:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-131" value="1" />is also known as one Stage Waterfall theory
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-131" value="2" />involvement of protein C and S was included
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A cell based coagulation theory 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-132" value="1" />will replace the classical model of coagulation cascade
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-132" value="2" />involvement of protein C and S was included
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Propagation phase in cell based coagulation theory
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-133" value="1" />occurs on the surface of platelets
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-133" value="2" />results later in the propagation of large amounts of thrombin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The factor/s in Extravascular phenomenon  is/are: 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-134" value="1" />Type of tissues
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-134" value="2" />Tone of Tissues
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        An initial hemostatic plug occurs in: 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-135" value="1" />Vasoconstriction
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-135" value="2" />Adhesion
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Tissue thromboplastin works in:  
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-136" value="1" />Intrinsic Pathway
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-136" value="2" />Common Pathway
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In Howell'’'s theory of coagulation, there is: 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-137" value="1" />Neutralization of prothrombin by the thromboplastin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-137" value="2" />Neutralization of thromboplastin by  the antoprothrombin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Deficiency of Fibrinogen occurs in: 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-138" value="1" />Acquired Coagulopathies
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-138" value="2" />Liver and Bone diseases
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Prothrombin is 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-139" value="1" />Present in Fresh and Stored Plasma
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-139" value="2" />decreased in Liver and Anticoagulant Therapy
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is/These are decreased in Dicumarol therapy  
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-140" value="1" />SPCA
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-140" value="2" />Labile factor
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Labile Factor is also known as:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-141" value="1" />Proaccelerin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-141" value="2" />Proaccelerator globulin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Clotting factors dependent on Vitamin K
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-142" value="1" />Prothrombin Group
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-142" value="2" />II, V, VII and X
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This clotting factor initiates clotting by surface contact
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-143" value="1" />Clotting factor XII
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-143" value="2" />Glass factor
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Laki Lorand is another name for
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-144" value="1" />Factor XIII
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-144" value="2" />Fibrin Stabilizing Factor
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This/these- vasoconstriction promotes/promote 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-145" value="1" />Serotonin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-145" value="2" />Thromboxane A2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This/these promotes/promote aggregation 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-146" value="1" />ADP and calcium from dense granules of platelets
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-146" value="2" />Antiheparin or Pf4 from the alpha granules
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is/these are- the substance/the substances  that promotes/promote clot retraction
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-147" value="1" />thrombosthenin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-147" value="2" />thromboxane A2
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The  Contact group  excludes:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-148" value="1" />XII and XI
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-148" value="2" />HMWK
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The prothrombin group is inhibited by: 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-149" value="1" />Oral anticoagulant therapy
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-149" value="2" />Administration of antibiotics that sterilize the intestinal tract
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What clotting factors  below are not consumed?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-150" value="1" />I, V and VIII
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-150" value="2" />XIII
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Hereditary deficiencies of Alpha I antitrypsin are seen in:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-151" value="1" />Liver cirrhosis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-151" value="2" />Pulmonary Disease (emphysema)
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Alpha 2 macroglobulin :
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-152" value="1" />are large innate plasma glycoprotein
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-152" value="2" />binds with proteolytic enzymes including thrombin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Alpha 2 antiplasmins
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-153" value="1" />prevent plasmin from binding fibrin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-153" value="2" />prevent uncontrolled digestion of fibrin, fibrinogen, factors V and VIII
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        These are vitamin K dependent:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-153" value="1" />Protein C aand S
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-153" value="2" />Clotting factors in the prothrombin group
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is the time required  for a small cut to stop bleeding
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-154" value="1" />Bleeding Time
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-154" value="2" />Clotting Time
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is the extrusion of serum  from a clot in the presence of Castor Oil
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-155" value="1" />Clot Retraction Time by Hirshboeck method
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-155" value="2" />CRT by Mc Farland Method
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is the dissolution of a firbin clot
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-156" value="1" />Clot Retraction
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-156" value="2" />Fibrinolysis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is the time from the collection of the blood up to the formation of fibrin.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-157" value="1" />Clotting Time
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-157" value="2" />Closure Time
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        This is the time from the addition of CaCl2 to the citrated plasma in the presence of thromboplastin up to the formation of a gel.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-159" value="1" />Prothrombin Time
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-159" value="2" />Coagulation Time
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Activated partial thromboplastin time procedure requires the addition of _______ for activation.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-160" value="1" />Kaolin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-160" value="2" />Ellagic Acid 
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following anticoagulants works by inactivation of complement?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-161" value="1" />thrombin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-161" value="2" />sodium heparin 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-161" value="3" />acid citrate dextrose 
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-161" value="4" />buffered sodium citrate
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Blood collected from infants are usually done with a skin puncture. This procedure can also be done in adults with one of the following conditions:
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-162" value="1" />sepsis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-162" value="2" />hemorrhage
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-162" value="3" />comatose patients
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-162" value="4" />thrombotic tendencies
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A blood specimen was collected using EDTA at 8:00am for CBC. The Medical Technologist on duty worked on the sample at 12nn. What is the next step to be done?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-163" value="1" />request for a new sample
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-163" value="2" />proceed with hemoglobin only
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-163" value="3" />proceed with the CBC examination
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-163" value="4" />proceed with all examinations except hematocrit
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the final dilution of a 4.5ml of blood drawn for coagulation studies after an addition of 0.5ml of 3.8% Na Citrate?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-164" value="1" />1:4
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-164" value="2" />1:9
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-164" value="3" />1:18
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-164" value="4" />1:27
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the best approach to use on a 7yr old child who needs to have blood drawn?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-165" value="1" />offer the child a toy
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-165" value="2" />tell the child it won'’'t hurt
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-165" value="3" />have someone restrain the child
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-165" value="4" />explain to the child the procedure in simple terms and ask him to cooperate.
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which red cell parameter indicates the average concentration of Hgb in the erythrocytes of any specimen?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-166" value="1" />MCV
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-166" value="2" />MCH
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-166" value="3" />RDW
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-166" value="4" />MCHC
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which red cell parameter indicates the average concentration of Hgb in the erythrocytes of any specimen?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-167" value="1" />Alkali hematin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-167" value="2" />Assendelft test
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-167" value="3" />Oxyhemoglobin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-167" value="4" />Cyanmethemoglobin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A Miller ocular disc is most useful in counting what type of blood cells?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-168" value="1" />rbc
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-168" value="2" />reticulocytes
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-168" value="3" />wbc
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-168" value="4" />platelets
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which parameters are needed to compute for the MCV?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-169" value="1" />RBC and Hematocrit
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-169" value="2" />RBC and Hemoglobin
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-169" value="3" />Hemoglobin and PCV
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-169" value="4" />Hematocrit and Hemoglobin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following would cause an increase in ESR?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-170" value="1" />Hb CC and hepatitis
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-170" value="2" />hepatitis and polycythemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-170" value="3" />hepatitis and menstruation
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-170" value="4" />polycythemia and     menstruation
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following is NOT true regarding reticulocyte count?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-171" value="1" />normal value is 0.5-1.5%
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-171" value="2" />stains with Wright'’'s stain
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-171" value="3" />increased in hemolytic anemia
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-171" value="4" />best index of effective erythropoiesis
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the absolute reticulocyte count if the patient'’'s retic count is 4% and the RBC count is 3.30 X 10<sup>12</sup>/L<
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-172" value="1" />130 x 10 <sup>12</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-172" value="2" />132 x 10 <sup>12</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-172" value="3" />135 x 10 <sup>12</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-172" value="4" />136 x 10 <sup>12</sup>/L
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the MCV of the patient with a hematocrit of 37 vol % and RBC count of 3.48 x 1012/L?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-173" value="1" />102 fl
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-173" value="2" />104 fl
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-173" value="3" />106 fl
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-173" value="4" />108 fl
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the WBC count of the patient with a total number of 250 leukocytes in four quadrants?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-174" value="1" />10. 5 x 10<sup>9</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-174" value="2" />11.0 x 10<sup>9</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-174" value="3" />12.0 x 10<sup>9</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-174" value="4" />12.5 x 10<sup>9</sup>/L
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the RBC count of the patient with a total number of 450 erythrocytes in five central RBC squares?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-175" value="1" />400 x 10<sup>12</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-175" value="2" />450 x 10<sup>12</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-175" value="3" />500 x 10<sup>12</sup>/L
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-175" value="4" />550 x 10<sup>12</sup>/L
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        If 2 large corner squares are counted in the manual WBC count, what is the correction factor for volume?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-176" value="1" />2.5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-176" value="2" />5.0
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-176" value="3" />7.5
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-176" value="4" />10.0
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The differential count of 100 leukocytes of a 21yr old patient showed 8 eosinophils. If the WBC count is 8.4 x 109/L, what is the indirect absolute number of eosinophils/Liter?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-177" value="1" />0.8 x 109/L and above normal
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-177" value="2" />0.67 x 109/L and above normal
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-177" value="3" />0.8 x 109/L and within normal range
                </label>
            </li>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-177" value="4" />0.67 x 109/L and within normal range
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        If a medical technologist will measure hemoglobin using cyanmethemoglobin method, what is the best time to wait for the full conversion of hemoglobin to cyanmethemoglobin?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-178" value="1" />5mins
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-178" value="2" />10mins
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-178" value="3" />15mins
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-178" value="4" />30mins
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        If a patient is suspected to have fetal hemoglobin in the circulation, what is the best assay to do to confirm its presence?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-179" value="1" />Hemoglobin electrophoresis
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-179" value="2" />Kleihauer-Betke stain
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-179" value="3" />HiCN method
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-179" value="4" />Acid hematin method
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What system was formulated by Technicon instrument that has the ability to perform a seven-part leukocyte differential and leukocyte count using volume determination based on light scatter and light absorbency of stained cells?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-180" value="1" />Hemalog D
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-180" value="2" />Cell-Dyn 2000
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-180" value="3" />Coulter Counter
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-180" value="4" />Sysmex electronic cell counting
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which automated electronic counting device takes advantage of the difference in the conductiveness of the blood cell and the diluents but has a disadvantage that includes the lack of a closed sampling system?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-181" value="1" />Hemalog D.  
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-181" value="2" />Cell-Dyn 2000
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-181" value="3" />Coulter Counter
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-181" value="4" />Sysmex electronic cell counting
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Cell counters manufactured by Coulter electronics operate on the principle of electronic impedance. In what kind of fluid conductor are the cells suspended to achieve this?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-182" value="1" />saline
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-182" value="2" />phloxine
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-182" value="3" />soap solution
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-182" value="4" />ethylene glycol
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        False increase in WBC count using electronic cell counters may occur if the abnormal red cells resist lyses. Which of the following will give artifactual increase in WBC count on the said condition?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-183" value="1" />Sickle cell
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-183" value="2" />Target cells
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-183" value="3" />Extremely hypochromic cells
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-183" value="4" />All choices mentioned above
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In Sysmex automation method, how can HiCN be prevented from creating environmental hazard?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-184" value="1" />using bromcresol blue
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-184" value="2" />utilizing methyl orange
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-184" value="3" />replacing HiCN with protamine sulfate
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-184" value="4" />replacing HiCN with Sodium lauryl sulfate
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the expected result of WBC count done in automated cell counter from a patient with thalassemia major? 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-185" value="1" />increased
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-185" value="2" />decreased
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-185" value="3" />not affected
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-185" value="4" />none of the above
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        If a hematologist will formulate a graph for the standard deviation, what should be the significant difference between 2 electronic cell counts? 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-186" value="1" />+/- 0.5
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-186" value="2" />+/- 1.0
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-186" value="3" />+/- 1.5
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-186" value="4" />+/- 2.0.
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A patient'’'s hematologic results from an electrical impedance instrument are:<br>
        WBC = 2.0 X 10 9/ L<br>
        RBC  = 3.83 X 1012/L<br>
        Hb      = 115 g/L<br>
        Hct.  =  0.34 L/L<br>
        Plt.   =  60.0 X 109/L<br>
        Blood smear evaluation for quality control purposes revealed normocytic normochromic RBCs;  2 WBC/ hpf ; and 10 platelets / oif . The next step would be:<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-187" value="1" />report the results as obtained
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-187" value="2" />repeat the leukocyte count
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-187" value="3" />repeat the platelet count
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-187" value="4" />repeat the RBC count
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        How can a Coulter-Counter automated machine for automated blood cells counting is distinguished from other automated method?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-188" value="1" />report the results as obtained
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-188" value="2" />repeat the leukocyte count
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-188" value="3" />repeat the platelet count
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-188" value="4" />repeat the RBC count
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A medical technologist performed a WBC count using an automated counter.  If his results is spurious, which of the following is the cause of such result?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-189" value="1" />clotting of blood
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-189" value="2" />presence of smudge cell
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-189" value="3" />Uremia plus immunosuppresants
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-189" value="4" />Presence of Cryoglobulin
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the special stain used for the evaluation of iron storage?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-190" value="1" />Perl'’'s reaction
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-190" value="2" />Iron hematoxylin
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-190" value="3" />Sudan Black B
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-190" value="4" />ACP stain
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A cell is detected through a screening procedure of its metabolic defects. Which of the following cells is detected by nitroblue tetrazolium reduction test?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-191" value="1" />basophil
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-191" value="2" />monocytes
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-191" value="3" />neutrophils
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-191" value="4" />eosinophils
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        An abnormal Schilling test is obtained on a patient.  A second Schilling test is performed, supplemented with oral intrinsic factor.  The second Schilling test is normal.  What do the results indicate?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-192" value="1" />sprue
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-192" value="2" />pernicious anemia
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-192" value="3" />intestinal disorders
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-192" value="4" />malabsorption syndrome
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        How will a medical technologist perform correctly the sugar-water test?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-193" value="1" />the collected blood is placed in ice water bath
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-193" value="2" />the separated serum is placed in warm water bath
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-193" value="3" />sucrose solution is added in the patient'’'s specimen
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-193" value="4" />complement is added after centrifugation
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A hemoglobin electrophoresis pattern on cellulose acetate at pH 8.6 demonstrates a band in the S region. Which of the following is the best  test to confirm the identity of this hemoglobin ?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-194" value="1" />Heat stability
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-194" value="2" />Alkali denaturation
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-194" value="3" />Hemoglobin solubility
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-194" value="4" />Hemoglobin electrophoresis on citrate agar at pH 6.0
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the earliest recognizable stage of the megakaryocyte maturation series?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-195" value="1" />Megakaryoblast
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-195" value="2" />promegakaryocyte
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-195" value="3" />megakaryocyte
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-195" value="4" />metamegakaryocyte
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What platelet region is responsible for maintaining its discoid shape?<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-196" value="1" />peripheral zone
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-196" value="2" />submembrane region
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-196" value="3" />sol gel zone
            </label>
            </li>  
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-196" value="4" />organelle zone
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Abnormal megakaryocytes that have lost their ability to undergo endomitosis:<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-197" value="1" />metamegakaryocytes
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-197" value="2" />macromegakaryocytes
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-197" value="3" />promegakaryocytes
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-197" value="4" />micromegakaryocytes
    </li>  
    </ul>
    </li>         
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following platelet activities may also be impaired in the presence of Von Willebrand'’'s disease?<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-198" value="1" />adhesion
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-198" value="2" />aggregation
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-198" value="3" />release reaction
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-198" value="4" />clot dissolution
    </li>  
    </ul>
    </li>         
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    What is the Clot Retraction Value if the serum expressed from the clot is 2.5 ml.  The amount of blood used is 5 ml.<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-199" value="1" />50%
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-199" value="2" />40%
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-199" value="3" />55%
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-199" value="4" />60%
    </li>  
    </ul>
    </li>   
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    When performing an indirect platelet count, which can be considered normal?<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-200" value="1" />2 platelets / OIF
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-200" value="2" />5 platelets/OIF
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-200" value="3" />10 platelets/OIF
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-200" value="4" />30 platelets/OIF
    </li>  
    </ul>
    </li>  
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Why is it necessary to place a charged counting chamber in a moistened petri dish in doing manual platelet counting?<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-201" value="1" />to allow rapid settling
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-201" value="2" />to prevent platelet disintegration
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-201" value="3" />to keep the counting chamber sterile
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-201" value="4" />to prevent drying of the solution in the chamber
    </li>  
    </ul>
    </li>  
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    In clot retraction time, a complete retraction should be attained within:<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-202" value="1" />1 hour
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-202" value="2" />12 hours
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-202" value="3" />24 hours
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-202" value="4" />72 hours
    </li>  
    </ul>
    </li>  
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    GpIb is a platelet receptor required for normal adhesion. What disorder is MOST likely to occur when platelets fail to adhere on damaged tissues even in the presence of normal GpIb?<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-203" value="1" />Bernard Soulier Syndrome
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-203" value="2" />Von Willebrand'’'s disease
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-203" value="3" />Glanzmann'’'s Thrombasthenia
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-203" value="4" />Chediak Higashi Anomaly
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    In the red cell maturation, a basophilic erythroblast is also known as ________ in
    the rubric nomenclature.<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-204" value="1" />metarubricyte
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-204" value="2" />prorubricyte
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-204" value="3" />reticulocyte
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-204" value="4" />rubricyte
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following are applicable to the maturation sequence of blood cells?<br>
    1 -  Immature cells are large and become progressively 
    smaller as they mature<br>
    2 -  As the cells age, the absolute and relative size of 
    the nucleus decreases<br>
    3 -  The nuclear chromatin strands of immature cells 
    contain DNA<br>
    3 -  One of the signs of immaturity in blood cells is the
    presence of nucleoli<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-205" value="1" />1, 3
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-205" value="2" />2, 4
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-205" value="3" />1, 2, 3
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-205" value="4" />1, 2, 3, 4
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    The M:E ratio is determined by dividing the number of:<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-206" value="1" />Fat cells by the number of granulocytes and their precursors
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-206" value="2" />Nucleated red blood cells by the number of lymphoid cells
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-206" value="3" />Granulocytes and their precursors by the number of nucleated red blood cells
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-206" value="4" />Nucleated hematopoietic cells by the number of fat cells
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    The following results are obtained on a bone marrow differential:<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Myeloblasts   1<br>       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Lymphocytes   7<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Promyelocytes 5<br>       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Monocytes     2<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Myelocytes       10<br>       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plasmacytes   1<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Neutrophilic Bands     9<br>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Normoblasts   60<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Segmented neutrophils 4<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Eosinophils   1<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Basophils              0<br>
    What is the myeloid-erythroid  ratio (M:E) ?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-207" value="1" />0.5:1
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-207" value="2" />0.8: 1
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-207" value="3" />2.0 : 1
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-207" value="4" />1: 0.5
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A bone marrow biopsy from a 40 year old patient with a history of malignant lymphoma shows the following:     hypocellular marrow with a predominance of fibroblast. This is consistent with:
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-208" value="1" />Normal marrow
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-208" value="2" />Myeloid metaplasia
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-208" value="3" />Myelofibrosis
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-208" value="4" />Metastatic disease
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Arrange the following erythrocytic stages from the youngest to the oldest.<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  1   basophilic normoblast<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  2   reticulocyte<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  3   pronormoblast<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  4  orthochromic normoblast<br>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  5  polychromatophilic normoblast<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-209" value="1" />1, 2, 3, 4, 5
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-209" value="2" />3, 1, 5, 4, 2
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-209" value="3" />3, 1, 4, 5, 2
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-209" value="4" />3, 4, 1, 5, 2
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following red cell inclusions stain with both Perl'’'s Prussian blue and Wright'’'s stain ?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-210" value="1" />Howell-Jolly bodies
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-210" value="2" />Basophilic stippling
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-210" value="3" />Pappenheimer bodies
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-210" value="4" />Metastatic disease
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following red cell inclusion is characteristically found in lead poisoning?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-211" value="1" />Howell-Jolly
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-211" value="2" />Cabot rings
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-211" value="3" />Heinz Bodies
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-211" value="4" />Basophilic Stippling
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A mixture of macrocytes, microcytes and normal erythrocytes present in the peripheral smear is described as ____________.
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-212" value="1" />Poikilocytes
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-212" value="2" />Polychromatophilia
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-212" value="3" />Anisocytosis
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-212" value="4" />Anisochromasia
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following cells may be seen in the peripheral blood smear if a patient sickle cell anemia is in an acute crisis state?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-213" value="1" />Depranocytes
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-213" value="2" />Elliptocytes
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-213" value="3" />Leptocytes
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-213" value="4" />Stomatocytes
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    In anemia, what will you most likely do in the examination of the bone marrow?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-214" value="1" />discard the bone marrow
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-214" value="2" />evaluate abnormalities in erythropoiesis
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-214" value="3" />isolate the platelets
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-214" value="4" />laminate the cells
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    When performing a manual RBC count on a blood sample coming from a patient with Polycythemia vera , what step is most appropriate to come up with an accurate result?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-215" value="1" />Aspirate blood up to 0.1 mark of the RBC pipette
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-215" value="2" />Count cells in two quaternary squares. erythropoiesis
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-215" value="3" />Count cells using LPO
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-215" value="4" />Use a WBC pipette
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A blood sample was obtained from a patient with a hematocrit value of 0.39.  435 cells were counted in 5 tertiary squares of a Neubauer Hemocytometer using a dilution of 1:200.  What would be the MCV?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-216" value="1" />86.5 fl
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-216" value="2" />89.6 fl
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-216" value="3" />95.8 fl
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-216" value="4" />100.2 fl
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A blood sample was obtained from a patient with a hematocrit value of 0.39.  435 cells were counted in 5 tertiary squares of a Neubauer Hemocytometer using a dilution of 1:200.  What would be the MCV?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-217" value="1" />130 x 10<sup>9</sup>/L
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-217" value="2" />132 x 10<sup>9</sup>/L
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-217" value="3" />135 x 10<sup>9</sup>/L
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-217" value="4" />136 x 10<sup>9</sup>/L
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Arrange the following from the earliest to the latest precursor.<br>
    I&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -  Porphyrinogen<br>
    II&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    -  Protoporphyrinogen<br>
    III&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   -  Coproporphyrinogen<br>
    IV&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    -  Porphobilinogen<br>
    V&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   -  Uroporphyrinogen<br>
    VI&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    -  Bilirubin<br>
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-218" value="1" />II, V, IV, VI
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-218" value="2" />IV, V, III, II
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-218" value="3" />I, V, III, II   
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-218" value="4" />V, IV, III, II
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A shift to the left of the Hgb O2 dissociation curve is caused by increased affinity of Hgb to oxygen, which results to a decrease in the release of oxygen to the tissues that eventually causes hypoxia.  Which of the following can cause a left shifting of the curve?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-219" value="1" />Increased CO2 in the blood
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-219" value="2" />Low body temperature
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-219" value="3" />Acidosis        
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-219" value="4" />Increase in 2,3 DPG levels
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    In patients with infectious mononucleosis, what blood cells are infected by the causative virus?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-220" value="1" />T-lymphocytes
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-220" value="2" />Monocytes
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-220" value="3" />Macrophages     
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-220" value="4" />B-lymphocytes
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    The process of proliferation and maturation of  neutrophils lasts for
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-221" value="1" />1 – 2 days
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-221" value="2" />2 – 4 days
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-221" value="3" />5 - 6 days  
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-221" value="4" />8 - 10 days
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A monocyte can be mistaken for large lymphocyte during differential counting. To be certain that it is monocyte a Medical Technologist must take note a unique characteristic of a monocyte and that is ______________________.
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-222" value="1" />the chromatin material is compact
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-222" value="2" />a brain-like convolution of the chromatin material
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-222" value="3" />a very fine chromatin material with distinct euchromatin
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-222" value="4" />coarsely distributed chromatin material with distinct boarder
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    In which stage of the granulocytic series can the granulocyte can be first classified as neutrophilic, eosinophilic or basophilic?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-223" value="1" />Myeloblast
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-223" value="2" />Promyelocytel
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-223" value="3" />Myelocyte
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-223" value="4" />Metamyelocyte
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Using the Neubauer hemocytometer, what will be the WBC count if 100 cells were counted on slide A and 110 cells on slide B?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-224" value="1" />5.25 x 10<sup>9</sup>/L
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-224" value="2" />5.4 X 10<sup>9</sup>/L
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-224" value="3" />5.5 x 10<sup>9</sup>/L
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-224" value="4" />5.6 x 10<sup>9</sup>/L
    </li>  
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    What is the absolute neutrophil count of the patient with a WBC Count of 8.5 x10<sup>9</sup>/L and a neutrophil relative count of 67%?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-225" value="1" />5.2 x 10<sup>9</sup>/L
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-225" value="2" />5.4 10<sup>9</sup>/L
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-225" value="3" />5.7 x 10<sup>9</sup>/L
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-225" value="4" />6.0 x 10<sup>9</sup>/L
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    The CBC result of a patient revealed a WBC Count of 11.5 x 10<sup>9</sup>/L.  Upon scanning the blood smear, the medical technologist was able to count 30 nRBC'’'s.  What is the Corrected WBC Count?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-226" value="1" />7.25 x 10<sup>9</sup>/L
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-226" value="2" />8.15 x 10<sup>9</sup>/L
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-226" value="3" />8.85 x 10<sup>9</sup>/L
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-226" value="4" />9.35 x 10<sup>9</sup>/L
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Compute for the WBC Count when 130 cells have been counted in 2 secondary squares of a Neubauer hemocytometer using a 1:20 blood dilution.
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-227" value="1" />8 x 10<sup>9</sup>/L
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-227" value="2" />9 x 10<sup>9</sup>/L
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-227" value="3" />11 x 10<sup>9</sup>/L
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-227" value="4" />13 x 10<sup>9</sup>/L
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following tests is used to monitor heparin therapy?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-228" value="1" />Prothrombin time
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-228" value="2" />Activated partial prothrombin time
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-228" value="3" />Reptilase time
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-228" value="4" />Thrombin time
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following platelet count methods will employ the phase contrast microscope?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-229" value="1" />Rees and Ecker
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-229" value="2" />Brecker-Cronkite
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-229" value="3" />Indirect method
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-229" value="4" />Coulter Counter
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    D-dimers are degradation products of
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-230" value="1" />non-cross linked fibrin
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-230" value="2" />fibrinogen
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-230" value="3" />cross linked fibrin
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-230" value="4" />plasmin
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following  is the primary inhibitor of the fibrinolytic system
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-231" value="1" />Protein C
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-231" value="2" />Protein S
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-231" value="3" />Alpha-2 antiplasmin
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-231" value="4" />Alpha-2 macroglobulin
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which results would be expected for both PT and APTT in a patient with polycythemia?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-232" value="1" />Protein Cprolonged PT and APTT
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-232" value="2" />shortened PT and APTT
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-232" value="3" />Normal PT, prolonged APTT
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-232" value="4" />normal PT and APTT
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    A deficiency in factor X will result to prolonged PT and APTT corrected by
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-233" value="1" />aged serum but not with absorbed plasma
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-233" value="2" />absorbed plasma but not with aged serum
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-233" value="3" />both absorbed plasma and aged serum
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-233" value="4" />normal PT and APTT
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    The plasma proteins absorbed by barium sulfate includes
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-234" value="1" />II, VII, IX, X
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-234" value="2" />I, V, VIII, XIII
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-234" value="3" />X, XI, XII, HMWK
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-234" value="4" />V, VIII
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Which of the following differentiates primary from secondary fibrinolysis?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-235" value="1" />Euglobulin clot lysis time
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-235" value="2" />D-dimer test
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-235" value="3" />Reptilase time
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-235" value="4" />Assay of fibrin degradation products
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    Specimen for APTT in sodium citrate is collected at 9:30 and allowed to remain at room temperature until 3:30 PM. How will this affect the result?
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-236" value="1" />Euglobulin clot lysis timeprolonged, due to loss of factor VIII
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-236" value="2" />prolonged, due to platelet clumping
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-236" value="3" />prolonged, due to decreased fibrinogen
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-236" value="4" /> Assay of fibrin degradation productsprolonged, due to loss of factor XII
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
    <b>
    The Med. Tech. has noted that a blood sample for APTT was at room temperature for 3 hours. The Med. Tech. should 
    </b>
    <ul>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-237" value="1" />requests a new sample
    </label>
    </li>
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-237" value="2" />continue with the APTT
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-237" value="3" />separate the plasma from the blood components immediately
    </label>
    </li>  
    <li>
    <label>
    <input class="answer-radio" type="radio" name="question-237" value="4" />warm the plasma at 37&deg;C
    </label>
    </li>
    </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Measurement of the time for fibrin formation when thrombin is added to recalcify plasma evaluates the 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-238" value="1" />extrinsic system
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-238" value="2" />intrinsic system
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-238" value="3" />common pathway immediately
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-238" value="4" />all of these
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In factor VII deficiency, a prolonged result will not be corrected by the addition of
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-239" value="1" />aged serum
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-239" value="2" />aged plasma
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-239" value="3" />fresh plasma
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-239" value="4" />adsorbed plasma
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The  World Health Organization recommends :
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-240" value="1" />reporting the INR on patients on long term coumarin therapy
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-240" value="2" />using the INR to monitor patients  on stabilized heparin therapy
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-240" value="3" />reporting the INR to monitor patients on stabilized aspirin therapy
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-240" value="4" />reporting the INR to monitor patients on stabilized hirudin therapy
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following factors are adsorbable?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-241" value="1" />Factors  I, II, V, XIII
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-241" value="2" />Factors  II, VII, IX, X
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-241" value="3" />Factors  II, V, VIII, XI
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-241" value="4" />Factors  III, IV, IX, X
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        In disseminated intravascular coagulation (DIC):
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-242" value="1" />all coagulation tests are prolonged and abnormal
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-242" value="2" />all coagulation tests are normal except clot solubility test
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-242" value="3" />all coagulation tests are prolonged except clot solubility test
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-242" value="4" />all coagulation tests are prolonged except bleeding time
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following clotting factors is most likely deficient when there is prolongation of the APTT corrected by the addition of adsorbed plasma?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-243" value="1" />Factor IX
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-243" value="2" />Factor X
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-243" value="3" />Factor VIII
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-243" value="4" />Factor II
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A patient develops severe unexpected bleeding and the following test results are obtained:<br>  
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;PT and APTT      -  prolonged<br>
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Platelet count       -  50 x 10<sup>9</sup>/L<br>
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Fibrinogen level     - 50 mg/dL<br>
        &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Fibrin Degradation Products- increased<br><br>
        What would be the most probable cause?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-244" value="1" />Disseminated Intravascular Coagulation
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-244" value="2" />Factor VII deficiency
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-244" value="3" />Lupus anticoagulant
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-244" value="4" />Hemophilia A
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        A normal platelet count, prolonged bleeding time, and deficient clot retraction are laboratory findings in 
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-245" value="1" />Disseminated Intravascular Coagulation
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-245" value="2" />Idiopathic Thrombocytopenic purpura
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-245" value="3" />von Willebrand'’'s disease
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-245" value="4" />Thrombasthenia
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        The coagulation factors that can be affected by coumadin drugs are
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-246" value="1" />II, VII, IX, X
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-246" value="2" />I, V, VIII, XIII
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-246" value="3" />X, XI, XII, HMWK
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-246" value="4" />V, VIII
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Two standard deviations (2SD) from the mean in a normal distribution curve would include ___ of all values.
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-247" value="1" />99%
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-247" value="2" />95%
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-247" value="3" />80%
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-247" value="4" />75%
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        Which of the following statements best describes the Gaussian curve?
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-248" value="1" />It represents the standard deviation
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-248" value="2" />It represents a normal bell-shaped distribution
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-248" value="3" />It represents the coefficient of variation
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-248" value="4" />It represents variance of population
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        An upward trend was observed over a six day period in a quality control plot for a prothrombin time procedure on a photo-optical instrument. This observation indicates a
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-249" value="1" />decrease in standard deviation values and thus a change in the
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-249" value="2" />decreased coefficient  of variation
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-249" value="3" />loss of accuracy but not precision
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-249" value="4" />loss of precision
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
    <?php if(in_array($i++, $randomQuestion)): ?>
    <li class="question-item" id="hematology-<?php echo $i; ?>">
        <b>
        What is the absolute reticulocyte count if the patient'’'s retic count is 4% and the RBC count is 3.30 X 10<sup>12</sup>/L<br>
        </b>
        <ul>
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-250" value="1" />130 x 10<sup>12</sup>/L 
                </label>
            </li>
            <li>
                <label>
                    <input class="answer-radio" type="radio" name="question-250" value="2" />132 x 10<sup>12</sup>/L
            </li>
            <li>
            <label>
            <input class="answer-radio" type="radio" name="question-250" value="3" />135 x 10<sup>12</sup>/L
            </label>
            </li>  
            <li>
                <label>
                <input class="answer-radio" type="radio" name="question-250" value="4" />136 x 10<sup>12</sup>/L
                </label>
            </li>
        </ul>
    </li>
    <?php endif; ?>
</div>
<?php include 'inc/header.php'; ?>
	<h1>Topic</h1>
	<p>We are currently seeking contributors for the topics below or you can also share your stories and experiences, anything that would help a colleague or future colleague.</p>
	<table id="topic-table">
		<thead>
			<tr>
				<td>Haematology</td>
				<td>Biochemistry</td>
				<td>Blood transfusion<br>and immunology</td>
				<td>Microbiology</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<ul>
						<li>· Haematopoiesis</li>
						<li>· Granulocytes</li>
						<li>· Monocyte and macrophages</li>
						<li>· Lymphocytes and plasma cells</li>
						<li>· Malignant Leukocyte disorder</li>
						<li>· Erythrocytes</li>
						<li>· Haemoglobin</li>
						<li>· Anaemias</li>
						<li>· Haemoglobinopathies</li>
						<li>· Thalassemias</li>
						<li>· Diseases associated with vascular system</li>
						<li>· Thrombocytes</li>
						<li>· Haemostasis</li>
						<li>· Fibrinolytic system</li>
						<li>· Regulatory proteins of coagulation and fibrinolysis</li>
						<li>· Thrombotic disorder</li>
						<li>· Haemorrhagic disorder</li>
						<li>· Anticoagulation therapies</li>
					</ul>
				</td>
				<td>
					<ul>
						<li>· Instrumentation and analytical principles</li>
						<li>· Proteins and tumor markers</li>
						<li>· Nonprotein nitrogenious compounds</li>
						<li>· Carbohydrates</li>
						<li>· Lipids and Lipoproteins</li>
						<li>· Enzymes and Cardiac Assessment</li>
						<li>· Liver Function and Porphyrin formation</li>
						<li>· Electrolytes and Osmolality</li>
						<li>· Acid-Base Metabolism</li>
						<li>· Endocrinology</li>
						<li>· Therapeutic Drug Monitoring</li>
						<li>· Toxicology</li>
						<li>· Vitamins</li>
					</ul>
				</td>
				<td>
					<ul>
						<li>· Genetics </li>
						<li>· ABO and H blood group system </li>
						<li>· RH blood group</li>
						<li>· Other blood group system</li>
						<li>· Reagents and methods</li>
						<li>· Direct antiglobulin test</li>
						<li>· Identification of unexpected alloantibodies</li>
						<li>· Pretransfusion testing</li>
						<li>· Haemolytic disease of the newborn</li>
						<li>· Blood collection</li>
						<li>· Blood components: preparation, storage and shipment</li>
						<li>· Blood component therapy</li>
						<li>· Transfusion therapy</li>
						<li>· Transfusion reaction</li>
						<li>· Transfusion transmitted disease</li>
						<li>· Safety and quality assurance</li>
						<li>· The immune system</li>
						<li>· Major histocompatibility Complex</li>
						<li>· Non specific immune response</li>
						<li>· Autoimmune disease</li>
						<li>· Hypersensitivity</li>
						<li>· Immune deficiency</li>
						<li>· Antigen antibody reaction</li>
						<li>· Tumor immunology</li>
						<li>· Complement fixation</li>
					</ul>
				</td>
				<td>
					<ul>
						<li>· Aerobic gram positive bacteria</li>
						<li>· Aerobic gram negative bacteria</li>
						<li>· Mycobacteria</li>
						<li>· Anaerobic bacteria</li>
						<li>· Chlamydia, rickkettsia and mycoplasma</li>
						<li>· Spirochetes</li>
						<li>· Antimicrobial agents and antimicrobial susceptibility testing</li>
						<li>· Procedures and biochemical identification of bacteria</li>
						<li>· Mycology</li>
						<li>· Culture and isolation</li>
						<li>· Yeasts</li>
						<li>· Fungi</li>
						<li>· Virology</li>
						<li>· Viral identification</li>
						<li>· DNA viruses</li>
						<li>· RNA viruses</li>
						<li>· Hepatitis viruses</li>
						<li>· Urinalysis</li>
						<li>· Renal pathology and renal fnction tests</li>
						<li>· Body fluids and fecal analysis</li>
					</ul>
				</td>
			</tr>
		</tbody>
	</table>
<?php include 'inc/footer.php'; ?>

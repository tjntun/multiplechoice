<?php include 'inc/random.php'; ?>
<?php $i = 0; ?>
<div class="tab-item" id="tab-4">
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following are unacceptable specimen for microbiological examination? <br>
    1.  use of swab with wooden shaft or calcium alginate for viruses  <br>
    2.  formaldehyde treated stool <br>
    3.  biopsy specimen for anaerobic culture <br>
    4.  hemolyzed sera <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />1, 2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />1, 2, 3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />1 and 3
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    If a swab is used to collect specimen, polyester-tipped swab on a plastic shaft is acceptable for most organisms.  Calcium alginate swabs are to be avoided when suspecting:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />Herpes simplex virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />Chlamydia trachomatis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />Neisseria gonorrhoeae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />Corynebacterium diphtheriae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The modified Kinyoun method does NOT require heating to stain the acid fast bacilli due to high concentration of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />tergitol
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />phenol
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />acidic dye
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />malachite green
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The color of the fluorescent light depends on the dye and light filters used.  Hence, dyes such s auramine and fluorescent isothiocyanate (FITC) reagent is used, the light filter used is which color?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />green
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />red
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />violet
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />blue
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The following are true of Gram and Acid fast staining:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />A.   addition of the primary stain colors the cells violet
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />B.   application of the decolorizer will separate two groups of cells based on staining reaction*
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />C.   cells are colored blue following counterstaining
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A useful screen to differentiate the Verotoxin E. coli of serotype O157:H7 is:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />A.   citrate utilization
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />B.   indophenol oxidase production
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />C.   production of indole from tryptophan
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />D.   sorbitol fermentation
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What should be done when a suspension of organisms suspected to be Salmonella based on biochemical reactions did not agglutinate with Salmonella O antisera subgroups A, B, C1,C2, D, but reactive with Vi antisera?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />A.   report as NEGATIVE for Salmonella spp.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />B.   consider the strain as most probably Shigella spp
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />C.   organism should be heated at 100’C for 10 minutes and retested
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />D.   organism should be autoclave and retested
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Salmonella species are characteristically
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />A.   nonmotile
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />B.   oxidase positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />C.   lactose fermenter
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />D.   H2S producer
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    All Enterobacteriaceae ferment what sugar(s) <br>
    1.  lactose  <br>
    2.  sucrose  <br>
    3.  mannitol <br> 
    4.  glucose
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="3" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"  value="4" />4 only
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
        Plesiomonas shigelloides cross react with Shigella group D but can be differentiated by performing what test?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />Catalase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />Bile solubility
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />Quellung
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />Oxidase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The Klebsiella-Enterobacter-Serratia-Hafnia (KESH) group are POSITIVE on what test (s)?	
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  indole
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Vogues-Proskauer
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  lysine decarboxylase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  methyl red
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which strain of E. coli produces Shigella toxin?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />ETEC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />EHEC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />EIEC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />EPEC
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Characterized Pseudomonas aeroginosa <br>
    1. beta hemolytic, oxidized glucose, citrate and oxidase positive <br>
    2. fluoresce greenish yellow under ultraviolet light<br>
    3. cause infections in burn sites, wounds, otitis externa and urinary tract infection<br>
    4. good growth at 22’C<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    14  Disease characterized by cross reactions of antibodies produced against streptococcal antigens and human heart tissue:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Acute glomerulonephritis syndrome
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Rheumatic fever
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Streptococcal toxic shock 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Scarlet fever
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    One of the following is associated with prokaryotes
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  bacterial chromosomes exists as nucleoid in the cell cytoplasm*
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  cell membrane is responsible for the shape of the cell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  cell wall is made of polysaccharides
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  have multiple diploid chromosomes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following statement describes both the Gram (+) and Gram (-) cell envelope?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  They are carriers for the major surface O antigen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  They contain significant amounts of teichoic acid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  They prevent extraction of the CVI complex
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  They act as diffusion barriers allowing certain molecules to enter the cell.*
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   A 20 year old Med Tech student was admitted because of headache, abdominal pain, weakness, one week on and off fever and presence of red spots on the abdomen. The possible causative agent isolated was
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />Salmonella paratyphi A
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />Salmonella typhi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />Salmonella typhimurium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />Salmonella choleraesuis
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of these statements is true of endospores of Bacillus anthracis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Destroyed at 121C, 15 lbs steam pressure for 15 minutes.*
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Has lesser resistance to drying than the vegetative cell>	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Has a high calcium oxalte content
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  It is seen as a violet colored sphere in Gram-stained cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The susceptibility of P. aeruginosa to aminoglycosides is affected by what factor?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  pH of the medium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  cations Ca +2 and Mg+2 content 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Temperature of incubation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Thymidine content in the medium
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Pitting on sheep blood agar is a characteristic demonstrated by 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />Staphylococcus aureus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />Stomatococcus mucilaginosus	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />Eikenella corrodens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />Streptococcus pyogenes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Fungi survive  in enriched matter.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Decaying proteinous substance
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Decaying nitrogenous substances
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Decaying carbohydrate substances
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Oxygenated matters
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A person may suffer from mycotic infection
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  By the use of infected material.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  When undergoing therapy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Upon exposure to patients with long-term antimicrobial therapy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Introduction of the etiologic agent into the traumatized tissue
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    23. Clearing of specimen, dissolving the keratinized tissue making the fungi more visible.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  10% KOHtechnique
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Acid fast stain
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Calcofluor white
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  India ink
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    This fungus was seen in pus of lesions and grows out readily with small, oval to round to cigar – shaped characteristic feature revealing a delicate septate branching mycelium with spores that occur singly or in cloverleaf-like clusters at the extremity of short branches, the portal of entry is through a prick of a thorn.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  H. capsulatum	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  C. neoformans
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  S. schenckii
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  B. dermatitidis
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A yeastlike organism primarily an intracellular parasite of the reticuloendothelial system,  with small, oval to round budding cells, often found clustered within histiocytes.  
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />Blastomyces dermatitidis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />Crytococcus neoformans
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />Sporothrix schenckii
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />Histoplasma capsulatum
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The Tzanck test is used for the diagnosis of
    </b><br><br>
    <p>
        I. herpes simplex  <br> 
        II. smallpox    <br>
        III. varicella      <br>
        IV. measles<br>
    </p>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  I and III only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />II and IV only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />I, II, & III only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />I, II, III & IV
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The method of detection of a virus is usually based on the type of specimen received or the specific virus suspected. It is not practical to process specimen for comprehensive virus detection. However, there is one instance when such comprehensive processing is required, and that is in the case of
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Congenital viral infections
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Infections in immunocompromised hosts
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Respiratory infections in infants
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Blood cultures
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The advantage/s of shell vial culture over conventional virus culture include/s
    <br>
    I.     rapid detection of virus within 24 hours
    <br>
    II.    each shell vial culture can detect more than one type of virus
    <br>
    III. best for viruses that require relatively long incubation period before producing CPE
    <br>
    IV. detection of viruses that cannot grow in conventional culture
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />I and III only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />II and IV only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />I, II, & III only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />I, II, III & IV
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    To demonstrate inclusion bodies in virus-infected tissues, which will you use?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />light microscopy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />PCR
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />electron microscopy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />Western blot
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    To diagnose if a virus has caused infection to the newborn thru transplacental transfer, the best method is to determine specific
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />IgG titer from the blood of the newborn
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  IgM titer from the blood of the newborn
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  IgG titer form the blood of the mother
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  IgM titer form the blood of the mother
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Crystal violet and bile salts on MacConkey agar inhibits growth of
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  gram negative
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  gram positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  fungal contaminants  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  coliform
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Sugar(s) incorporated on Hektoen Enteric agar (HEA) <br>
    1. lactose <br>
    2. sucrose <br>
    3. salicin <br>
    4. glucose
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A selective gram negative broth for pathogenic members of Enterobacteriaceae ( e.g. Salmonella, Shigella)  should be incubated for how many hours prior to subculture in a plated medium?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />2 - 4 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />6 – 8
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />10 – 12
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />16 – 18
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Transport medium of choice for stool specimen suspected of Vibrio spp
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  buffered glycerol saline
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  buffered glycerol PO4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Stuart’s
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Cary-Blair
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    String test using 0.5% sodium deoxycholate is positive for what organism?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Aeromonas 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Vibrio
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Plesiomonas
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Campylobacter
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The tests that can be used to separate V. cholerae from V. parahemolyticus
    <br>1.  sucrose fermentation  
    <br>2.  Oxidase      
    <br>3.  growth in 6% NaCl    
    <br>4.  O129 susceptibility
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 & 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which medium is used to demonstrate the typical “bull’s eye” colonies of Yersinia pestis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />Columbia CNA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />Chopped Meat Medium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />Chapman Stonne agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />Cefsoludin Irgasan Novobiocin agar
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Produce positive result in INDOLE test
    <br>1.  Escherichia coli    
    <br>2.  Proteus vulgaris        
    <br>3.  Klebsiella oxytoca      
    <br>4.  Edwardsiella tarda

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 & 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    H2s producing organisms
    <br>1.  Salmonella typhimurium 
    <br>2.  Proteus mirabilis  
    <br>3.  Edwardsiella  tarda   
    <br>4.  Enterobacter sp
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 & 4 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Phenylalanine/lysine deaminase and urease positive
    <br>1. Proteus     2. Morganella    3. Providencia 4. Citrobacter
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 & 4 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The addition of a 0.033M phosphate buffer glycerol (pH 7.0) supports the viability of pathogens (e.g. Shigella) in what specimen?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  urine
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  stool
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  gastric biopsy  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  sputum
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Can NOT grow in MacConkey
    <br>1. Pseudomonas     2. Haemophilus      3. E. coli       4. Legionella
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />4 only
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The pigment produced by Serratia marcescens on MacConkey agar
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />violacein
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />pyocyanin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />prodigiosin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />flavin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Oxidase positive, MacConkey positive, assacharolytic
    <br>1. Eikenella        2. Moraxella        3. Pasteurella       4. Haemophilus
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 & 4 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Organism from watery stool showed growth on BAP, MacConkey and selective media such as  Hektoen Enteric Agar and Xylose Lysine Desoxycholate Agar producing colorless colonies with black center. Biochemical test revealed this reaction: TSI= K/A no gas, with small amount of H2S, IMVC= -, +, -, - and Urease, ONPG negative This organism is reported as:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Shigella dysenteriae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Salmonella typhimurium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Salmonella enteritidis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Salmonella typhi
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Urine culture from patient with urinary tract infection revealed more than 100,000 CFU./ml on colony count. Colonies on BAP showed a “swarming” appearance which is highly suggestive of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Providencia
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Moraganella
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Edwardsiella
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Proteus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Cultures from a “rice watery” stool revealed grayish, moist, opaque colonies on BAP and yellow colonies on TCBS. Growth however, is NOT seen on media with 6% NaCl or higher.  On gram stain, it exhibits small gram negative bacilli(comma shaped). This is reported as:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Helicobacter pylori
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Campylobacter jejuni
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Vibrio cholera
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Vibrio parahemolyticus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    When infection is suspected of Helicobacter pylori, a portion of gastric biopsy may be placed directly on what broth?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  thioglycollate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  urea
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  tryptophan
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Schaedler
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    An Oxidase positive non-fermentative organism that produces a large amount of H2S in the butt of triple sugar iron (TSI) agar 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Salmonella  typhi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Shewanella putrefaciens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Erysiphilothrix rhusiopathiae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Stenotrophomonas maltophilia
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Media of choice when infection of Burkholderia pseudomallei is suspected
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Regan Lowe 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Buffered charcoal yeast extract
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Ashdown
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Cystine dextrose agarmucus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Helpful for differentiating Pseudomonas aeruginosa from other Pseudomonas species  
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />oxidase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />O-F test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />niacin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />pyocyanin production
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The susceptibility of Pseudomonas aeruginosa to aminoglycosides is affected by what factor?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  pH of the medium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  cations Ca++ and Mg++ content
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Temperature of incubation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Thymidine content in the medium
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Acinetobacter can be separated from the Pseudomonads on the basis of:
    <br>1.  motility
    <br>2.  oxidase
    <br>3.  reduction of nitrate nitrite
    <br>4.  growth on MacConkey

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Alwina, a 17 year old girl has an history of animal bite. The findings of a gram negative bacillus that grows on blood agar but NOT on MacConkey and is oxidase and indole positive and ONPG negative are highly indicative of what probable organism? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Eikenella corrodens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Francisella tularensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Pasteurella multocida
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Brucella canis
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Satellitism of H. influenzae  may be seen on sheep blood agar very close to colonies of Staph. aureus that can produce
    <br>
    1. V factor     2. X factor     3. NAD      4. hemin
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2 and 3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The presence of a gram negative bacillus in pairs or rows (“school of red fish”) on a gram stain smear from a soft chancroid lesion is indicative of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Treponema pallidum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Haemophilus aegypticus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Haemophilus ducreyi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Pseudomonas aeruginosa
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Provide a differential identification of Brucella species
    <br>1.  motility
    <br>2.  urease
    <br>3.  CO2 requirement
    <br>4.  susceptibility to aniline and fuchsin dye
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1, 2, 3 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2, 3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />2 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Recommended transport broth medium for recovery of Bordetella pertusis
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Stuart’s
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Amies
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Casamino acid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Area
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which medium is used for isolation of Haemophilus influenzae while preventing the 
    overgrowth of Pseudomonas aeruginosa?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  horse blood basitracin agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  gentamicin blood agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  chocolate agar with isovitale X
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  blood agar with Staph aureus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The antibiotics disks should be kept at what temperature when not in used?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  -20’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  4’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  28’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  37’C
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    How many antibiotic disks may be placed on the surface of a 150 mm Mueller Hinton agar plate?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />8
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />12
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />20
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />28
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    This uses the principle of diffusion method to generate an MIC value using antibiotic gradient strips
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Kirby-Bauer
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Sherris and Turck   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  E-test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Vitek system
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    For detecting methicillin resistant Staphylococci, incubation temperature is as low as
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  25’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  30’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  37’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  40’C
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following should be ignored while reading the Kirby-Bauer zone of inhibition?
    <br>1.  swarming of Proteus
    <br>2.  bacterial colonies within an obvious zone of inhibition with cephalosporin resistant Enterobacteriaceae
    <br>3.  hazes of growth with sulfonamides-trimethoprim
    <br>4.  hazes of growth with vancomycin resistant enterococci
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Suspension of organisms is introduced unto antibiotic containing wells in the card with automated filling device and once inoculated, these are placed in an incubator-reader module where results are obtained in 6-15 hours. This automated anti-microbial susceptibility system is known as:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  E-test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  VITEK 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Hi-TEK
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  WalkAway
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    For long term storage, Potato Dextrose agar slant for stock cultures of molds may be overlaid with mineral oil and stored at what temperature
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  -20’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  4’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  28’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  37’C
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Acid fast bacilli may be kept on Lowenstein-Jensen agar slants at 4’C for up to how long?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  8 weeks
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  10 months
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  1 year
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  2 years
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Acid fast bacilli may be kept on Lowenstein-Jensen agar slants at 4’C for up to how long?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  8 weeks
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  10 months
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  1 year
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  2 years 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following will preserve the bacterial stock cultures for long period of time?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  freezing at –20’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  lyophilization
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  refrigeration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  overlaying the medium with mineral oil and stored at room temperature
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Electron Microscopy of stool is a significant method of diagnosing viral gastroenteritis agent, which is stained by:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  phosphotungstic acid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  methenamine silver
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  FITC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Metallic acid
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    70. Buffered charcoal yeast extract agar is selective medium for the isolation of Legionella.  This medium contains antibiotics such as polymyxin B, which inhibits the growth of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  gram positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  gram negative
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  yeasts
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What is the most preferred “plasma” for use in the coagulase test?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  human plasma with SPS
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  citrated sheep plasma
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  heparinized horse plasma
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  rabbit’s plasma with EDTA
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Diagnostic method most commonly used to identify anaerobic bacteria:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  BACTEC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Bioluminescent
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Gas Liquid Chromatography
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  IMVic
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Diagnostic of Pseudomonas aeruginosa
    </b>
    <p>
        <br>1.  oxidase positive
        <br>2.  bluish green on Mueller Hinton
        <br>3.  TSI – K/K
        <br>4.  Good growth at 42 deg C

    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />1, 2, 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />1, 2, 3, 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Glucose fermenter, oxidase (+), 0129 sensitive, string test (+), gram (-) rods:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Aeromonas
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Plesiomonas
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Vibrio
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Klebsiella
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Blood agar with Staphylococcus aureus is used to demonstrate what phenomenon of Haemophilus? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Dienes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Swarming
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Dots and Dashes of Morse Code
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Satellitism
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Medium of choice to stimulate growth of C. diphtheriae and production of metachromatic granules:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Tinsdale
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Cystine tellurite blood agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Potassium tellurite blood agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Loeffler’s serum slant
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    For disk interpretation in Kirby Bauer test, swarming of Proteus and sulfonamide-trimethoprim hazes of growth should be
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  not ignored
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  ignored
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  measured
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  repeated
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following is an enrichment and selective medium for the isolation of Bordetella pertusis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Regan Lowe
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Skirrow agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Todd-Hewitt broth
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Schaedler agar
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    An isolate of E. coli recovered from the stool of a patient with severe bloody diarrhea should be tested for which sugar before sending to a reference laboratory for serotyping
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Sorbitol fermentation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Mannitol oxidation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Raffinose fermentation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Sucrose fermentation
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A pure culture from a blood agar grew grayish white, transparent, matte, beta hemolytic colonies and gave the following reactions:
    <br>PYR = +         
    <br>Hippurate test = 0
    <br>VP = 0
    <br>CAMP test = 0
    The organism most likely identification is:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Streptococcus agalactiae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Streptococcus pneumoniae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Streptococcus pyogenes 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Streptococcus dysgalactiae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What is the most appropriate interpretation when a zone of inhibition in an optochin susceptibility test is 13 mm?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Negative
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Equivocal, perform bile soluble test 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d . Repeat the test
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following is observed when azide acts on Bile esculin azide agar with vancomycin?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Inhibition of  gram negative bacteria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Inhibition of gram positive bacteria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Growth of resistant gram positive bacteria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Differentiates Enterococci from other vancomycin-resistant bacteria
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following enzymes contribute to the virulence of Staphylococcus aureus?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Urease and lecithinase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Hyalurunidase and beta lactamase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Beta lactamase and catalase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Hyaluronidase and catalase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A urease test reaction which does not show change in the color of the medium is a presumptive identification of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Corynebacterium diphtheriae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Listeria monocytogenes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Nocardia
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Actinomyces
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following microorganisms shows a presumptive identification which can be made when vaginal discharge shows “clue cells”?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Treponema pallidum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Neisseria gonorrhoeae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Gardnerella vaginalis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Escherichia coli
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following Enterobacteriaceae grows on orange to salmon pink colonies on hektoen enteric agar?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Escherichia 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Proteus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Shigella
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Citrobacter
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Which of the following microorganisms shows a gram stain of exudates from small papule that soon develops into a painful ulcer may show small pleomorphic gram-negative bacilli in clusters  described as "school of fish" morphology?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Haemophilus ducreyi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b. Streptococcus pyogenes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Bacillus anthracis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Corynebacterium diptheriae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Mycobacteria stained with the Zeihl-Neelsen or Kinyoun methods using a methylene blue counterstain are seen microscopically as:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Bright red rods against a yellow background
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Bright yellow rods against a black background
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Orange red rods against a black background
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Bright blue rods against a pink background
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Standardized testing condition for the Kirby Bauer agar disk diffusion antimicorbial susceptibility test includes:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  use of Thayer Martin medium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  standard inoculum size
            </label>
        </li>
         <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  anaerobic incubation
            </label>
        </li>
         <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  8-10% carbon dioxide incubation
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following statements is associated with the Lag Phase of bacterial growth?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  Cells are most susceptible to adverse conditions during this phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Cells are undergoing a period of intense metabolic activity.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  It’s a period of adaptation to the new environment.*
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  The number of cell division occurring is greater than the number of cell death
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    In the CAMP test, a single streak of a beta-hemolytic Streptococci is placed perpendicular to a streak of beta-lysin producing Staphylococcus aureus.  After incubation, a zone of increased lysis in the shape of an arrow-head is noted, which indicates the presumptive identification of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  S. bovis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  S. mitis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  S. equinus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  S. agalactiae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Alwina, a 17-year old girl has history of animal bite.  The findings of a gram (-) bacillus that grows on BAP, but on MC and is oxidase and indole (+) and ONPG (-) are highly indicative of what probable organism:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  Eikenella corrodens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  Francisella tularensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  Pasteurella multocida
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  Brucella canis
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    A suspected Bacillus anthracis culture obtained from a wound specimen produced colonies that have many outgrowths (medusa-head appearance), but were not beta-hemolytic on SBAP.  Which test should be performed next?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  penicillin (10 U) susceptibility test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  lecithinase test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  glucose test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  motility test
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which is the best series of characteristics to demonstrate P. aeruginosa?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  blue-green pigment in media, oxidase (+), grape-like odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  oxidase (-), ferments glucose, blue-green pigment in media
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  oxidase (+), non-motile, grape-like odor, no pigment in media
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  grape-like odor, brown pigment, oxidase (-)
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Heat sensitive materials such as disposable plastic petri dishes, syringes, heart lung machine, catheter devices are sterilized by:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />a.  ultraviolet light radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />b.  ethylene oxide gas sterilization
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />c.  filtration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />d.  autoclaving
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Biological safety cabinet with high efficiency particulate sterilized air by:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  heating
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  aeration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  coagulation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  filtration
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Glasswares and other heat stable items may be sterilized by dry heat at
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  121’C for 15 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  141’C for 20 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  160-170’C for 2 hours
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  180-200’C for 10 seconds
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    This is an excellent sterilizing agent that penetrate deep unto objects
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  UVL radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Ionizing radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Filtration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Adsorption
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Used to control airborne infection
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="1" />A.  UVL radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="2" />B.  Aeration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="3" />C.  Flocculation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"   value="4" />D.  Incineration
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Chocolate agar and modified Thayer-Martin agar are used for the recovery of
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Haemophilus spp. and Neisseria spp. respectively
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Haemophilus spp. and Neisseria gonorrhea, respectively
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Neisseria spp. and Streptococcus spp., respectively
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Streptococcus spp. and Staphylococcus spp. respectively
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Both red and pink colonies from an initial stool culture on MacConkey agar be subcultured and tested further because?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />most Shigella strain are lactose positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />most Salmonella strains are maltose negative
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />most Proteus spp. are lactose negative
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />parhogenic E. coli can be lactose positive or negative
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following is used to control microbial population in the surfaces of laboratory and food handling equipment but does not penetrate the foods?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. gamma radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. sedimentation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. ultraviolet radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. radappertization
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Dehydration is applied for preservation of which of the following?
    <br>1.  meats
    <br>2.  beers
    <br>3.  fish
    <br>4.  fruits
    <br>5.  fruit juices

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,2, 5
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1, 2, 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 3, 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3, 4 and 5
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    .  Ultra-high pasteurization of milk is held at
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. 62.8’C for 30 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. 71’C for 15 seconds
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. 141’C for 2 seconds
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. 170’C for 1 second
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Increasingly used as “indicator organism” for fecal contamination in salt water (e.g. marine)
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. Escherichia coli
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. Enterococcus faecalis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. Enterobacter aerogenes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. Edwardsiella tarda
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Water purification involves removal of disease causing bacteria. Which of the following disinfection process may be applied for safety consumption of water?
    <br>1.  irradiation
    <br>2.  ozonation
    <br>3.  chlorination
    <br>4.  aeration
    <br>5.  adsorption

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1, 2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3, 4 and 5
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which chemicals can be used to preserve foods?
    <br>1.  propionic acid
    <br>2.  sodium nitrite
    <br>3.  ethyl formate
    <br>4.  sulfites
    <br>5.  hydrochloric acid

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1, 2, 3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 3, 4 and 5
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3, 4 and 5
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What is the major yeast used in the production of beer products?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. Lactobacillus acidophilus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. Candida tropicalis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. Streptococcus thermophilus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. Saccharomyces cerevisiae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Technique(s) employed to estimate the number of coliforms in the water?
    <br>1.  Lactose fermentation (L-F) test
    <br>2.  Multiple Tube fermentation 
    <br>3.  Sedimentation test
    <br>4.  Membrane Filtration

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. 1 and 3 A                         
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. 2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. 1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. 1, 2, 3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of he following is preferably used to pasteurize meat, seafoods, fruits and other foods? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. ultraviolet light radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. gas sterilization
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. gamma radiation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. ionizing radiation
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What gives milk products their flavor?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. brewing 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. fermenting microbes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. aflatoxins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. oxidation of glucose
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What is the positive result indicative of the presence of coliform in Colilert defined substrate test  using o-nitrophenyl-β-D-galactopyranoside (ONPG) and 4 –methylumbelliferyl- β-D-glucoronide (MUG) for water bacteriology?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Turbidity of the media
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Gas production 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Media turns yellow 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Precipitate formation 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    If accuracy and precision is acceptable, the quality control anti-microbial susceptibility testing may be reduced from daily to:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A. once a week
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. twice a week
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C. once a month
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D. twice a year
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Associated with external activities that ensure positive patient outcome
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Quality control
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Quality Assurance
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Total Quality Management
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Quality Improvement
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following quality control testing combination results requires selecting organisms that will demonstrate a positive reaction for Moeller’s test?  
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />. Lysine: Klebsiella pneumonie and Ornithine: Entetobacter cloacae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Arginine: Klebsiella pneumoniae and Base: Klebsiella pneuminiae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Arginine decarboxylase: Edwardsiella and Ornithine: Enterobacter cloacae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Ornithine: Klebsiella pneumonie and Enerobacter cloacae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Largest human DNA virus:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Picornaviridae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Poxviridae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Flavoviridae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Hepadnaviridae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Non A Non B hepatitis virus:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Hepadviridae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Hepeviridae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. HC virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. HIV
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Also known as mariner’s wheel fungi
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. P. brasiliensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. S. schenkii
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. B. Burgdoferri
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. C. neoformans 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Protein coat of viruses are composed of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Capsid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Capsomeres
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Peplomer
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Genome
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    The following viral genomes are replicated in the cytoplasm expect
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. ssDNA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. dsRNA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. (+) ssRNA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. (-) ssRNA
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Causative agent of Acute respiratory Distress Syndrome
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Adenovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Arenavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Parvovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Papilomavirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Largest DNA virus with brick shape complex appreance
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Poliovirus	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Poxvirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Picornavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Poliovirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Smallest RNA
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Enterovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Filovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Bunyavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Adenovirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following viruses can be transmitted by arthropod vectors
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Orthomyxovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Arenavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Retrovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Flavivirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Exchange of genes which occur only in segment RNA viruses producing an antigenetically new virus
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Genetic drift
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Genetic reassortment 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Phenotypic masking
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Phenotypic mixing
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Which of the following is caused by aHH7
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Chickenpox
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Chancre 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Infectious mononucleosis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Roseola infantum
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    To detect multinucleated giant gells of herpes infections
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Tzanck smear
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Monospot test 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. TORCH test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Western blot
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Most important cause of gastroenteritis among adults
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Adenovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Norvovirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Rotavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Enterovirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    This virus is the agent of measles
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. A lentiviridae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Oncoviridae 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Spumvirinae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Lymphoviridae
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Small pox:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Flavivirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Morbillivirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Roseovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. None of the above
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
     German measles 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Flavivirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Morbillivirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Roseovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Togavirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Exantem subitum 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Flavivirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Morbillivirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Roseovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. None of the above
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Dengue fever  
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Flavivirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Morbillivirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Roseovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Togavirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Infectious hepatitis 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Delta virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Hepacvirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Hepadnavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Hepatovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />e. Hepevirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Serum hepatitis  
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Delta virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Hepacvirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Hepadnavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Hepatovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />e. Hepevirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    High mortality rate in pregnant women  
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Delta virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Hepacvirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Hepadnavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Hepatovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />e. Hepevirus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    Defective virus:   
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a. Delta virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />b. Hepacvirus 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c. Hepadnavirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />d. Hepatovirus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />e. Hepevirus
            </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Which of the following substances is reabsorbed in the tubules by passive transport? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />glucose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />amino acids	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />chloride
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />urea
            </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	How will failure of the proximal convoluted tubules to secrete hydrogen ions affect the urine pH?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />The pH of urine is consistently alkaline.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />The pH of urine is consistently neutral.	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />The pH of urine is consistently acidic.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />There is no effect on the urine pH.
            </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Glomerular filtration rate would be increased by
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />constriction of the afferent arteriole alkaline.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />a decrease in afferent arteriolar pressure
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />compression of the renal pressure
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />a decrease in the concentration of plasma protein
            </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Which of the following types of urine specimen is ideal for routine analysis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />random
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />catheterized
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1st morning
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />24 hour
            </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Which of the following urine characteristics provides the best rough indicator of urine concentration and body hydration?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />color	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />clarity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />foam
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />volume
            </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Which of  the following criteria should one use to evaluate urine color and clarity consistently?
    </b>
    <p>
    1. Mix all specimens well
	2. Use the same depth or volume of a specimen
	3. Evaluate the specimens at the same temperature
	4. View the specimens against a dark background with good lighting

    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,2 and 3 are correct
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1 and 3 are correct
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />4 is correct
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />all are correct
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Urine specimen containing 2 grams of protein and 1 gram of glucose has a specific gravity of 1.026 using refractometer. What is the true specific gravity reading of the specimen?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1.016
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1.026
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1.015
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1.017
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	A urine specimen has a specific gravity of 1.020 and it has been found to contain 2 gms% glucose. What should be the final corrected specific gravity?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1.010
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1.012
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1.014
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1.015
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Urine with specific gravity reading of 1.003 correlates with
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />diabetes  insipidus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />congestive heart failure
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />dehydration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />diabetes mellitus
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	What chemical parameter is tested by reagent strip containing Methyl red and Bromthymol blue?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />pH 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Protein
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Glucose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ketones
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	What substance is considered to be the most commonly tested and analyzed?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Protein
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Glucose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Ketones
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Bilirubin
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	What substance is considered to be the most indicative of renal disease?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Protein
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Glucose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Ketones
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Bilirubin
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	a negative glucose oxidase test and a positive test for reducing sugars in urine indicates:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />true glycosuria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />a trace quantity of glucose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />a false negative oxidase reaction
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />presence of non glucose reducing sugar *
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	What is the reaction involved in the routine testing for urinary bilirubin using reagent strip?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Ehrlich  reaction 	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Greiss reaction   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Diazo reaction 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Na nitroprusside
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Which information regarding the collection of sample for urobilinogen testing is correct?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Collect a 24 hour urine sample with hydrochloric acid as the preservative 	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Collect between 2 to 4 pm and preserved with hydrochloric acid  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Collect a 24 hour urine sample and placed in a dark bottle
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Collect between 2 to 4 pm and placed in a dark bottle. 
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	A qualitative measurement Resorcinol test is useful in the identification of this non glucose sugar in  urine.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />fructose 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />pentose	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />lactose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />sucrose
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    	Specimens that contain hemoglobin can be visually distinguished from those that contain red blood cells because:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />hemoglobin produces a much brighter red color
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />hemoglobin produces a cloudy, pink specimen	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />red blood cells produce a cloudy specimen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />red blood cells are quickly converted to hemoglobin
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What are the types of cast that are seen in dilated tubules or with stasis in collecting ducts?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Thin casts
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Long casts
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Oxaluria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Tyrosinuria
           </label>
        </li>
    </ul>
</li>





<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   The findings of six-sided puffy crystals in the acid urine of an infant should arouse a suspicion of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Cystinosis	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Cystinuria	  	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Oxaluria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Tyrosinuria
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Casts are formed primarily in which portion of the kidney?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Glomerulus  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Loop of Henle  	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Distal convoluted tubule
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Proximal Convoluted Tubule
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   	Which of the following urinary crystals is the most pleomorphic?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Cystine
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Calcium Oxalate 	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Calcium phosphate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Uric acid
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   	What will happen to hyaline casts in unpreserved urine?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />become granular casts
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />dissolve due to alkaline environment	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />become mucus strands
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />dissolve due to acid environment
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   A urine specimen from a patient with group A streptococcus infection was found to have dysmorphic RBCs and red cell casts.  Which of the following conditions could be present?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Acute glomerulunephritis	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Pyelonephritis		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Nephrotic Syndrome
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Wegener’s granulomatosis
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
  What is the major content of calcium stones?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Uric acid	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Oxalate	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Cystine
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Lysine
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What is the constant finding in urinalysis when renal stones are present?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Pyuria
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Hematuria	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Proteinuria 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cylindruria
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   	The urinary pH affects the formation of stones.   A patient was found positive for a stone made up of calcium oxalate.   The doctor advised him to take foods which will prevent the formation of these stones.   What are these type of foods?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />pork
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />vegetables	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />chicken	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Beef
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   What is  the possible cause of black stool?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />iron therapy	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />lower GIT bleeding
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />bile duct obstruction  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />barium sulfate
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   In Apt test,presence of pink color after addition of NaOH to the emulsion indicates 
             presence of_______ in stool
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />maternal blood	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />fetal blood
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />maternal fats
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />fetal fats
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
  Watery stool contains increased neutrophils,positiveculture ,normalfecal fats and   
              meat fibers.

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Secretory	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Osmotic
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />”factitious”	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />diarrhea due to altered bowel motility
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
What is the reagent of choice for measuring  total CSF protein utilizing the principle f turbidity production
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />SSA	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />TCA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />both	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />none 
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   What cell type is predominantly seen in adult CSF?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Histiocytes	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Macrophages
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Monocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Lymphocytes 
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
    What is the principle of the Coomassie blue dye binding method for measuring CSF protein?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Protein dye precipitation		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Protein error of indicators
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Peptide bond and dye combination
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Protein interferes with dye binding to specific substrates
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Laboratory results of a CSF specimen reveal the following:   elevated WBC count, moderate protein elevation, decreased glucose, positive India ink preparation.   Which of the following conditions would it refer to?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Tuberculous meningitis		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Bacterial meningitis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Fungal meningitis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Viral meningitis
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Which of the following substances is helpful in the assessment of  the secretory function of the seminal vesicles?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />zinc	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />acid phosphatase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />fructose	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />citric acid
           </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
  This sperm function test measures the membrane integrity of sperm cells
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />acrosin assay		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />sperm penetration assay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />hypoosmotic swelling     
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />cervical mucus penetration assay
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 What is the  sperm function test that might be performed following an abnormal eosin nigrosin test result
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Acrosin assay		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Hamster egg penetration  	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Hypo osmotic swelling       
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Sperm penetration assay
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 When should semen specimens be analyzed?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />immediately upon receipt
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />prior to liquefaction  	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />one hour after collection    
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />after a period of liquefaction
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 The semen of Mr  X  fails  to liquefy within 1 hour. What is the possible indication of this finding?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />low sperm count
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />inadequate prostatic secretion  	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />low fructose level   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />increased acid phosphatase
           </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 Calculate the sperm count/ejaculate  using the following information:<br>
Dilution 1:20&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; 100 sperm cells in 5 RBC squares  <br>   
Specimen volume – 2 ml

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />10million/ejaculate 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />20 million/ejaculate  	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />100 million /ejaculate   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />200 million/ejaculate
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
The length of sexual abstinence should be noted in seminalysis. Longer periods of  abstinence before seminalysis usually result in

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />higher semen volume & decreased sperm motility   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />lower semen volume   	&  increased sperm motility	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />higher semen volume   	&  increased sperm motility   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />lower semen volume   	&  decreased sperm motility
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Routine   Semen Analysis has been reported as follows <br>
volume – 3.0 ml	pH – 7.3<br>
viscosity – pours in droplets 30 min after collection<br>
sperm concentration  –10 million/ml	<br>
motility –  50 % ;  grade 1<br>
morphology - 30 % normal morphology using  routine criteria<br>


     What are the abnormal parameters?<br>

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />volume, sperm concentration ,morphology
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />volume , pH, sperm concentration 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />sperm concentration ,  motility, ph
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />sperm concentration, morphology , motility
           </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   The length of sexual abstinence should be noted in seminalysis. Longer periods of  abstinence before seminalysis usually result in
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />higher semen volume 	& decreased sperm motility  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />lower semen volume   	&  increased sperm motility
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />higher semen volume   	&  increased sperm motility
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />lower semen volume   	&  decreased sperm motility
           </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Which of the following is NOT a proper way of collecting semen specimen for testing?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Collection of sample in a sterile container
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Collection after a 3-day period of sexual abstinence 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Collection at the laboratory followed by  1 hour of refrigeration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Collection at home and delivery to the laboratory within 1 hour
            </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
  Pulmonary maturity of a fetus may be determined by amniocentesis testing of what substance?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Bilirubin	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Hexosamidase	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Estriol
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Phospholipids
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 What principle should be applied to measure the change in microviscosity of the amniotic fluid?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Complement fixation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Fluorescence polarization	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Enzyme immunoassay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Hemagglutination inhibition
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 Which of the following pairs of joint disorder and possible cause  is correctly matched?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Non Inflammatory  arthritis– degenerative joint disorder
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Inflammatory  arthritis–  coagulation disorder	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Septic arthritis – Rheumatoid arthritis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Hemorrhagic  arthritis – bacterial infection
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 These  crystals in synovial fluid are highly  birefringent   under polarized light. They will turn  yellow  when aligned with the slow vibration of the compensated polarized light.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Calcium pyrophosphate dihydrate	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Cholesterol	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Monosodium urate 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Calcium oxalate
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 Which of the following is a characterisitic of a normal synovial fluid?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />neutrophils -  65 %	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Leukocyte count – 500 cells/ul	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Glucose level – 10 mg/dl lower than plasma  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />uric acid –  higher  than  blood level
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Elevated peritoneal urea nitrogen and creatinine associated with elevated serum urea but normal serum creatinine suggests:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Bladder rupture		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Acute pancreatitis	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Malignancy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Tuberculous peritonitis  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Classify this serous fluid given the following chemical test results
    </b>
    <p style="float:right; margin-left:20px;">
    Pleural fluid cholesterol level		
	Pleural fluid protein			
	Pleural fluid:serum cholesterol ratio	
	Pleural fluid:bilirubin ratio		

    </p>
    <p>
    120 mg/dL
    220 mg/dL
    0.2
    0.4   
    </p>
    
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Transudate		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Exudate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Exudative transudate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Transudative exudate   
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
The cells seen in a pleural effusion were described as having nuclear division, heavy cytoplasmic staining, cellular clumping and cytoplasmic molding.   What test would you use to diagnose the problem?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Flow cytometry		
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Limulus lysate test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Gram staining
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Antinuclear antibody test  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Methionine malabsorption
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Mousy odor	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />rancid odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Rotting fish odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Sweetened odor  
         </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cabbage hops odor  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Trimethylaminuria
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Mousy odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />rancid odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Rotting fish odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Sweetened odor  
         </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cabbage hops odor  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Hypermethinemia
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Mousy odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />rancid odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Rotting fish odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Sweetened odor  
         </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />Cabbage hops odor  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Diabetes mellitus
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Mousy odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />rancid odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Rotting fish odor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Sweetened odor  
         </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />Cabbage hops odor  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Which of the following parameters is mismatched? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />pH – read immediately
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Protein (CHON) – 60 seconds
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Glucose (CHO) – 30 to 60 seconds
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ketones – 70 to 80 seconds 
         </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />Cabbage hops odor  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
What is the usual infective stage of Malaria or Plasmodium  to man?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Merozoites
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Schizonts
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Sporozoites
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Tropozoites 
         </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />Cabbage hops odor  
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   What is the intermediate host of Diphyllobothrium latum?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Cattle	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Dog
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Fish
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Pig
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   The following trematodes require two intermediate hosts to complete its life cycle, EXCEPT:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Fasciola hepatica	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Clonorchis sinensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Schistosoma japonicum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Fasciolopsis buski
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Which of the following stage/s in the life cycle  of Ascaris will distinguish it from that Trichuris trichiura?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Heart and lung  larval migration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Embryonation in the soil
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Ingestion of embryonated egg
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Passing out of eggs in the feces
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   The diagnostic stage of the following parasites is ova, EXCEPT:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Whipworm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Giant intestinal roundworm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Old world hookworm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Thread worm
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   Hydatid disease in humans maybe caused by the larval stage of hydatid     worm.Which  of the ff.  animals are most likely to be the definitive host for this parasite?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />dogs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />cattle
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />pigs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />sheep
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   In the life cycle of Schistosoma,which of the ff. developmental forms enters     
          the intermediate snail host?

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />cercaria	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />rediae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />trophozoites
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />quadrinucleated cyst
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
   What infective stage of Toxoplasma gondii is passed out in cat’s feces?

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />sporozoites	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />oocyst
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />trophozoites
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />quadrinucleated cyst
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
  Embryonated egg  of Ascaris lumbricoides after being ingested  by man will hatch   in the small intestine then
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />becomes adult worm	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />goes to the lungs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />actively migrate the intestinal wall
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />goes to the portal circulation
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
In the lifecycle of  Plasmodium vivax , some trophozoite invade unaffected liver cell. What will happen to the trophozoite that enters the liver cell?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Trophozoite will develop into Schizont
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Trophozoite will develop into mature trophozoite
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Trophozoite will develop into gametocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Trophozoite will develop into hypnozoite
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
What protozoa resemble an “old man with eyeglasses”?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Chilomastix
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Entamoeba
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Trichomonas
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Giardia  
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Which trophozoite of Entamoeba has 4 nuclei and with occasional 
		inclusions of erythrocytes?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Endolimax nana	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Dientamoeba fragilis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Entamoeba coli
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Entamoeba histolytica
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Which of the ff. pairs of helminths cannot be reliably differentiated by the ________ appearance of their eggs?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Whipworm and Pudoc worm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Rat tapeworm and Dwarf tapeworm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Pork tapeworm and Beef tapeworm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Fish tapeworm and Dog tapeworm
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
The following parts/structures maybe observed in adult tapeworm,EXCEPT:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />acetabulum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />ripe segment	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />genital pore
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />scolex
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
The fertilized eggs of the following parasites are found in the single cell  
              stage EXCEPT:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Pudoc worm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Giant intestinal roundworm	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Whipworm
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Hookworm
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Lemon-shaped cyst, uninucleteated can be confused with Trichomonas in
		the laboratory :

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Chilomastix mesnili
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Giardia lamblia	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Ascaris lumbricoides
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Taenia solium
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Egg that show knob-like spine or projection on one side is characteristics of

    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />amoeba
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Hookworm	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Paragonimus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Schistosoma
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Flagellates shaped like a half pear, pathogenic intestinal protozoa, mature cysts has 4 nuclei, has 8 flagella is    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Chilomastix mesnili
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Trichomonas vaginalis	            
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Giardia lamblia
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Trichomonas hominis     
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Which trophozoite of Entamoeba has 4 nuclei and with occasional inclusions of erythrocytes?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Endolimax nana
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Dientamoeba fragilis           
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Entamoeba coli
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Entamoeba histolytica
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 Kidney shaped caudal bursa is characteristic of what male nematode?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />D. medinensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B. malayi	        
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />A. cantonensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />W. bancrofti 
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 Buccal cavity has two pairs of ventral teeth:
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Necator americanus	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Ancylostoma duodenale	        
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Ancylostoma brazilliense
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ancylostoma caninum
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 The following parasites possess four suckers,EXCEPT:
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />H. heterophyes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />H. nana		        
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />T. solium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />R. garrisoni
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
This egg is characterized to have a barrel-shaped with transparent plug
                at each pole:

   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Hookworm ova	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Ascaris ova			        
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Taenia ova
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Trichuris ova
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Characterized by 3 pairs of ventral teeth:
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Necator americanus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Ancylostoma duodenale			     
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />ancylostoma brazilliense
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />ancylostoma caninum
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
What is the solution used in Kato katz technique to clear the parasite?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />10% Formaldehyde
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Glycerine			     
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />NSS
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />PVA
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
A watery stool specimen was submitted to the laboratory 2 hours after collection. What will most likely happen to the trophozoites if present?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Their movement will be more visible
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />They will remain intact			     
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />They will disintegrate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />They will proliferate
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
In cases  of low microfilaremia, how many ml of 2 % formalin should be added to ____________  2 ml of blood?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />5
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />10			     
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />15
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />20
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 How would you differentiate the time of collection of blood in the diagnosis of Malaria and Wuchereriasis?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Blood should be collected between 10PM to 2 AM for  diagnosis of Wuchereriasis.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Blood may be collected any time of the day for the recovery of malarial parasite		     
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Blood may be collected at the height of fever  for the diagnosis of Wuchereriasis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Blood should be collected during day time only for both diseases.
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
 A sputum examination was requested from a patient who was suffering from chest pains, dyspnea, dry cough and hemoptysis. Results shoed that the sputum contained a coffee-bean shaped parasite with a flattened operculum with abopercular thickening. Which of the following parasite was most likely isolated?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Strongyloides stercoralis
B.)	Paragonimus wastermani

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Paragonimus wastermani
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Cryptosporidium parvum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Echinococcus granulosus
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
A cellulose scotch tape swab was submitted in the laboratory . Which of the following ova or parasites is most likely to be recovered?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Mammilated with fine granules

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />belongated with one side flattened
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Barrel-shaped with prominent bipolar plugs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ovoidal with 2-8 cell stages
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Clearing of specimen, dissolving the keratinized tissue making the fungi more visible.
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />10% KOH

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Acid fast stain
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Calcofluor white
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ovoidal with 2-8 cell stages
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
To demonstrate inclusion bodies in virus-infected tissues, which will you use?
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />light microscopy	

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />PCR
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />electron microscopy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Western blot
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Salmonella species are characteristically:
   </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />nonmotile   

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />oxidase positive	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />lactose fermenter   
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />H2S producer
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
All Enterobacteriaceae ferment what sugar(s)
   </b>
   
   <p>
5.	lactose  	<br>	
6.	sucrose		<br>	
7.	mannitol	<br>		
8.	glucose 	<br>	
 </p>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 3

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 2 and 3	 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />4 only
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
H2s producing organism
   </b>
   
   <p>
5.	Salmonella typhimurium  	<br>	
6.	Proteus mirabilis			<br>	
7.	Edwardsiella  tarda			<br>		
8.	Enterobacter sp 			<br>	
 </p>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 3

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 2 and 3 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3 & 4 
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Phenylalanine/lysine deaminase and urease positive 
   </b>
   
   <p>
1. Proteus  	<br>	
2. Morganella	<br>	
3. Providencia	<br>		
4. Citrobacter 	<br>	
 </p>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 3

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 2 and 3 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3 & 4 
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
Phenylalanine/lysine deaminase and urease positive 
   </b>
   
   <p>
1. Proteus  	<br>	
2. Morganella	<br>	
3. Providencia	<br>		
4. Citrobacter 	<br>	
 </p>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 3

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 2 and 3 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3 & 4 
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	The addition of a 0.033M phosphate buffer glycerol (pH 7.0) supports the viability of 
         pathogens (e.g. Shigella) in what specimen?	
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />urine

            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />stool
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />gastric biopsy 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />sputum 
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Which of the following statement describes both the Gram (+) and Gram (-) cell envelope?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />They are carriers for the major surface O antigen.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />They contain significant amounts of teichoic acid.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />They prevent extraction of the CVI complex. 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />They act as diffusion barriers allowing certain molecules to enter the cell.
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	A 20 year old Med Tech student was admitted because of headache, abdominal pain, weakness, one week on and off fever and presence of red spots on the abdomen. The possible causative agent isolated was: 
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Salmonella paratyphi A
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Salmonella typhi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Salmonella typhimurium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Salmonella choleraesuis
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	The modified Kinyoun method does NOT require heating to stain the acid fast bacilli due to high concentration of:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />tergitol
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />phenol
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />acidic dye
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />malachite green
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	The color of the fluorescent light depends on the dye and light filters used.  Hence, dyes such s auramine and fluorescent isothiocyanate (FITC) reagent is used, the light filter used is which color?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />green
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />red
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />violet
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />blue
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Which medium is used for isolation of Haemophilus influenzae while preventing   
        the overgrowth of Pseudomonas aeruginosa?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />horse blood basitracin agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />gentamicin blood agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />chocolate agar with isovitale X
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />blood agar with Staph aureus
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	The antibiotics disks should be kept at what temperature when not in used?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />-20’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />4’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />28’C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />37’C
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Blood agar with Staphylococcus aureus is used to demonstrate what phenomenon of Haemophilus?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Dienes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Swarming
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />28’CDots and Dashes of Morse Code
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Satellitism
         </label>
        </li>
    </ul>
</li>




<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Medium of choice to stimulate growth of C. diphtheriae and production of metachromatic granules:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Tinsdale
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Cystine tellurite blood agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Potassium tellurite blood agar
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Loeffler’s serum slant
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Mycobacteria stained with the Zeihl-Neelsen or Kinyoun methods using a methylene blue counterstain are seen microscopically as:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Bright red rods against a yellow background
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Bright yellow rods against a black background
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Orange red rods against a black background
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Bright blue rods against a pink background
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Standardized testing condition for the Kirby Bauer agar disk diffusion antimicorbial susceptibility test includes:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />use of Thayer Martin medium
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />standard inoculum size
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />anaerobic incubation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />8-10% carbon dioxide incubation
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Biological safety cabinet with high efficiency particulate sterilized air by:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />heating
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />aeration
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />coagulation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />filtration
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Glasswares and other heat stable items may be sterilized by dry heat at:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />121’C for 15 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />141’C for 20 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />160-170’C for 2 hours
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />180-200’C for 10 seconds
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	What is the positive result indicative of the presence of coliform in Colilert defined substrate test  using o-nitrophenyl-β-D-galactopyranoside (ONPG) and 4 –methylumbelliferyl- β-D-glucoronide (MUG) for water bacteriology?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Turbidity of the media
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Gas production
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Media turns yellow
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Precipitate formation
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Alwina, a 17 year old girl has an history of animal bite. The findings of a gram negative bacillus that grows on blood agar but NOT on MacConkey and is oxidase and indole positive and ONPG negative are highly indicative of what probable organism? 
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Eikenella corrodens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Francisella tularensis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Pasteurella multocida
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Brucella canis
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	If a swab is used to collect specimen, polyester-tipped swab on a plastic shaft is acceptable for most organisms.  Calcium alginate swabs are to be avoided when suspecting:
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Herpes simplex virus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Chlamydia trachomatis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Neisseria gonorrhoeae
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Corynebacterium diphtheria
         </label>
        </li>
    </ul>
</li>

<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Routine   Semen Analysis has been reported as follows 
volume – 3.0 ml	pH – 7.3 <br>
viscosity – pours in droplets 30 min after collection<br>
sperm concentration  –10 million/ml	<br>
motility –  50 % ;  grade 1<br>
morphology - 30 % normal morphology using  routine criteria<br>

     What are the abnormal parameters?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />volume, sperm concentration ,morphology	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />volume , pH, sperm concentration 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />sperm concentration ,  motility, ph	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />sperm concentration, morphology , motility
         </label>
        </li>
    </ul>
</li>


<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	Calculate the sperm count/ejaculate  using the following information:
Dilution 1:20   <br>          100 sperm cells in 5 RBC squares  <br>   
Specimen volume – 2 ml<br>
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />10million/ejaculate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />20 million/ejaculate 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />100 million /ejaculate	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />200 million/ejaculate
         </label>
        </li>
    </ul>
</li>



<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="microbiology-<?php echo $i; ?>">
    <b>
	What is the major content of calcium stones?
 	</b>

    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Uric acid	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Oxalate 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Cystine	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Lysine
         </label>
        </li>
    </ul>
</li>

<?php endif; ?>
</div>
<?php include 'inc/header.php'; ?>
<div id="question-form-wrap">
    <h4><strong>Choose one</strong></h4>
    <div id="question-form">
        <label> <input type="radio" name="type" value="Haematology">Haematology</label>&nbsp;&nbsp;
        <label> <input type="radio" name="type" value="Biochemistry">Biochemistry</label>&nbsp;&nbsp;
        <label> <input type="radio" name="type" value="Blood transfusion">Blood transfusion</label>&nbsp;&nbsp;
        <label> <input type="radio" name="type" value="Microbiology">Microbiology</label>
        <br>
        <textarea name="question" rows="5" placeholder="Enter your question here"></textarea>
        <br>
        <input type="text" name="option-a" placeholder="Option A">
        <input type="text" name="option-b" placeholder="Option B">
        <input type="text" name="option-c" placeholder="Option C">
        <input type="text" name="option-d" placeholder="Option D">
        <br>
        <textarea name="answer" rows="5" placeholder="Your answer and explaination"></textarea>
        <br>
        <br>
        <input type="submit" value="Submit your question" id="question-form-submit">
        <h3>Send your own question and help other test takers</h3>
        <br>
        <br>
    </div>
</div>
<!-- <div class="banner">
    <img src="images/books.jpg"  alt="">
    <img src="images/owl_picture.jpg" alt="">
</div> -->
<span class="button" id="start-timer-btn">Start</span>
<span id="timer">45:00</span>
<span class="button" id="stop-timer-btn">Stop</span>
<h3>Page <span id="page-number">1</span></h3>
<div class="content left">
    <div class="right timer"></div>
    <br>
    <br>
    <div class="qset">
		<ol id="question-list">
			
		</ol>
	</div>
    <div id="cbtn"><button id="calculate-score-btn">Calculate Score</button></div>
    <br>
    <br>
    <div class="center-x">
        <button id="back-page">Back</button>
        <button id="next-page">Next</button>
    </div>
    <br>
    <br>
</div>
<?php include 'inc/footer.php'; ?>
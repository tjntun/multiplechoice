<?php
require_once 'config.php';
if ($_SERVER['REQUEST_METHOD']=='POST') {
	$post = $_POST;

	$body             = '<strong>Question: <br>';
	$body             .= $post['question'];
	$body             .= '<br>';
	$body             .= '<strong>Type</strong>: ' . $post['type'];
	$body             .= '<br>';
	$body             .= '<strong>Option A</strong>: ' . $post['optionA'] . '<br>';
	$body             .= '<strong>Option B</strong>: ' . $post['optionB'] . '<br>';
	$body             .= '<strong>Option C</strong>: ' . $post['optionC'] . '<br>';
	$body             .= '<strong>Option D</strong>: ' . $post['optionD'] . '<br>';
	$body             .= '<br>';
	$body             .= '<strong>Answer</strong>: <br>';
	$body             .= $post['answer'];

	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail(RECEIVER_EMAIL, 'Question form from user', $body, $headers);
	echo 0;
}
?>
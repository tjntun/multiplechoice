jQuery(document).ready(function($) {
	$('html,body').animate({
      scrollTop: $('#timer').offset().top
    }, 1000);

	// Get question
	// ---------------------------------------------
	$.get('hematology.php', function(data){ 
	  $(data).appendTo("#question-list");
	  	$.get('immunology.php', function(data){ 
		  	$(data).appendTo("#question-list");
		  	$.get('biochemistry.php', function(data){ 
			  	$(data).appendTo("#question-list");
			  	$.get('microbiology.php', function(data){
				  	$(data).appendTo("#question-list");
				  	whenQuestionLoaded();
				});
			});
		});
	});
	
	function whenQuestionLoaded () {
		// Show page when click next/back
		$('.tab-item').addClass('flyout');
		$('#tab-1').removeClass('flyout');

		// When click the answer
		$('.answer-radio').click(function(event) {
			var $li = $(this).parent().parent().parent().parent();
			var liId = $li.attr('id');
			var qNum = liId.split('-')[1];
			var qType = liId.split('-')[0];
			var answer = $(this).val();

			$li.find('input[type=radio]').attr('disabled',true);
			$.ajax({
				url: 'answers/'+qType + '-answer.php',
				type: 'POST',
				data: {
					question: qNum,
					answer: answer
				},
			})
			.done(function(res) {
				$li.find('input[value='+res+']').parent().addClass('correct-answer');
			});
		});
	}


	// Show page when click next/back
	// ---------------------------------------------
	var currentPage = 1;
	$('#next-page').click(function(event) {
		if (currentPage < 4) currentPage++;
		$('.tab-item').addClass('flyout');
		$('#tab-'+currentPage).removeClass('flyout');
		$('#page-number').text(currentPage);
		showHideNextBack();
		$('html,body').animate({
          scrollTop: $('#timer').offset().top
        }, 1000);
	});
	$('#back-page').click(function(event) {
		if (currentPage > 0) currentPage--;
		$('.tab-item').addClass('flyout');
		$('#tab-'+currentPage).removeClass('flyout');
		$('#page-number').text(currentPage);
		showHideNextBack();
	});
	showHideNextBack();
	function showHideNextBack () {
		if (currentPage == 1) {
			$('#back-page').hide();
		}else{
			$('#back-page').show();
		}
		if (currentPage == 4) {
			$('#next-page').hide();
		}else{
			$('#next-page').show();
		}
	}
	
	// Count down timer
	// ---------------------------------------------
	var timeout;
	var min = 44, sec = 60;
	function countDown(){
		sec--;
	  	if(sec < 0) {
	  		min--;
	  		sec = 59;
	  	}
	  	if (min < 0) {
	     	clearTimeout(timeout);
	     	whenTimeUp();
	     	return;
	  	}
		$('#timer').html(min+":"+sec); // watch for spelling
		timeout = setTimeout(countDown,1000);
	}
	function whenTimeUp () {
		alert("Time's up");
		calculateScore();
	}
	// Start timer
	$('#start-timer-btn').click(function(event) {
		countDown();
	});
	$('#stop-timer-btn').click(function(event) {
		clearTimeout(timeout);
	});


	// Send question form
	// ---------------------------------------------
	$('#question-form-submit').click(function(event) {
		var question = $('#question-form [name=question]').val();
		var type = $('#question-form [name=type]:checked').val();
		var optionA = $('#question-form [name=option-a]').val();
		var optionB = $('#question-form [name=option-b]').val();
		var optionC = $('#question-form [name=option-c]').val();
		var optionD = $('#question-form [name=option-d]').val();
		var answer = $('#question-form [name=answer]').val();
		var error = false;
		if (question == '' || type == undefined || answer == '') {
			error = true;
		}
		if (!error) {
			$('#loading-wrap').show('fast');
			$.ajax({
				url: 'question-form.php',
				type: 'POST',
				data: {
					question:question,
					type:type,
					optionA :optionA ,
					optionB :optionB ,
					optionC :optionC ,
					optionD :optionD ,
					answer:answer,
				},
			})
			.done(function(res) {
				if (res == '0' || res == 0) {
					$('#loading-wrap h1').text('Thank you. Your question form is sent.');
				}else{
					$('#loading-wrap h1').text('Can not send. Please try again later.');
				}
				setTimeout(function(){
					$('#question-form-message').val("");
					$('#loading-wrap').hide('fast');
				},3000);
			});
			
		}else{
			alert('Please enter your all required fields!');
		}
	});

	

	// calculate-score
	// ---------------------------------------------
	$('#calculate-score-btn').click(function(event) {
		calculateScore();
	});

	var setLabel = ['Hematology','Immunoserology and Blood Bank','Biochemistry','Microbiology'];

	function calculateScore () {
		var data = {};
		var emptyAns = $('input[type=radio]:checked').length;
		
		if (emptyAns != 0) {
			var msg = "Your score is: \n";
			for(var i = 1; i <= 4; i++){
				var correctAnsInSet = 0;
				$('#tab-'+i+' .correct-answer').each(function(index, el) {
					if ($(el).find('input:checked').length != 0) {correctAnsInSet++;}
				});
				var correctPercent = (correctAnsInSet/15*100);
				correctPercent = correctPercent.toFixed(0);
				msg += setLabel[i-1]+": "+ correctPercent +"% - "+correctAnsInSet+"/15 \n";
			}
			alert(msg);
			if (confirm('Do you want to take another test?')) {
				location.reload();
			}
		}else{
			alert('You do not answer any question. Please try to answer before submit');
		}
	}
});
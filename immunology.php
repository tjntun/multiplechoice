<?php include 'inc/random.php'; ?>
<?php $i = 0; ?>
<div class="tab-item" id="tab-2">
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is a characteristic of primary response but not of the secondary response?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Production of IgG
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Long lag phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />High antibody titer
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Long plateau stage
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The following are markers of B cell population, EXCEPT:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Rosette formation with antibody coated sheep RBC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Presence of membrane associated T3 complex
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Presence of membrane associated IgM
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Presence of membrane associated IgG
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The presence of HbsAg, anti-HBc and often HbeAg is characteristic of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Early acute phase HBV hepatitis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Early convalescent phase HBV hepatitis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Recovery phase of acute HBV hepatitis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Carrier state of acute HBV hepatitis
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Who was the first to discover the phenomenon of phagocytosis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Louis Pasteur
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Elie Metchnikoff
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Edward Jennner
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Paul Erlich
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Who postulated the side-chain receptor theory of antibody synthesis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />E. Jenner
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />P. Erlich
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />J. Bordet
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />K. Landsteiner
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What non-specific anti-viral substance released by virally infected cells
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Complement
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Interleukin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Interferon
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Opsonin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following belongs to the innate system of the immune defense of our body?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Plasma cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />T cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Macrophages
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Antibodies
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What type of immunity is conferred by transfer of antibodies across the placenta from the mother to fetus and later acquired through breast milk
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />natural active immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />natural passive immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />artificial passive immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />artificial active immunity
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The major role of natural killer cells is lysis of virally infected cells.  Which one of the following molecules is released by activated natural killer cells and causes destruction of the infected cell?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />major basic protein
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />perforin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />superoxide
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />lysozyme
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
        Arrange the following steps of phagocytosis in its proper sequence:<br>
        <p style="float:left; margin-right:19px">
            I.	Ingestion of bacteria	<br>	
            II.	Digestion of bacteria   <br>
        </p>
        <p>
            III. Movement of phagocyte  <br>
            IV. Adherence of bacteria to the phagocyte<br>
        </p>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />I, II, III, IV
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />II, III, IV, I
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />III, IV, I, II
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />IV, I, II, III
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is an example of  a delayed type of hypersensitivity reaction? 	
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />ABO incompatibility
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Arthus reaction
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Bee stings
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Contact dermatitis
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The patient complains of frequent colds, sneezing and itchy eyes especially in the morning.  He was referred to an allergist to perform skin test with different allergens.  The patient had a wheal and flare reaction with several pollens.  The likely event to produce the patient reaction with different pollens is : 	
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />The antigen binds to IgM in the plasma which activates complement to produce C3b
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />The antigen binds with specific IgE bound to mast cells and potent chemical mediators were released
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />The antigen binds with antigen specific IgE bound to plasma cell and potent chemical mediators were released
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />The antigen binds to IgE in  the plasma and the antigen-Ig E complex binds the surface macrophages and histamine is released
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the most common method used for rheumatoid factor (RF) determination?	
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Indirect Immunofluorescent Test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Enzyme Linked Immunosorbent Assay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Latex Fixation test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Radio Immunoassay test
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the reference method used for the detection and quantitation of rubella antibody?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Indirect immunofluorescence
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Enzyme Linked Immunosorbent Assay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Hemagglutination-Inhibition
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Radio Immunoassay test
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In what test for syphilis does biological false-positive reaction occurs the least?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />VDRL
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />RPR
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />FTA-ABS
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />TRUST
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In the Weil-Felix test, the antibody that is strongly reactive in patient with Scrub typhus fever is directed against which of these antigens?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Proteus OX-K
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Proteus OX-2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Proteus OX-17
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Proteus OX-19
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A 3-year old boy came to the OPD for PPD (Purified Protein Derivative) skin test.  After 48 hours an induration developed on the site of injection.  Histologically, the site of reaction would most probably show predominance of :
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />B cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Macrophage
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />PMN
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />T cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A newborn was found to be with abnormal facie, decrease parathyroid hormone, with interrupted aortic arch and an absent of thymus. The most appropriate step is to detect deficiency for:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />B cell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B and T cells	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />complement
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />T cell
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The surface markers of lymphocytes can be identified in a flow cytometric analyzer.  Two sets of cells were analyzed, results show the presence of CD19 and CD20 markers in set A, and CD4 marker in set B.  Which of the following are correct:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Set A are B cells, Set B are T helper cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Set A are B cells, Set B are T suppressor cells	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Set A are T helper cells, Set B are T suppressor cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Set A are T suppressor cells, Set B are B cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Serum immunoglobulins are produced by plasma cells which are a progeny of…
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Transformed T cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Transformed monocyte	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Transformed B cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Stem cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which are responsible for humoral immunity?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />T cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />B cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Monocyte
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Neutrophil
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which characteristic separates T lymphocyte from B lymphocyte?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />T cells form rosettes in the presence of chicken RBC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />T cells form rosettes in the presence of sheep RBC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />T cells may transform into plasmacytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />B cells form rosettes in the presence of sheep RBC
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the technique employed in the separation of lymphocytes from other formed elements of the blood?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Ficoll-Hypaque technique
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Nitroblue tetrazolium dye test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Enzyme-linked immunosorbent assay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Immunochromatography
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is the most immunogenic?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Lipid	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Nucleic acid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Carbohydrate
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Protein
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Arrange the following series of events in the maturation of a B lymphocyte.
    </b><br><br>
    <p style="float:left; margin-right:19px">
        I – Lymphoid stem cell		<br>	
        II – mature B cell				
        <br>
    </p>
    <p>
        III – plasma cell<br>
        IV – pro B-cell<br>
    </p>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />I- II- III- IV
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />I-II-IV-III
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />I-IV-II-III
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />I-IV-III-II
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Monoclonal antibodies are used in:
    </b><br><br>
    <p>
        1.	somatic cell fusion<br>
        2.	hybridoma selection<br>
        3.	immunoassays<br>
        4.	reagent preparations<br>
    </p>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />2, 3, 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3, 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In Cold agglutination screening test, patient cells and patient serum are first incubated at what temperature?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />4&deg;C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />15&deg;C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />20&deg;C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />35&deg;C
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The mother of infant A had a toxoplasmosis during gestation but no placental rupture occurred before birth. Infant A shows signs and symptoms of having acquired a congenital infection of T.gondii. What is the test to be done to diagnose if infant A has congenital infection?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Enzyme Immunoassay to test for IgG antibodies in infant's serum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Indirect Fluorescent Antibody Technique to test for IgM antibodies in infant's serum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Enzyme Immunoassay to test for IgM in maternal serum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Infant A has no congenital infection since placental rupture did not occur
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    When inactivation of serum has elapsed for 4 hours, serum is re-inactivated by heating at
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />56&deg;C for 30 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />56&deg;C for 20 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />56&deg;C for 10 minutes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />56&deg;C for 5 minutes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In Davidsohn test, antibodies that agglutinate sheep RBC after absorption with sheep RBC and guinea pig RBC is a characteristic of:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Infectious mononucleosis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Serum sickness
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Forsman antibodies
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cold agglutinins
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cause false positive in ASO titration
    </b>
    <p>
        1.	oxidized SLO
        2.	beta lipoprotein
        3.	bacterial contamination
        4.	use of reduced SLO
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1, 2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1, 2, 3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the most frequent immunologic deficiency disorder?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />selective IgA deficiency
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />DiGeorge syndrome
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Bruton's disease
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ataxia telangiectasia
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following parameters is elevated in the convalescence phase of Infectious mononucleosis?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />heterophil antibody
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti EBNA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Anti EBVCA IgM
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Anti EBEA
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The following reactions were obtained from 
    </b><br><br>
    <table border="none">
        <tr>
            <td>Typhi Dot test</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td>IgM</td>
            <td>IgG</td>
        </tr>
        <tr>
            <td>Sample</td>
            <td>+</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Control</td>
            <td>+</td>
            <td>+</td>
        </tr>
    </table>
    <br>
    This is positive for
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />acute typhoid fever
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />middle stage typhoid fever
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />past typhoid fever
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />epidemic typhus fever
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In case of drug-induced SLE, which of the following autoantibodies is found to be elevated?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Anti-double stranded DNA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Anti-extractable nuclear antigen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Anti-histone
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Anti-centromere
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A 25-year-old woman is in kidney failure and needs a transplant. She is A positive an exhibits the HLA antigens A1A3, B5B7, C2C9, and DR4DR8. Which of the following donors would be the best match?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />her brother who is B positive with HLA antigens A1A3, B7B5,C2C9, and DR8DR4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />her sister who is A positive with HLA antigens A3A4,B7B5, C2C9, and DR8DR7
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />her mother who is AB positive with HLA antigens A1A4, B9B5, C9C3, and DR8DR7
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />an unrelated donor who is A positive with HLA antigens A6A3, B5B7, C2C10, and DR4DR9
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The results of a quantitative VDRL are as follows:
    </b>
    <p>
        1:1&nbsp;&nbsp;	1:2&nbsp;&nbsp;	1: 4&nbsp;&nbsp;	1:8&nbsp;&nbsp;	1:16<br>
        WR&nbsp;&nbsp;&nbsp;&nbsp;	R&nbsp;&nbsp;&nbsp;&nbsp;	R&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	R&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	NR
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />8
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />16
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    An antinuclear antibody screen exhibits diffuse, peripheral and coarse-speckled fluorescence. All of the controls are acceptable. No other samples tested in the same run exhibit multiple patterns of fluorescence. The medical technologist should:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Concentrate the sample exhibiting multiple patterns and reassay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Dilute the sample in question and reassay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Report the types of fluorescence seen; this is not unusual
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Run an LE preparation to confirm
            </label>
        </li>
    </ul>
</li>
<b>
Used to detect febrile agglutinins<br>
1.	Widal<br>
2.	Weil-Felix<br>
3.	Brucella agglutination <br>
4.	Paul-Bunnel<br><br>
</b>
<ul>
    <li>
        <label>
        <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />1 and 3
        </label>
    </li>
    <li>
        <label>
        <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="6" />2 and 4
        </label>
    </li>
    <li>
        <label>
        <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="7" />Report the types of fluorescence seen; this is not unusual
        </label>
    </li>
    <li>
        <label>
        <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="8" />Run an LE preparation to confirm
        </label>
    </li>
</ul>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following fragments is produced during pepsin digestion but not during papain digestion of an antibody molecule:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Heavy chain
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Fab
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Fc
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />2(Fab)
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The placental leakage of maternal antibody into the baby's circulation during pregnancy is an example of…
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Natural passive immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Natural active immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Artificial active immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Artificial passive
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following techniques employs the movement of both antigen and antibody in an agarose gel medium?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Electroimmunodiffusion
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Ouchterlony technique
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Radial Immunodiffusion
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Immunofixation
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If 1 ml of patient serum is added with 9 ml normal saline solution (NSS), what is the final dilution?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1:1
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1:5
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1:10
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1:20
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The gauge needle used to deliver the reagent for CSF syphilis test
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />18
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />19
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />20
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />22
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In western blot, bands in p24, GP41, GP120 or GP160 region is considered
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Positive result and confirmatory for HIV infection
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Negative result and confirmatory for HIV infection
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Positive result but not confirmatory for HIV infection
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Negative result but not confirmatory for HIV infection
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Chemically induced tumors have tumor-associated transplantation antigens(TATS) that…
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Are very strong antigens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Do not induce responses
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Are always the same for a given carcinogen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Are different for tumors of different histologic type even if induced by the same carcinogen
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Alpha-feto protein(AFP) is elevated in the blood of patients with hepatocellular carcinoma and also of non-malignant disease of the liver like cirrhosis and hepatitis.  What is the most likely reason of this statement.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />AFP is contagious
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />AFP is a major serum protein in the fetus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />AFP metastasize to the liver and other organs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />AFP is a marker for neural tube defects
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This oncofetal antigen is not tumor-specific and a parameter for the early detection of recurrence or relapse after surgical removal of the tumor.  Which of the following actions should the medical technologist apply when performing this test: 
    </b>
    <p>  
        I – He should not use this as screening test for this cancer <br>
        II- He should monitor the tests every 2-3 months post operatively<br>
        III – He may use this to monitor treatment of metastases<br>
        IV – He may use different immunoassay and will produce accurate and precise report<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />I & II
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />III & IV
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />I, II & III
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />I, II, III, & IV
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The VDRL test for syphilis should be rotated at what rpm?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />100
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />150
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />180
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />200
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    When so –called prozone phenomenon happened, what is the next to be done?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Add more antigen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Add more antibody
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Do antibody dilution
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Do antigen dilution
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The first line of defense in protecting the body from infection includes all of the following, except: 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Unbroken skin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Normal microbial flora
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Phagocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Secretions such as mucus
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Titers in ASO testing are expressed using what units? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Todd
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />International units
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Either
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Aso testing detects antibodies against:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />S. pyogenes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Streptolysin O
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Streptolyisn S
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Proteus Vulgaris
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    All of the following are under true treponemal testing for syphilis, except:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />FTA-ABS
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />VDRL
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />MHA-TP
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />EIA
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is an accurate statement about monoclonal antibodies?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />MAbs are antibodies engineered to bind to a single epitope
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />MAbs are used to classify and identify specific cellular membrane characteristics
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />MAbs are purified antibodies cloned from a single cell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />All of the above
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    During serum electrophoresis, all immunoglobulins are found in the gamma region except? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />IgG
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />IgA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />IgM
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />IgD
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    With which cell type are anaphylactic reactions associated? 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />B and T cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Monocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Plasma Cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Mast cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Type IV reactions are responsible for all of the following except: 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Contact sensitivity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Skin testing
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Elimination of tumor cells bearing neoantigens
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Hemolysis of the red cell
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cytotoxic reactions are characterized by the interactions of
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />IgG to soluble antigen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />IgG to cell- bound antigen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />IgM to soluble antigen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />IgM or IgG to cell bound antigen
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is associated with an increase in IgE production?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Transfusion reactions
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Exposure to an allergen such as shrimps
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Reaction to poison ivy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Hemolytic disease of the newborn
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Complement proteins can be inactivated in human serum by which of the following methods?
    </b>
    <br>
    <p>
        i.	Heating at 56 deg celcius for 30 mins<br>
        ii.	Using choline chloride<br>
        iii.	Heating at 60-62 deg celcius for 10mins<br>
        iv.	Using thimerosal<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,2,3,4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1,2,3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1 only
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A specimen should be re-inactivated when more than 4 hours has elapsed since inactivation. What is the process of reinactivation?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />56 deg Celsius for 30mins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />56 deg Celsius for 15mins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />56 deg Celsius for 10 mins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />60-62 deg Celsius for 5 minutes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A shyphilitic stage that is characterized by its lack of chemical symptoms and it is non infectious 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Primary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Secondary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Tertiary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Latent
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Localized areas of granulomatous inflammation on skin was seen in patients with syphilis. This stage is:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Primary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Secondary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Tertiary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Latent
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The presence of congenital syphilis from a newborn can be diagnosed by what laboratory procedure?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />ELISA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Western Blot
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />PCR
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />All of the above
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Microscopically, T. pallidum is characterized as
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />With corkscrew motility
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Curved rods
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />It has a corkscrew like morphology
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cannot be seen microscopically
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    No active lesion was seen in patient, however, the neurological signs such as hearing loss and facial weakness were observed. The physician suspects syphilis. What type of testing should be done?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Serological test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Direct microscopy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />None
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    True for Non-treponemal testing
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Specific test
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Flocculation is the positive result
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />The reagent is regain
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Detects cardiolipin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Ticks are carriers of this agent that causes “erythema migrans" 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Borrellia burgdoferi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Borellia hermsii
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Orientia tsutsugamutsi
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ricketsia ricketsi 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    RPR does not require heating as compared with VDRL, this is due to what reagent of RPR?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />EDTA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Thimerosal
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Lecithin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Choline chloride
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    All of the following are under true treponemal testing for syphilis, except:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />FTA-ABS
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />VDRL
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />MHA-TP
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />EIA
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    RA is strongly associated with the alleles of
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />HLA-DR3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />HLA-DR4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />HLA-B27
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />HLA-B45
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    VDRL is non specific test for diagnosing syphilis. It uses the cardiolipin- lecithin- cholesterol antigen to detect the non specific regain antibodies. What is the required rotation of this test when using serum samples?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />100 rpm for 4mins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />100rpm for 8mins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />180rpm for 4mins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />180rpm for 8mins
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which among the following has wrong pairing of the skin test and its disease?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Frei test- lymphogranuloma venereum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Casoni- echinococcus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Becks- trichinelliasis
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Ascoli- anthrax
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    2 methods may be employed to diagnose syphilis. These methods may detect the non specific marker treponemal antibodies. Which among the following are specific test for syphilis?
    </b>
    <p>
        i.	MHA-TP
        ii.	HATTS
        iii.RPR
        iv.	VDRL
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />I only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />I and ii
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />I,ii,iii
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />All of the above
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Lymphadenopathy associated virus has increased trophisim for
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />CD4+cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />CD21+cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Cytotoxic Tcells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />B lymphocytes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    First antibody marker to appear in HBV infection is 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />IgM anti HBc
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />IgM anti HBs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />IgG anti HBe
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />IgG anti HBs
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Increased risk for liver cirrhosis and hepatocellular carcinoma is associated with
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />HBV infection
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />HAV infection
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />HEV infection
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />CMV infection
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A patient is considered as highly infective of HBV if
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />HBsAg is strongly positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Anti- HBc is negative
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />HBeAg is positive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />HBcAg is positive
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These are immunologic reactions that show visible reactions
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Primary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Secondary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Tertiary
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />AOTA
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is the capacity of an individual to ward off an infectious agent:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Resistance
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Passive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Immunogenic
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This deals with the altered states of immunity:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Immunobiology
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Immunochemistry
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Hapten
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Opsonisation
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Branch of Immunology that deals with the study of sera and antisera or 
    antibodies.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Immuno-biology
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Immunopathology
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Immunochemistry
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Immuno Serology
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is the failure of the immune response to a potential immunogen 
    following exposure.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Immunocompetency
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Immunologic Telerance
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Immune Response
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Immunocompromised
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is the continuous efforts of the immune system primarily the T cells to 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Immunologic competency
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Tolerance
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is the deleterious inflammatory reaction which is caused by a specific immunologic reaction.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Hypersensitivity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Surveillance
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In an immune reaction, the cells respond to substances:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Lymphokines
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Monokines
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These cells act as the infantry of the immune system:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Killer cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />helper cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This immunity is inherited from the mother:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Innate Immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Acquired Immunity
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is one of the factors affecting immunity.  Genes are the ones 
    responsible.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Physical and mechanical factors
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Genetic factors
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is an antiviral agent that are made up of glycoprotein.  It inhibits viral 
    replication.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Properdin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Interferon
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is a substance present in normal serum, can kill and lyse some  bacterial species.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Properdin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Interferon
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    his is a substance capable of splitting a bond found in the cell wall of 
    many bacterial species.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Leukocidin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />lysozyme
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Convalescent immunity is:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Active naturally acquired immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Passive naturally acquired
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Immunological response mediated by proteins called antibodies:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Humoral response
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Cellular response
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is an immune response mediated by cells.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Humoral response
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Cellular response
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This act as the first line of defense against infectious agents:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Natural Immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Adaptive Immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Humoral response
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cellular response
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This immunity provides memory:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Natural Immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Specific immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Humoral response
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cellular response
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This immunity does not improve by repeated infections: 
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Innate Immunity
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Acquired
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Humoral response
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cellular response
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is a sac like structure that opens into the cloaca and influences IgG secreting cells that affect humoral immunity:     
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Antigen dependent phase 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Antigen-Independent Phase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is this phase?  T cell precursors migrate to the thymus where it will undergo proliferation and differentiation.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Antigen dependent phase  
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Antigen-Independent Phase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Derived its name for the thymus:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />B cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />T cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />NK cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This amplify killer T cell differentiation:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />TC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />TS
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This helps the B cell to produce antibody response to antigen:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />TH
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />TC
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />CD45 helper cells 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />CD81 helper cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These cells do not carry markers of either T or B cells:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Null Cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Third Population cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These cells do not carry markers of either T or B cells:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Null Cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Third Population cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These cells kill tumor cells and virally infected cells and play a role in 
    regulating the immune response.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Eosinophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Neutrophils
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These cells are capable of phagocytosis and killing ingested organisms.  
    Morphologically they have affinity to the stains heterophically.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Eosinophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Neutrophils
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cells involved in allergy because of the mediators that are released 
    following degranulation by an allergen that cause the adverse reactions:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Basophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Mast Cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This organ is bilobed, located in the thorax and overlies the heart and major 
    blood vessels.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Thymus
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Lymph nodes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Model of cellular interaction wherein “capping" is observed.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Model A                         
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Model B
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The cells release SRS-A and histamine and therefore involved in Hypersensitivity.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Basophils 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Eosinophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Neutrophils
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is the type of immunity without memory.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Adaptive
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Innate
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What type of immunity is Immunization?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Passive acquired
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Active acquired
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What immunoglobulin is produced in secondary immunization?   
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />IgG
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />IgM
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />IgA
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />none of the above
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What phase of immunity is this?  There is a decrease of the antibody
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Lag phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Log Phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Plateua phase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What phase of immunity is this?   There is constant amount of antibody in the serum.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Lag phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Plateau phase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cells respond by releasing certain substances and send a signal to the immune System.  What is/are this/these substance/s.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Lymphokines
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Monokines
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Human Milk can fight the following:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Staphylococci
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Escherichia coli
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    C3a, C4a and C5a are anaphylatoxins responsible for:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Vascular Permeability
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Contraction of smooth muscles
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Responsible for antigen binding:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Fab
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Fc
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />FR
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />CR
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Contain/s two structural units of Ig
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />monomer
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Polymer
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />dimer
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is/These are light chains
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Lambda
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />kappa
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cells of the myeloid series
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />T cells	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />PMN and Monocytes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cells that are agranular
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Neutrophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Lymphocytes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cells believed to produce antibodies
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Basophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Plamacytes
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Antigen Presenting cells:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Skin cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />cells of the thymus and spleen
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Neutrohpils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Macrophages
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cell-mediated immunity refers to the production of “sensitized" lymphocytes that take part in such reactions as in:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Delayed hypersensitivity to tuberculin seen in patients with tubercle bacilli
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Rejection of Transplants
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is the MAC or the Membrane attack complex
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />C5b6789
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />C1q Cir and CIs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />C4a2b
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />C5a2b
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    MBL has been shown to bind to :
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />yeasts such as Candida albicans
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />to viruses such as HIV and influenza A
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Neutrophils
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Macrophages
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Factors absent in Classical Pathway but present in Alternative Pathway:
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Factor B
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Factor D known as Properdin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />IgM
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Bacteriostatic
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Complements present in Classical Pathway but absent in Alternative Pathway.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />C3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />C5
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c4a
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />c3a
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Can also bind to complement receptors on phagocytes causing opsonization of Pathogens.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />C3b
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />C2a and C4a
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Complements present in Classical Pathway but absent in Alternative Pathway.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />C3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />C5
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />c4a
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />c3a
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Can also bind to complement receptors on phagocytes causing opsonization of 
    Pathogens.
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />C3b
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />C2a and C4a
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What factor will be cleaved into Ba and Bb
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Factor D
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Factor B
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Factor X
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Factor V
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Inactivates C3a and C5a activity
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Anaphylotoxin Inhibitor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />CI esterase inhibitor
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Plasmin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Reagin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which  of the following is/are characteristic/s of ABO antibodies?
    </b>
    <br>
    <p>
        1. IgM , naturally occurring<br> 
        2. group O serum contains IgG<br>
        3. produce in vitro and in vivo hemolysis<br>
        4. react best in albumin medium<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the probable reaction of Ulex europaeus extract to Bombay phenotype?
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />0
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1+
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />2+
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />3+
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The following is/are true about anti-H:<br>
    </b>
    <p>
        1. naturally occurring
        2. an autoantibody in some individuals
        3. found in the sera of Bombay phenotype
        4. derived from Dolichos biflorus 
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following shows an increase pattern of immunogenecity?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />D,C,e
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />D,E,c
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />D,c,E
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />D,C,E
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the composition of Rh antigen?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />protein
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />lipids
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />carbohydrates
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />cellulose
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What laboratory test is used to determine weak D antigen?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />IAT
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />DAT
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />elution
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />adsorption
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The transfer of L-fucose to the type I precursor  chain is dependent  on what gene?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />H
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Se
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />ABO
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Le
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What is the most common Lewis phenotype found in whites and blacks?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Le ( a-b+)
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Le (a+b+)
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Le(a+b-)
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Le(a-b-) 
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti-E  antibody develops from which of the following genotypes?<br>
    </b>
    <p>
        1. rr		2. rr'		3. Ror		4. R2r"
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    What donor unit is best for a patient with anti-C?<br>
    </b>
    <p>
        1. RoRo	 2. rr		3. R2r		4. R1R0
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is /are true about Lewis antigen?<br>
    </b>
    <p>
        1. glycoprotein in nature<br>
        2. fully  developed at birth<br>
        3. not synthesized by the red blood cell<br>
        4. demonstrates dosage effect<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is associated with infectious mononucleosis?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />anti-I
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti-i
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />anti-P
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />anti-K
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is/are IgG antibody/ies? <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />anti-M
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti-S
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />anti-Le
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />anti-Pe
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is/are characteristic/s of anti-P1<br>
    </b>
    <p>
        1. found in the sera of P2 individual<br>
        2. can produce severe HDN<br>
        3. can be neutralized by P1 substance in  hydatid cyst fluid<br>
        4. IgG, warm reactive<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The Mcleod  phenotype is associated with blood group system?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Duffy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Kell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Kidd
            </label>
        </li>
        <li>
            <label>
                <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Rh
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which of the following is a common cause of DHTR?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />anti-M
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti-Fy
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />anti-Jk
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />anti-K
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which of the following has a different optimum temperature requirements?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />anti-I
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti-P
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />anti-Le
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />anti-Jk
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which of the following has the longest deferral period?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />a person  after whole blood transfusion
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />a person after whole blood donation
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />a person after plasmapheresis
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />a person who received vaccine for rubella
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which of the following would require one year deferral?<br>
</b>
<p>
1. after tattoo application
2. after a bite from rabid animal
3. rape victim
4. intranasal use of coccaine
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which of the following is/are reason/s for deferral?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />hematocrit= 39%
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />hemoglobin= 13 g/dL
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />diastolic pressure= 120 mmHg
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />weight of 120 pounds
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
What is the most common adverse effect of plateletpheresis?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />hypervolemia
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />citrate toxicity
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />hemolysis
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />allergy 
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
What is /are the advantage/s of Continuous Flow Centrifugation over Intermittent 
Flow  Centrifugation?<br>
</b>
<p>
1. requires only one venipuncture site
2. greater extracorporeal blood volume
3. less time consuming
4. yields  greater amount of blood components
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which  of the following has/have a correct shelf-life    <br>
</b>
<p>
1. Leuko-reduced red blood cell-   24 hours (open system)
2. granulocyte concentrate – 24 hours
3. thawed FFP – 24 hours
4. platelet conc.- 24 hours ( close system)
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
What is the component of choice for Hemophilia B?    <br>
</b>
<p>
1. Leuko-reduced red blood cell-   24 hours (open system)
2. granulocyte concentrate – 24 hours
3. thawed FFP – 24 hours
4. platelet conc.- 24 hours ( close system)
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Factor VII
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Factor VIII
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Factor IX 
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Factor X
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
What is the correct method of platelet separation? <br>
</b>
<p>
1. Leuko-reduced red blood cell-   24 hours (open system)
2. granulocyte concentrate – 24 hours
3. thawed FFP – 24 hours
4. platelet conc.- 24 hours ( close system)
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />light to hard spin
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />hard to light spin
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />two hard spin – one light spin
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />one hard spin – two light spin
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
How many platelets /uL should the platelet concentrate prepared from random blood 
donation?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />3 X 10 <sup>11</sup>
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />5.5 X 10<sup>11</sup>
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />7.2 X 10<sup>11</sup>
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />6.5 X  10<sup>11</sup>
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
One unit of packed red blood cell  increases  the hematocrit  of the patient by how
many percent?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1%
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2%
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />3%
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />4%
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
In high glycerol preparation of frozen red blood cell, the unit is preserved at what 
temperature?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1-6&deg;C
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />– 18&deg;C
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />– 65&deg;C
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />– 120&deg;C
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Can the unit be returned ,unused  if it leaves the laboratory for less than 30 minutes?<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />no, bacterial contamination is possible
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />no, the unit  has been exposed at different temperature
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />no, hemolysis is possible
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />yes , if it is not more than 30 minutes
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
In the preparation of leuko-reduced red blood cell , this method is the least efficient :<br>
</b>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />filtration
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />centrifugation
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />irradiation
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />washing
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
The following information should be found on the blood bag label:<br>
</b>
<p>
1. serologic tests     
2. name of the donor
3. ABO and Rh blood type
4. results of crossmatch
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3, only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
One unit of whole blood , under normal condition should be transfused within:<br>
</b>
<p>
1. serologic tests     <br>
2. name of the donor<br>
3. ABO and Rh blood type<br>
4. results of crossmatch<br>
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />less than one hour
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1-2 hours
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />2-4 hours 
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />more than four hours
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
<b>
Which of  is/are vital in preventing fatal transfusion reaction?<br>
</b>
<p>
1. correct identification of donor<br>
2. correct ABO and Rh blood typing<br>
3. proper collection of pre-transfusion specimen<br>
4. approval of the attending physician<br>
</p>
<ul>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
</label>
</li>
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
</label>
</li>  
<li>
<label>
<input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
</label>
</li>  
</ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Intravascular hemolysis could be due to the following:<br>
    </b>
    <p>
        1. correct identification of donor<br>
        2. correct ABO and Rh blood typing<br>
        3. proper collection of pre-transfusion specimen<br>
        4. approval of the attending physician<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />bacterial contamination
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />blood is diluted with hypotonic solution
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />complement activation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />faster rate of blood infusion
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    In post transfusion purpura , the cell type that shows a marked decrease is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />red blood cell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />white blood cell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />platelet
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neocyte
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following develops  in patient with anaphylactic reaction ?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />anti-IgM
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti-IgG 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />anti-IgA 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />anti-IgE     
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is/are true about extravascular hemolysis?<br>
    </b>
    <ul>
        <p>
            1. produced by IgG of  Kell, Kidd, Duffy and Rh<br>
            2. caused by non-ABO blood system<br>
            3. produced by clinically siginificant antibodies<br>
            4. less severe than the ABO produced hemolysis<br>
        </p>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4    
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    When is exchange transfusion in fetus with HDN indicated ?<br>
    </b>
    <ul>
        <p>
            1. reticulocytosis<br>
            2. erythroblastosis <br>
            3. hyperbilirubinemia<br>
            4. severe anemia<br>
        </p>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4    
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A 300 ug dose of RhIg is enough to neutralize this amount of red blood cell:<br>
    </b>
    <ul>
        <p>
            1.  15 ml. of packed red blood cell<br>
            2.  25 ml of red blood cell in whole blood<br>
            3.  30 ml. of red blood cell in whole blood<br>
            4.  45 ml  of packed red blood cell<br>
        </p>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4    
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Pre-transfusion specimen  should be used within:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />24 hours
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />48 hours
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />72 hours
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />one week    
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Sample specimen for pre-tranfusion  testing  must be stored at:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1-6 &deg;C  for  24 hours
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1-6&deg;C for  three days
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1-6&deg;C for  one week
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1-6&deg;C for one month   
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Agglutination is reported depending on the observed aggregates , presence of small to 
    medium aggregates  with clear background is graded :<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1+ 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2+ 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />3+
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />4+  
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following is/are used in the detection of irregular red cell antibodies?<br>
    </b>
    <p>
        1.  elution    2. adsorption    3.  absorption     4.  fluorescence technique 
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3  only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following  has/ have  anti-H specificity?<br>
    </b>
    <p>
        1. Ulex europaeus<br>
        2.  Helix pomatia<br>
        3. Lotus tetragonolobus<br>
        4  Dolichos biflorus<br> 
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3  only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Which of the following technique/s  employ/s incubation and addition of Coombs 
    reagent?<br>
    </b>
    <p>
        1. Broad Spectrum Compatibility test<br>
        2. DAT<br>
        3. IAT<br>
        4. warm autoabsorption<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3  only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Quality control in Blood Bank section includes checking of the centrifuge   onlyis done :<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />daily using stroboscope
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />weekly using stroboscope
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />monthly using stroboscope
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />quarterly using stroboscope
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti-A = 4+        Anti-B=   1+        A1 cell=   0       B cell =   4+
    Given the  ABO typing reactions above, which do you think is the discrepant?<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />anti-A = 4+
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />anti-B=1+
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />A1 cell=0
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />B cell=4+
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Weak A and B subgroups  confirmation can be done through what process?<br>
    </b>
    <p>
        1. antibody screen test<br>
        2. adsorption<br>
        3. saline replacement technique<br>
        4. elution<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2,4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3,4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This reagent is used to remove agglutinates caused by cold- reactive autoantibodies:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />PEG
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />LISS
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />bovine albumin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Sulfhydryl
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This test is used to determine feto-maternal hemorrhage:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Kleihauer-Betke
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Autologous Absorption
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Prewarming Technique
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Cold Autoadsorption
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Avidity is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Slower in ABO Typing
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Faster in Rh Typing
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Prozone is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />failure of agglutination due to excess of antibody
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />is corrected by dilution of Red Cell Suspension
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti A, B  reacts positively with:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A, B and AB
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />A, B, and O
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti H is found in:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Dolichos biflorus	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />immunized chicken
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Fish row and tears produce<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Anti A	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Anti B
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The immune forms of anti A and Anti B are produced by:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Pregnancy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Blood Transfusion
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    ABO antibodies are:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />found in normal individuals
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />have a wide variation of reactions
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Se symbol refers to:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Non-secretors
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Secretors
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    ABH antigens are not found in:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />granulocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />platelets
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti H reacts least with:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />A2 and A2B
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    BS Lectin 1 will give a negative reaction in the blood of patients with:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Proteus vulgaris	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />E. coli
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The B subgroup that reacts positively with Anti A,B<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Bm	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Bx
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Bombay phenotype:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />non reactive with Anti A, anti B and Anti H
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />reactive with Anti A, Anti B and Anti H
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Bombay donor cells given to “A" blood:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />compatible
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />incompatible
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    H deficient Bombay non-secretors are genetically called:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Parabombay
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Classical Bombay
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Rouleaux factor is due to:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />serum defect due to proteins
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />red cell defect
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The presence of two genetically red cell populations in one individual is due to:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Dispermy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Chimerism with a twin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The predominant sugar in “B"  individual is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />D galactose
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Fucosyltransferase
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The sugar found in between D galactose in H precursor substance is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />N-acetylglucosamine
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />N-acetylgalactosamine
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti acriflavin is produced by individuals exposed to:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Anti acriflavin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />acriflavin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    + to Anti H, (-) to Serum O, (-) to anti A and B:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Bombay or Oh
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    (+) to Anti A, (+) to Serum A (+) to Serum O:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Bombay or Oh
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    (+) to Anti A, (+) to Anti B, (+) to Ulex eurapaeus:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“A1B"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti A,B will give a positive reaction with:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Ax
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“O"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The serum is negative with all typing reagents cell A, cell B and Cell O<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“AB"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The enzyme derived from pineapple:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Bromelin
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Trypsin
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Anti A plus A plus ( secretion containing A substance):<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />No agglutination
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />with agglutination
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The incubation time in hemagglutination test is;<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />15 to 60 min.
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />37 deg, C. to 45 deg. C.
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Reducing the ionic strength of a reaction medium will enhance the ;<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />antigen uptake 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />antibody uptake
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The principle of LISS is :<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />the rate of antibody increases as the ionic strength of the medium decreases
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />The rate of antigen increases as the ionic strength of the medium decreases
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    These are the methods of enhancing agglutination:<br>
    </b>
    <p><br>
        e.	Centrifugation<br>
        f.	Treatment with proteolytic enzymes<br>
        g.	Use of colloids<br>
        h.	Use of AHG<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is/these are potentially infectious material/s:<br>
    </b>
    <p><br>
        i.	Blood	<br>			
        j.	C S F<br>
        k.	Saliva<br>
        l.	vaginal secretions<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1 and 2	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2,3 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and  4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Antigen/s that can be destroyed by enzymes is/are;<br>
    </b>
    <p><br>
        1.  M<br>
        2.	N<br>
        3.	Duffy antigens<br>
        4.	Kell antigens<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 3 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Enzyme coded by A gene;<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />fucosyltransferase    
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />D-galactosyltransferase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />N-acetylgalactosyltransferase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />T d t
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Characteristics of Anti H ( U.e.)<br>
    </b>
    <p><br>
        1 Used in the laboratory for subgrouping<br>
        2 Reacts strongly with  O and A2<br>
        3 Warm acting<br>
        4 derived from Bombay blood group<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The antibody titer is decreased in the following:<br>
    </b>
    <p><br>
        e.	Adult patients<br>
        f.	5-10 years old<br>
        g.	Cord blood<br>
        h.	infants<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    ABO system is controlled by the following:<br>
    </b>
    <p><br>
        1.  A1, A2, B and O<br>
        2.  H and h<br>
        3.  Se and se<br>
        4.  Rho<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    All of these except one are the sources of ABH antigens<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Erythrocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Kidneys
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />lymphocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1granulocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="5" />Platelets
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Uses of Group O serum:<br>
    </b>
    <p>
        1 Proves that the blood type is O<br>
        2 reacts with cells containing Ax and Bx<br>
        3 reacts with cells not containing A and B antigens<br>
        4 reacts with cells containing A and B antigens<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Characteristics of Bombay phenotype<br>
    </b>
    <p><br>
        e.	Made up of IgM<br>
        f.	occurs in consanguineous marriage<br>
        g.	serum reacts positively with Anti A, Anti B, Anti H NS Anti A,B serum<br>
        h.	cells react positively with A cells, B cells, AB cells and O cells<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />1 and 2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />1,2 and 3
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Parents of ABO groups of “A" and “B" cannot have children without<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A, B, O and AB
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />A and AB only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />A and B only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />AB only
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the mother is cde/cde and the alleged father is CDE/CDE , the child is the 
    man's child if he/she is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />CDE/cde
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />CDE/cDe
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />cde/cde
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />R1 R1
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the blood is positive to Anti D and Anti rh' but negative with anti E, the 
    Rh blood type is:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Rho
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Rh1
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />rh2
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />rho
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the recipient is Type “AB" and the donor is type “O", the incompatibility 
    is seen in:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Major Crossmatch
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Minor Crossmatch
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Both Major and Minor Crossmatch
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Neither in major and minor X-match
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the recipient is cde and the donor is cDE, primary isoimmunization is 
    possible and the recipient will produce immune:<br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Anti D and Anti E	
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Anti D and Anti C
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Anti D only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Anti E only
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Low tittered anti Rh is best detected in this phase of Broad Spectrum 
    Compatibility Test.
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Protein Phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Thermophase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Antiglobulin Phase
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />None of the phases
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the patient's serum contains an autoantibody or alloantibody, use of 
    autocontrol is necessary in crossmatching.  This is:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O" cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“AB" serum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />mixture of  Patient's serum and cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />mixture of P serum and donors cells
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    No crossmatching is done in:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />use of pooled “O" cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />use of pooled “O"serum
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Use of Pooled plasma
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Use of F F P
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Directed donations:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />A patient nominates the person from which blood will be accepted for transfusion
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />a donor nominates the recipient for a donation
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />either
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    A donor who has received Yeast derived IG for Hepatitis B vaccinination 
    is deferred for:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />one year
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />6 months
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />two years
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />lifetime
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Aspirin and piroxicam are contraindicated to blood donation if such 
    preparation is for:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />granulocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />red blood cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />prothrombin complex
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />thrombocytes/platelets
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Control/s used in Rh Typing to resolve discrepancies in Rh:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Known D (+) cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Known D (-) cells 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    To check the performance of Anti A used as typing serum, this must be 
    reacted with:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Known B cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Known A1 cells
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Drug induced hemolytic anemia is detected at what antiglobulin test?
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />I A T
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />D A T 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Both
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />neither
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Whole blood is deficient of the following except:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Active platelets 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Erythrocytes 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Granulocytes
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Factors V, VIII & VWF
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Cryoprecipitate indications
    <br>
    </b>
    <p style="float:left; margin-right:19px">
        1.  Fibrinogen deficiency	<br>	
        2.  XIII deficiency  		<br>
    </p>
    <p>
        3.  VWF disease<br>
        4.  Anticipated surgery in Hemophilia A<br>
    </p>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />4 only
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" /> 1,3 and 4
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />1,2,3 and 4
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    The following are indications of fresh frozen plasma except:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Factors V and VIII deficiency
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Bone Marrow depression
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Thrombotic  thrombocytopenic purpura
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />H.  Fever
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This blood product is used as supportive therapy for those with neutropenia 
    and infections not responsive to antibiotic therapy
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Platelets
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />RBCs
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Thrombotic  thrombocytopenic purpura
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />H.  Fever
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the cells are reactive to Anti A and Anti A,B sera, the blood group is:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“B"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“A"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />“AB"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />“O"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the serum is negative with all typing reagents Cell A, Cell B and Cell O, 
    the blood group is:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“A"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“B"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />“AB"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    If the serum A, B and O are all negative, the cells belong to:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“A"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“B"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />“AB"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Use of Anti A,B is used as a third test to confirm that the blood group belongs 
    to Group:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />“O"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />“A1"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />“A2"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />“AB"
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Indirect Anti-globulin Test detects the ff: except:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Du Variants
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Anti Duffy
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Anti Kell
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Rhesus HDN
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    This is a gelatinous intercellular substance consisting of primitive 
    connective tissue of the umbilical cord which is a sampling error in 
    hemagglutination test:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Amniotic fluid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Peritoneal fluid
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Dextran
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Wharton's Jelly
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Compatibility includes the following except:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />Selection of appropriate donor unit 
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />Identification of R and D
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />Cross-matching
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />Paternity Testing
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
<?php if(in_array(++$i, $randomQuestion)): ?>
<li class="question-item" id="immunology-<?php echo $i; ?>">
    <b>
    Parents of  rh" cannot have a child without:
    <br>
    </b>
    <ul>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="1" />hr'
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="2" />rh"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="3" />hr"
            </label>
        </li>
        <li>
            <label>
            <input class="answer-radio" type="radio" name="question-<?php echo $i; ?>"    value="4" />rh'
            </label>
        </li>
    </ul>
</li>
<?php endif; ?>
</div>